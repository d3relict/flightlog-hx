package service.sync.response;

/**
 * @author d3relict
 */
typedef ErrorResponse =
{
    > Response,
    var errorType:ErrorType;
	var errorMessage:String;
}
