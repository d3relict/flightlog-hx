package service.sync.response;

/**
 * @author d3relict
 */
typedef Response =
{
    var success:Bool;
}
