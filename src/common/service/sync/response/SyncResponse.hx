package service.sync.response;

/**
 * @author d3relict
 */
typedef SyncResponse =
{
    > Response,
	var transactions:Array<String>;
    var syncId:UInt;
}
