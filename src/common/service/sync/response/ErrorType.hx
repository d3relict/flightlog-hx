package service.sync.response;

/**
 * @author d3relict
 */
enum ErrorType
{
    unauthorized;
    malformed;
}
