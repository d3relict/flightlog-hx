package service.sync.request;

/**
 * @author d3relict
 */
typedef SyncRequest =
{
    > Request,
    var userId:String;
    var transactions:Array<String>;
    var lastSyncId:UInt;
}
