package;
import php.Exception;
import sys.db.Connection;
import sys.db.Mysql;

/**
 * ...
 * @author d3relict
 */
class Sql
{
    public var lastInsertId(default, null):Int;

    private var connection:Connection;

    public function new()
    {
        connection = Mysql.connect({
            host:       'localhost',
            user:       'derelicth_fl',
            pass:       'psamnuchhafastuchh',
            database:   'derelicth_flightlog'
        });
    }

    public function destroy():Void
    {
        connection.close();
    }

    private function doesTableExist(connection:Connection, name:String):Bool
    {
        var query:String = 'SELECT table_name FROM information_schema.tables WHERE table_schema = "derelicth_flightlog" AND table_name = "' + name + '";';
        return connection.request(query).results().length > 0;
    }

    public function initializeDb():Void
    {
        if (doesTableExist(connection, 'transactions'))
            connection.request('DROP TABLE transactions');

        if (doesTableExist(connection, 'users'))
            connection.request('DROP TABLE users');

        connection.request(
            'CREATE TABLE `users` (' +
                '`id` char(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL' +
            ') ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;'
        );

        connection.request(
            'CREATE TABLE `transactions` (' +
                '`id` int(10) UNSIGNED NOT NULL, ' +
                '`uid` char(32) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL, ' +
                '`data` text COLLATE utf8_unicode_ci NOT NULL, ' +
                '`ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP' +
            ') ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;'
        );

        connection.request(
            'ALTER TABLE `transactions` ' +
                'ADD PRIMARY KEY (`id`), ' +
                'ADD KEY `uid` (`uid`);'
        );

        connection.request(
            'ALTER TABLE `users` ' +
                'ADD PRIMARY KEY (`id`);'
        );

        connection.request(
            'ALTER TABLE `transactions` ' +
                'MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;'
        );

        connection.request(
            'ALTER TABLE `transactions` ' +
                'ADD FOREIGN KEY (`uid`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;'
        );
    }

    public function getUserIdList():Array<String>
    {
        var results:List<Dynamic> = connection.request('SELECT `id` FROM `users`;').results();

        var users:Array<String> = [];
        for (i in results)
        {
            users.push(Reflect.getProperty(i, 'id'));
        }
        return users;
    }

    public function addUser(id:String):Void
    {
        //TODO: sanitize
        connection.request('INSERT INTO `users` (`id`) VALUES("' + id + '");');
    }

    public function delUser(id:String):Void
    {
        connection.request('DELETE FROM `users` WHERE `id` = `"' + id + '"`;');
    }

    public function getTransactionsSince(userId:String, syncId:Int):Array<String>
    {
        var results:List<Dynamic>;

        try
        {
            results = connection.request(
                'SELECT * FROM `transactions` ' +
                'WHERE `uid` = "' + userId + '" ' +
                'AND `id` > ' + syncId + ' ' +
                'ORDER BY `ctime`;'
            ).results();
        }
        catch (e:Exception)
        {
            //TODO: handle errors
            return [];
        }

        var transactions:Array<String> = [];
        for (i in results)
        {
            transactions.push(Reflect.getProperty(i, 'data'));
            lastInsertId = cast Math.max(lastInsertId, Reflect.getProperty(i, 'id'));
        }
        return transactions;
    }

    public function appendTransactions(userId:String, transactions:Array<String>):Void
    {
        for (transaction in transactions)
        {
            connection.request('INSERT INTO `transactions` (`uid`, `data`) VALUES ("' + userId + '", "' + transaction + '");');
        }
        lastInsertId = cast Math.max(lastInsertId, connection.lastInsertId());
    }
}
