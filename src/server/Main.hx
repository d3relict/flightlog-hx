package;
import haxe.Json;
import haxe.Serializer;
import haxe.Unserializer;
import php.Lib;
import php.Session;
import php.Web;
import php.Exception;
import service.sync.response.*;
import service.sync.request.*;

/**
 * ...
 * @author d3relict
 */
class Main
{
    static var self:Main;

    static var sql:Sql;

    public function new() {}

    static function main()
    {
        self = new Main();

        parseRequest();
    }

    private static function parseRequest():Void
    {
        var request:Request;

        try
        {
            request = Unserializer.run(Web.getParams().get('request'));
        }
        catch (e:Exception)
        {
            var errorResponse:ErrorResponse = {
                success: false,
                errorType: ErrorType.malformed,
                errorMessage: 'Malformed request.'
            };
            reply(errorResponse, HTTPStatus.BAD_REQUEST);
            return;
        }

        sql = new Sql();

        //sql.initializeDb();

        switch(request.type)
        {
            case RequestType.sync: synchronize(cast request);
        }

        sql.destroy();
    }

    private static function synchronize(request:SyncRequest):Void
    {
        var userId:String = request.userId;
        if (sql.getUserIdList().indexOf(userId) == -1)
        {
            var errorResponse:ErrorResponse = {
                success: false,
                errorType: ErrorType.unauthorized,
                errorMessage: 'Unauthorized request.'
            };

            reply(errorResponse, HTTPStatus.UNAUTHORIZED);
        }
        else
        {
            var transactions:Array<String> = sql.getTransactionsSince(userId, request.lastSyncId);
            sql.appendTransactions(userId, request.transactions);

            var response:SyncResponse = {
                success: true,
                transactions: transactions,
                syncId: cast Math.max(request.lastSyncId, sql.lastInsertId)
            };

            reply(response);
        }
    }

    private static function reply(response:Response, httpStatus:UInt = 200):Void
    {
        Web.setReturnCode(httpStatus);
        Lib.print(Serializer.run(response));
    }
}
