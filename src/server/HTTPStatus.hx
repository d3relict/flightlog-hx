package;

/**
 * ...
 * @author d3relict
 */
class HTTPStatus
{
    public static var OK(default, never):UInt =                     200;

    public static var BAD_REQUEST(default, never):UInt =            400;
    public static var UNAUTHORIZED(default, never):UInt =           401;

    public static var INTERNAL_SERVER_ERROR(default, never):UInt =  500;
    public static var NOT_IMPLEMENTED(default, never):UInt =        501;
    public static var SERVICE_UNAVAILABLE(default, never):UInt =    503;
}
