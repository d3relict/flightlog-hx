package interfaces.logic;
import types.complex.event.Event;
import types.primitive.Identity;

/**
 * @author d3relict
 */
interface IEventUpdater
{
    function addEvent(event:Event):Void;
    function removeEvent(identity:Identity):Void;

    function scrutinize():Void;
}
