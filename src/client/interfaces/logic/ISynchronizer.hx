package interfaces.logic;
import types.primitive.TransactionProperties;

/**
 * @author d3relict
 */
interface ISynchronizer
{
    var userId(get, set):String;

    var status(get, null):UInt;
    var lastAttempt(get, null):Date;
    var lastSuccess(get, null):Date;

    function sync():Void;
    function queue(transaction:TransactionProperties):Void;
}
