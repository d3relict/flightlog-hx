package interfaces.logic;
import types.complex.aircraft.Aircraft;
import types.complex.transaction.AircraftTransaction;
import types.complex.event.Event;
import types.complex.transaction.RemovalTransaction;
import types.primitive.Identity;

/**
 * @author d3relict
 */
interface IAircraftUpdater
{
    function addAircraft(aircraft:Aircraft):Void;
    function removeAircraft(identity:Identity):Void;

    function applyFlight(flightEvent:Event):Void;
    function undoFlight(flightEvent:Event):Void;
}
