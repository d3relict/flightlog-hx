package interfaces.logic;
import types.complex.transaction.AircraftTransaction;
import types.complex.transaction.BatteryTransaction;

/**
 * @author d3relict
 */
interface IAssociator
{
    function setAssociationByAircraft(transaction:AircraftTransaction):Void;
    function setAssociationByBattery(transaction:BatteryTransaction):Void;
}
