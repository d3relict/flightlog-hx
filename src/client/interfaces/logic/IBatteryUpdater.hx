package interfaces.logic;
import hex.module.IModule;
import types.complex.battery.Battery;
import types.complex.event.Event;
import types.primitive.Identity;

/**
 * @author d3relict
 */
interface IBatteryUpdater extends IModule
{
    function addBattery(battery:Battery):Void;
    function removeBattery(identity:Identity):Void;

    function applyCharge(chargeEvent:Event):Void;
    function undoCharge(chargeEvent:Event):Void;
}
