package interfaces.logic;
import types.complex.event.Event;
import types.complex.event.EventList;

/**
 * @author d3relict
 */
interface IEventProvider
{
    var events(get, never):EventList;

    function getLastEventForBattery(id:String):Event;
    function getLastEventForAircraft(id:String):Event;

    function getAllEventsForBattery(id:String):EventList;
    function getAllEventsForAircraft(id:String):EventList;
}
