package interfaces.logic;
import hex.module.IModule;
import types.complex.aircraft.Aircraft;
import types.complex.aircraft.AircraftList;

/**
 * @author d3relict
 */
interface IAircraftProvider extends IModule
{
    var aircrafts(get, never):AircraftList;

    function getAircraftById(aircraftId:String):Aircraft;
}
