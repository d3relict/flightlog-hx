package interfaces.logic;
import hex.module.IModule;
import types.complex.battery.Battery;
import types.complex.battery.BatteryList;

/**
 * @author d3relict
 */

interface IBatteryProvider extends IModule
{
    var batteries(get, null):BatteryList;

    function getBatteryById(id:String):Battery;
    function getBatteriesByAssociation(id:String):BatteryList;
    function getBatteriesByCharge(minPercent:Float = 0, maxPercent:Float = 100):BatteryList;
}
