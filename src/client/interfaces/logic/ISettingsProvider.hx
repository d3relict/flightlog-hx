package interfaces.logic;
import hex.module.IModule;

/**
 * @author d3relict
 */
interface ISettingsProvider extends IModule
{
    var minChargeToEnableFlight(default, never):Float;
    var maxLevelToEnableCharge(default, never):Float;
    var minLevelToEnableDischarge(default, never):Float;
    var defaultDischargeLevel(default, never):Float;
    var defaultPostFlightLevel(default, never):Float;
    var overChargeMultiplier(default, never):Float;
    var defaultMaxFlightDuration(default, never):Float;
}
