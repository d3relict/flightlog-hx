package interfaces.util;

/**
 * @author d3relict
 */
interface ILocalStorage 
{
    function set(key:String, value:Dynamic):Void;
    function get(key:String):Dynamic;
    function remove(key:String):Void;
}
