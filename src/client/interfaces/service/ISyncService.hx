package interfaces.service;
import hex.service.stateless.http.IHTTPService;
import service.sync.response.Response;

/**
 * @author d3relict
 */
interface ISyncService extends IHTTPService
{
    var result(get, null):Response;
}
