package interfaces.ui;

/**
 * @author d3relict
 */
interface IEdit
{
    function edit(id:String):Void;
}
