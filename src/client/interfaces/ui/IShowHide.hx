package interfaces.ui;

/**
 * @author d3relict
 */
interface IShowHide 
{
    function show ():Void;
    function hide ():Void;
}
