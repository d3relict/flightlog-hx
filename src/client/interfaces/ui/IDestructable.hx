package interfaces.ui;

/**
 * @author d3relict
 */
interface IDestructable
{
    function destroy():Void;
}
