package interfaces.ui;

/**
 * @author d3relict
 */
interface IViewComponent extends IDestructable
{
    var label(default, null):String;
    var id   (default, null):String;
}
