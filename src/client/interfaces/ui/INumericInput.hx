package interfaces.ui;

/**
 * @author d3relict
 */
interface INumericInput<T>
{
    var value   (default, set):T;
    var min     (default, set):T;
    var max     (default, set):T;
}
