package interfaces.ui;

/**
 * @author d3relict
 */
interface IOpenClose 
{
    function open ():Void;
    function close ():Void;
}
