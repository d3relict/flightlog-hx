package interfaces.ui;

/**
 * @author d3relict
 */
interface IModal extends IOpenClose
{
    var available(get, never):Bool;
    
    function update():Void;
}
