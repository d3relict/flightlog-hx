    package module.ui.modal.common.view;

import haxe.Template;
import hex.view.BasicView;
import hex.view.IView;
import js.Browser;
import js.html.ButtonElement;
import js.html.DivElement;
import js.html.Element;
import js.html.MouseEvent;
import js.jquery.JQuery;
import module.ui.modal.common.controller.interfaces.IModalController;
import module.ui.modal.common.model.interfaces.IModalModelListener;
import module.ui.modal.common.model.interfaces.IModalModelRO;
import module.ui.modal.common.template.ModalButtonTemplate;

/**
 * ...
 * @author d3relict
 */
class ModalView extends BasicView implements IView implements IModalModelListener
{
    @Inject
    public var panelModel:IModalModelRO;

    @Inject
    public var controller:IModalController;

    private var modal:Element;
    private var modalId:String;

    private var buttonContainer:DivElement;
    private var submitButton:ButtonElement;

    public function new(panelId:String)
    {
        super();
        this.modalId = panelId;
    }

    @PostConstruct
    public function init():Void
    {
        modal = Browser.document.getElementById(modalId);

        buttonContainer = cast modal.getElementsByClassName('button')[0];
        buttonContainer.innerHTML = ModalButtonTemplate.generate({});
        buttonContainer.addEventListener('click', onClick);

        submitButton = cast buttonContainer.querySelectorAll('[data-id="submit"]')[0];

        panelModel.addListener(this);

        onStateChange(panelModel.open);
        onSetSubmittable();
    }

    private function onClick(event:MouseEvent):Void
    {
        var target:Element = cast event.target;
        switch (target.getAttribute('data-id'))
        {
            case 'cancel': controller.close();
            case 'submit': controller.submit();
        }
    }

    public function onStateChange (open:Bool):Void
    {
        untyped new JQuery(modal).modal(open ? 'open' : 'close');
    }

    public function onSetSubmittable():Void
    {
        submitButton.disabled = !panelModel.submittable;
    }
}
