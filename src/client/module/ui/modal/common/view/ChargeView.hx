package module.ui.modal.common.view;

import haxe.Template;
import hex.view.BasicView;
import hex.view.IView;
import interfaces.ui.IDestructable;
import interfaces.ui.INumericInput;
import interfaces.ui.IViewComponent;
import js.Browser;
import js.html.DivElement;
import js.html.Element;
import js.html.InputElement;
import module.ui.modal.common.controller.interfaces.IChargeController;
import module.ui.modal.common.model.interfaces.*;
import module.ui.modal.common.template.ChargeGroupTemplate;
import module.ui.modal.common.view.component.SliderInputCombo;
import util.StringUtil;

/**
 * ...
 * @author d3relict
 */
class ChargeView extends BasicView
    implements IView
    implements IChargeModelListener
    implements IEditModelListener
{
    @Inject
    public var chargeModel:IChargeModelRO;

    @Inject
    @Optional(true)
    public var batteryModel:IBatteryModelRO;

    @Inject
    @Optional(true)
    public var editModel:IEditModelRO;

    @Inject
    public var controller:IChargeController;

    private var modalId:String;
    private var levelOnly:Bool;

    private var container:DivElement;

    private var components:Array<IViewComponent>;

    private var lockCheck:InputElement;

    public function new(modalId:String, levelOnly:Bool = false)
    {
        super();
        this.modalId = modalId;
        this.levelOnly = levelOnly;

        components = new Array<IViewComponent>();
    }

    @PostConstruct
    public function init():Void
    {
        container = cast Browser.document.getElementById(modalId).getElementsByClassName('charge')[0];

        generateTemplate();
        initializeElements();

        chargeModel.addListener(this);

        if (editModel != null)
            editModel.addListener(this);
    }

    private function generateTemplate():Void
    {
        var items:Array<Dynamic> = new Array<Dynamic>();
        for (chargeItem in chargeModel.items)
        {
            items.push({
                id: chargeItem.batteryId,
                name: batteryModel != null ? StringUtil.batteryLabel(batteryModel.getBatteryById(chargeItem.batteryId)) : ''
            });
        }

        container.innerHTML = ChargeGroupTemplate.generate({modalId: modalId, levelOnly: levelOnly, items: items});
    }

    private function initializeElements():Void
    {
        while (components.length > 0)
            components.pop().destroy();

        for (item in chargeModel.items)
        {
            components.push(new SliderInputCombo<UInt>(modalId + '-', 'charge', item.batteryId, item.charge, item.minCharge, item.maxCharge, onChargeInput));
            components.push(new SliderInputCombo<Float>(modalId + '-', 'level', item.batteryId, item.level, item.minLevel, item.maxLevel, onLevelInput));
        }

        setupLockSlider();
    }

    private function setupLockSlider():Void
    {
        lockCheck = cast Browser.document.getElementById(modalId + '-lockCheck');
        lockCheck.checked = chargeModel.locked;
        lockCheck.onchange = onLockCheckChange;
    }

    private function onChargeInput(id:String, value:UInt):Void
    {
        controller.setCharge(id, value);
    }

    private function onLevelInput(id:String, value:Float):Void
    {
        controller.setLevel(id, value);
    }

    private function getComponent(label:String, id:String):IViewComponent
    {
        for (component in components)
            if (component.label == label && component.id == id)
                return component;

        return null;
    }

    //IChargeModelListener

    public function onSetLocked():Void
    {
        lockCheck.checked = chargeModel.locked;
    }

    public function onCountChange():Void
    {
        generateTemplate();
        initializeElements();
    }

    public function onSetCharge(id:String):Void
    {
        chargeModel.removeListener(this);
        var numericInput:INumericInput<UInt> = cast getComponent('charge', id);
        numericInput.value = chargeModel.getItem(id).charge;
        chargeModel.addListener(this);
    }

    public function onSetLevel(id:String):Void
    {
        chargeModel.removeListener(this);
        var numericInput:INumericInput<Float> = cast getComponent('level', id);
        numericInput.value = chargeModel.getItem(id).level;
        chargeModel.addListener(this);
    }


    private function onLockCheckChange():Void
    {
        controller.setLocked(lockCheck.checked);
    }

    public function onEditingChange():Void
    {
        var visible:Bool = editModel == null || (editModel != null && editModel.editing == null);
        container.style.cssText = visible ? '' : 'display: none;';
    }

}
