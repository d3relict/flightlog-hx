package module.ui.modal.common.view;

import hex.view.BasicView;
import hex.view.IView;
import js.Browser;
import js.html.DivElement;
import js.html.OptionElement;
import js.html.SelectElement;
import module.ui.modal.common.controller.interfaces.IChargeController;
import module.ui.modal.common.controller.interfaces.IFormController;
import module.ui.modal.common.model.interfaces.IBatteryModelListener;
import module.ui.modal.common.model.interfaces.IBatteryModelRO;
import module.ui.modal.common.model.interfaces.IChargeModelListener;
import module.ui.modal.common.model.interfaces.IChargeModelRO;
import module.ui.modal.common.template.BatterySelectorTemplate;
import util.DOMHelper;
import util.StringUtil;

/**
 * ...
 * @author d3relict
 */
class BatteryView extends BasicView
    implements IView
    implements IBatteryModelListener
    implements IChargeModelListener
{
    @Inject
    public var batteryModel:IBatteryModelRO;

    @Inject
    public var chargeModel:IChargeModelRO;

    @Inject
    public var chargeController:IChargeController;

    @Inject @Optional(true)
    public var formController:IFormController;

    private var modalId:String;

    private var batterySelect:SelectElement;

    public function new(modalId:String)
    {
        super();
        this.modalId = modalId;
    }

    @PostConstruct
    public function init():Void
    {
        var container:DivElement = cast Browser.document.getElementById(modalId).getElementsByClassName('battery')[0];
        container.innerHTML = BatterySelectorTemplate.generate({modalId:modalId});

        batterySelect = cast Browser.document.getElementById(modalId + '-batterySelect');
        batterySelect.onchange = onSelected;

        batteryModel.addListener(this);
        chargeModel.addListener(this);

        onUpdate();
    }

    private function onSelected(event):Void
    {
        var selected:Array<String> = new Array<String>();
        for (option in cast(event.target, SelectElement).selectedOptions)
        {
            selected.push(cast (option, OptionElement).value);
        };

        chargeModel.removeListener(this);
        chargeController.selectBatteries(selected);
        chargeModel.addListener(this);

        if (formController != null)
            formController.validate();
    }

    /* INTERFACE module.ui.flightpanel.model.IBatteryModelListener */

    public function onUpdate():Void
    {
        var selected:Array<String> = new Array<String>();
        for (item in chargeModel.items)
        {
            selected.push(item.batteryId);
        }

        var multiple:Bool = false;
        for (battery in batteryModel.batteries)
            if (battery.cellCount < chargeModel.cellCount)
                multiple = true;

        batterySelect.multiple = multiple;

        var propertyList:Array<SelectItem> = new Array<SelectItem>();

        if (multiple)
            propertyList.push({value: '', text: 'No battery selected', disabled: true, selected: false});

        for (battery in batteryModel.batteries)
        {
            propertyList.push({
                value:      battery.id,
                text:       StringUtil.batteryLabel(battery),
                disabled:   false,
                selected:   selected.indexOf(battery.id) != -1
            });
        }

        DOMHelper.fillSelectElement(batterySelect, propertyList);
    }


    /* INTERFACE module.ui.modal.common.model.interfaces.IChargeModelListener */

    public function onSetLocked():Void
    {

    }

    public function onCountChange():Void
    {
        onUpdate();
    }

    public function onSetCharge(id:String):Void
    {

    }

    public function onSetLevel(id:String):Void
    {

    }

}
