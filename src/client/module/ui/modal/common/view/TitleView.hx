package module.ui.modal.common.view;

import haxe.Template;
import hex.view.BasicView;
import hex.view.IView;
import js.Browser;
import js.html.DivElement;
import js.html.Element;
import module.ui.modal.common.model.interfaces.IEditModelListener;
import module.ui.modal.common.model.interfaces.IEditModelRO;

/**
 * ...
 * @author d3relict
 */
class TitleView extends BasicView implements IView implements IEditModelListener
{
    @Inject
    public var editModel:IEditModelRO;

    private var modal:Element;
    private var modalId:String;

    private var titleContainer:DivElement;
    private var template:String;

    public function new(panelId:String, template:String)
    {
        super();
        this.template = template;
        this.modalId = panelId;
    }

    @PostConstruct
    public function init():Void
    {
        modal = Browser.document.getElementById(modalId);
        titleContainer = cast modal.getElementsByClassName('title')[0];

        editModel.addListener(this);
        onEditingChange();
    }

    /* INTERFACE module.ui.modal.common.model.interfaces.IEditModelListener */

    public function onEditingChange():Void
    {
        if (titleContainer != null)
        {
            titleContainer.innerHTML = new Template(template).execute({editing: editModel.editing != null});
        }
    }
}
