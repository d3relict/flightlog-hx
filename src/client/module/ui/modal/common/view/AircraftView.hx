package module.ui.modal.common.view;

import haxe.Template;
import hex.view.BasicView;
import hex.view.IView;
import js.Browser;
import js.html.DivElement;
import js.html.OptionElement;
import js.html.SelectElement;
import module.ui.modal.common.controller.interfaces.IFlightController;
import module.ui.modal.common.model.interfaces.IAircraftModelListener;
import module.ui.modal.common.model.interfaces.IAircraftModelRO;
import module.ui.modal.common.template.AircraftSelectorTemplate;
import util.DOMHelper;
import util.StringUtil;

/**
 * ...
 * @author d3relict
 */
class AircraftView extends BasicView implements IView implements IAircraftModelListener
{
    @Inject
    public var aircraftModel:IAircraftModelRO;

    @Inject
    public var controller:IFlightController;

    private var modalId:String;
    private var modelSelect:SelectElement;

    public function new(modalId:String)
    {
        super();
        this.modalId = modalId;
    }

    @PostConstruct
    public function init():Void
    {
        var container:DivElement = cast Browser.document.getElementById(modalId).getElementsByClassName('aircraft')[0];
        container.innerHTML = AircraftSelectorTemplate.generate({modalId:modalId});

        modelSelect = cast Browser.document.getElementById(modalId + '-modelSelect');
        modelSelect.onchange = function(event) {
            var selected:OptionElement = cast cast(event.target, SelectElement).selectedOptions[0];
            controller.selectAircraft(selected.value);
        };

        aircraftModel.addListener(this);
        onUpdate();
    }

    /* INTERFACE module.ui.flightpanel.model.IAircraftModelListener */

    public function onUpdate():Void
    {
        var propertyList:Array<SelectItem> = new Array<SelectItem>();
        for (aircraft in aircraftModel.aircrafts)
        {
            propertyList.push({value: aircraft.id, text: StringUtil.aircraftLabel(aircraft), disabled: false, selected: false});
        }

        DOMHelper.fillSelectElement(this.modelSelect, propertyList);
    }
}
