package module.ui.modal.common.view;
import hex.view.BasicView;
import hex.view.IView;
import js.Browser;
import js.html.DivElement;
import js.html.OptionElement;
import js.html.SelectElement;
import module.ui.modal.common.controller.interfaces.ICellCountController;
import module.ui.modal.common.model.interfaces.ICellCountModelListener;
import module.ui.modal.common.model.interfaces.ICellCountModelRO;
import module.ui.modal.common.template.CellCountSelectorTemplate;
import util.DOMHelper;
import util.DOMHelper.SelectItem;
import util.StringUtil;

/**
 * ...
 * @author d3relict
 */
class CellCountView extends BasicView
    implements IView
    implements ICellCountModelListener
{
    @Inject
    public var cellCountModel:ICellCountModelRO;
    @Inject
    public var cellCountController:ICellCountController;

    private var modalId:String;

    private var cellCountSelect:SelectElement;

    public function new(modalId:String)
    {
        super();
        this.modalId = modalId;
    }

    @PostConstruct
    public function init():Void
    {
        var container:DivElement = cast Browser.document.getElementById(modalId).getElementsByClassName('cellcount')[0];
        container.innerHTML = CellCountSelectorTemplate.generate({modalId: modalId});

        cellCountSelect = cast Browser.document.getElementById(modalId + '-cellCountSelect');
        cellCountSelect.onchange = onSelected;

        cellCountModel.addListener(this);
    }

    private function onSelected(event):Void
    {
        var selected:Array<String> = new Array<String>();
        for (option in cast(event.target, SelectElement).selectedOptions)
        {
            selected.push(cast (option, OptionElement).value);
        };
        cellCountController.setCellCount(Std.parseInt(selected[0]));
    }

    /* INTERFACE module.ui.modal.common.model.interfaces.ICellCountModelListener */

    public function onSetCount():Void
    {
        var values:Array<UInt> = [1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 14];

        var propertyList:Array<SelectItem> = [];
        for (value in values)
        {
            propertyList.push({
                value:      cast value,
                text:       value + 'S (' + StringUtil.roundToString(3.7 * value, 2) + ' volts)',
                disabled:   false,
                selected:   value ==  cellCountModel.cellCount
            });
        }

        DOMHelper.fillSelectElement(cellCountSelect, propertyList);
    }

}
