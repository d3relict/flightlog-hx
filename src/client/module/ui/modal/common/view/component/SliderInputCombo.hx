package module.ui.modal.common.view.component;
import haxe.Constraints.Function;
import hex.view.BasicView;
import hex.view.IView;
import interfaces.ui.IDestructable;
import interfaces.ui.INumericInput;
import interfaces.ui.IViewComponent;
import js.Browser;
import js.html.Element;
import js.html.InputElement;
import util.StringUtil;

/**
 * ...
 * @author d3relict
 */
class SliderInputCombo<T>
    implements INumericInput<T>
    implements IViewComponent
{
    public var prefix(default, null):String;
    public var label (default, null):String;
    public var id    (default, null):String;

    public var value (default, set):T;
    public var min   (default, set):T;
    public var max   (default, set):T;

    private var slider:Element;
    private var input:InputElement;
    private var callback:String->T->Void;

    private var sliding:Bool = false;

    public function new(prefix:String, label:String, id:String, value:T, min:T, max:T, callback:String->T->Void)
    {
        this.prefix     = prefix;
        this.label      = label;
        this.id         = id;
        this.callback   = callback;

        this.value      = value;
        this.min        = min;
        this.max        = max;

        setup();
    }

    private function setup():Void
    {
        slider = cast Browser.document.getElementById(prefix + label + 'Slider-' + id);
        untyped noUiSlider.create(
            slider,
            {
                start: value,
                step: 1,
                range: {
                    min: min,
                    max: max
                }
            }
        );

        untyped slider.noUiSlider.on('slide', onSliderChange);

        input = cast Browser.document.getElementById(prefix + label + 'Input-' + id);
        input.value = cast value;
        input.onchange = onInputChange;
    }

    public function destroy():Void
    {
        if (untyped slider.noUiSlider != undefined)
            untyped slider.noUiSlider.destroy();
    }

    public function set_value(value:T):T
    {
        if (input != null)
            input.value = StringUtil.roundToString(cast value, 0);

        if (!sliding && slider != null)
            untyped slider.noUiSlider.set(StringUtil.roundToString(cast value, 0));

        return this.value = value;
    }

    public function set_min(value:T):T
    {
        return min = value;
    }

    public function set_max(value:T):T
    {
        return max = value;
    }

    private function onSliderChange():Void
    {
        sliding = true;
        value = cast Std.parseFloat(untyped slider.noUiSlider.get());
        callback(id, value);
        sliding = false;
    }

    private function onInputChange():Void
    {
        value = cast Std.parseFloat(input.value);
        callback(id, value);
    }
}
