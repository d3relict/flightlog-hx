package module.ui.modal.common.view;

import haxe.Template;
import hex.view.BasicView;
import hex.view.IView;
import interfaces.logic.ISettingsProvider;
import js.Browser;
import js.html.DivElement;
import js.html.Element;
import js.html.InputElement;
import module.ui.modal.common.controller.interfaces.IFlightController;
import module.ui.modal.common.model.interfaces.IFlightModelListener;
import module.ui.modal.common.model.interfaces.IFlightModelRO;
import module.ui.modal.common.template.DurationGroupTemplate;

/**
 * ...
 * @author d3relict
 */
class DurationView extends BasicView implements IView implements IFlightModelListener
{
    @Inject
    public var flightModel:IFlightModelRO;

    @Inject
    public var controller:IFlightController;

    @Inject
    public var settingsProvider:ISettingsProvider;

    private var modalId:String;

    private var durationSlider:Element;
    private var minuteInput:InputElement;
    private var secondInput:InputElement;

    private var durationSliderDragging:Bool = false;

    public function new(modalId:String)
    {
        super();
        this.modalId = modalId;
    }

    @PostConstruct
    public function init():Void
    {
        var container:DivElement = cast Browser.document.getElementById(modalId).getElementsByClassName('duration')[0];
        container.innerHTML = DurationGroupTemplate.generate({modalId:modalId});

        durationSlider = cast Browser.document.getElementById(modalId + '-durationSlider');
        resetDurationSlider();

        minuteInput  = cast Browser.document.getElementById(modalId + '-minuteInput');
        minuteInput.onchange = onInputChange;
        minuteInput.step = '1';

        secondInput  = cast Browser.document.getElementById(modalId + '-secondInput');
        secondInput.onchange = onInputChange;
        secondInput.step = '1';

        flightModel.addListener(this);
        onSetDischarge();
    }

    private function resetDurationSlider():Void
    {
        if (untyped durationSlider.noUiSlider != undefined)
        {
            untyped durationSlider.noUiSlider.destroy();
        }

        untyped noUiSlider.create(
            durationSlider,
            {
                start: flightModel.duration,
                step: 1,
                range: {
                    min: 0,
                    max: settingsProvider.defaultMaxFlightDuration
                }
            }
        );
        untyped durationSlider.noUiSlider.on('slide', onDurationSliderSlide);
    }

    private function onDurationSliderSlide():Void
    {
        durationSliderDragging = true;
        controller.setDuration(Std.parseInt(untyped durationSlider.noUiSlider.get()));
    }

    private function onInputChange():Void
    {
        controller.setDuration(60 * Std.parseInt(minuteInput.value) + Std.parseInt(secondInput.value));
    }

    /* INTERFACE module.ui.flightpanel.model.interfaces.IFlightModelListener */

    public function onSetDuration():Void
    {
        minuteInput.value = ('0' + Math.floor(flightModel.duration / 60)).substr(-2, 2);
        secondInput.value = ('0' + Math.floor(flightModel.duration % 60)).substr(-2, 2);

        if (!durationSliderDragging)
        {
            untyped durationSlider.noUiSlider.set(flightModel.duration);
        }
        durationSliderDragging = false;
    }

    public function onSelectAircraft():Void
    {
        resetDurationSlider();
    }

    public function onSetDischarge():Void {}
    public function onSelectBattery():Void {}
    public function onSetDate():Void {}

}
