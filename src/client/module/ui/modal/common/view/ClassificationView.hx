package module.ui.modal.common.view;

import haxe.Template;
import hex.view.BasicView;
import hex.view.IView;
import js.Browser;
import js.html.DivElement;
import js.html.Event;
import js.html.InputElement;
import js.jquery.JQuery;
import module.ui.modal.common.controller.interfaces.IClassificationController;
import module.ui.modal.common.enums.ItemType;
import module.ui.modal.common.model.interfaces.IClassificationModelListener;
import module.ui.modal.common.model.interfaces.IClassificationModelRO;
import module.ui.modal.common.template.ClassificationInputTemplate;

/**
 * ...
 * @author d3relict
 */
class ClassificationView extends BasicView implements IView implements IClassificationModelListener
{
    @Inject
    public var classificationModel:IClassificationModelRO;
    @Inject
    public var classificationController:IClassificationController;

    private var modalId:String;
    private var itemType:ItemType;

    private var manufacturerInput:InputElement;
    private var typeInput:InputElement;
    private var nameInput:InputElement;

    private var documentReady:Bool = false;

    public function new(modalId:String, itemType:ItemType)
    {
        super();
        this.modalId = modalId;
        this.itemType = itemType;
    }

    @PostConstruct
    public function init():Void
    {
        var container:DivElement = cast Browser.document.getElementById(modalId).getElementsByClassName('classification')[0];
        container.innerHTML = ClassificationInputTemplate.generate(
        {
            modalId: modalId,
            itemType: Std.string(itemType)
        });

        //materializecss autocomplete is not present before onready
        //TODO: find a less hacky solution
        new JQuery('document').ready(function(){
            documentReady = true;
            onSetKnownManufacturers();
            onSetKnownTypes();
            updateTextFields();
        });

        manufacturerInput = cast Browser.document.getElementById(modalId + '-manufacturerInput');
        manufacturerInput.onchange = function() {
            classificationController.setManufacturer(manufacturerInput.value);
        }

        typeInput = cast Browser.document.getElementById(modalId + '-typeInput');
        typeInput.onchange = function() {
            classificationController.setType(typeInput.value);
        }

        nameInput = cast Browser.document.getElementById(modalId + '-nameInput');
        nameInput.onchange = function() {
            classificationController.setName(nameInput.value);
        }

        classificationModel.addListener(this);
    }

    private function setNamePlaceHolder()
    {
        if (classificationModel.manufacturer != null && classificationModel.type != null)
        {
            //TODO: use ItemLabel to set placeholder
            nameInput.placeholder = classificationModel.manufacturer + ' ' + classificationModel.type;
            updateTextFields();
        }
    }

    /* INTERFACE module.ui.modal.aircraftpanel.model.IIdentityModelListener */

    public function onSetManufacturer():Void
    {
        manufacturerInput.value = classificationModel.manufacturer;
        updateTextFields();
        setNamePlaceHolder();
    }

    public function onSetType():Void
    {
        typeInput.value = classificationModel.type;
        updateTextFields();
        setNamePlaceHolder();
    }

    public function onSetName():Void
    {
        nameInput.value = classificationModel.name;
        updateTextFields();
    }

    public function onSetKnownManufacturers():Void
    {
        var autoComplete:Dynamic = {};
        for (manufacturer in classificationModel.knownManufacturers)
        {
            Reflect.setProperty(autoComplete, manufacturer, null);
        }

        if (documentReady == true)
        {
            untyped $(manufacturerInput).autocomplete({
                data: autoComplete
            });
        }
    }

    public function onSetKnownTypes():Void
    {
        var autoComplete:Dynamic = {};
        for (type in classificationModel.knownTypes)
        {
            Reflect.setProperty(autoComplete, type, null);
        }

        if (documentReady == true)
        {
            untyped $(typeInput).autocomplete({
                data: autoComplete
            });
        }
    }

    private function updateTextFields():Void
    {
        if (documentReady == true)
            untyped Materialize.updateTextFields();
    }
}
