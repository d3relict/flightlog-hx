package module.ui.modal.common.view;

import haxe.Template;
import hex.view.BasicView;
import hex.view.IView;
import js.Browser;
import js.html.DivElement;
import js.html.Event;
import js.html.InputElement;
import js.html.OptionElement;
import js.html.SelectElement;
import js.jquery.JQuery;
import module.ui.modal.common.controller.interfaces.IAssociationController;
import module.ui.modal.common.model.interfaces.IAssociationModelListener;
import module.ui.modal.common.model.interfaces.IAssociationModelRO;
import module.ui.modal.common.template.AssociationSelectorTemplate;
import util.DOMHelper;

/**
 * ...
 * @author d3relict
 */
class AssociationView extends BasicView implements IView implements IAssociationModelListener
{
    @Inject
    public var associationModel:IAssociationModelRO;
    @Inject
    public var associationController:IAssociationController;

    private var modalId:String;

    private var associationSelect:SelectElement;

    public function new(modalId:String)
    {
        super();
        this.modalId = modalId;
    }

    @PostConstruct
    public function init():Void
    {
        var container:DivElement = cast Browser.document.getElementById(modalId).getElementsByClassName('association')[0];
        container.innerHTML = AssociationSelectorTemplate.generate({modalId:modalId});

        associationSelect = cast Browser.document.getElementById(modalId + '-associationSelect');
        associationSelect.onchange = function() {
            var selectedIds:Array<String> = new Array<String>();
            for (option in associationSelect.selectedOptions)
            {
                selectedIds.push(cast(option, OptionElement).value);
            }

            //listening to change will close the popup selector
            associationModel.removeListener(this);
            associationController.setAssociation(selectedIds);
            associationModel.addListener(this);
        }

        updateSelector();

        associationModel.addListener(this);
    }


    /* INTERFACE module.ui.modal.aircraftpanel.model.IAssociationModelListener */

    public function onSetSelectedIds():Void
    {
        updateSelector();
    }

    public function onSetSelectItems():Void
    {
        updateSelector();
    }

    private function updateSelector():Void
    {
        var selectItems:Array<SelectItem> = [{value: '', text: 'No associations', disabled: true, selected: false}];
        for (selectable in associationModel.selectItems)
        {
            selectItems.push({value: selectable.id, text: selectable.name, disabled: false, selected: associationModel.selectedIds.indexOf(selectable.id) != -1});
        }
        DOMHelper.fillSelectElement(associationSelect, selectItems);
    }

}
