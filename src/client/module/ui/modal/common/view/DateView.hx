package module.ui.modal.common.view;

import haxe.Template;
import hex.view.BasicView;
import hex.view.IView;
import js.Browser;
import js.html.DivElement;
import js.html.InputElement;
import js.jquery.JQuery;
import module.ui.modal.common.controller.interfaces.IDateController;
import module.ui.modal.common.model.interfaces.IDateModelListener;
import module.ui.modal.common.model.interfaces.IDateModelRO;
import module.ui.modal.common.template.DateSelectorTemplate;
import util.StringUtil;

/**
 * ...
 * @author d3relict
 */
class DateView extends BasicView implements IView implements IDateModelListener
{
    @Inject
    public var dateModel:IDateModelRO;

    @Inject
    public var controller:IDateController;

    private var modalId:String;

    private var datePicker:InputElement;
    private var timePicker:InputElement;

    private var monthNames:Array<String> = [
        'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
    ];

    public function new(panelId:String)
    {
        super();
        this.modalId = panelId;
    }

    @PostConstruct
    public function init():Void
    {
        var container:DivElement = cast Browser.document.getElementById(modalId).getElementsByClassName('date')[0];
        container.innerHTML = DateSelectorTemplate.generate({modalId:modalId});

        datePicker = cast Browser.document.getElementById(modalId + '-datePicker');
        datePicker.onchange = update;
        untyped JQuery(datePicker).pickadate({
            format: 'dd mmmm, yyyy',
            clear: false,
            selectMonths: true,
            selectYears: 5
        });

        timePicker = cast Browser.document.getElementById(modalId + '-timePicker');
        timePicker.onchange = update;
        untyped JQuery(timePicker).pickatime({
            autoclose: false,
            twelvehour: false
        });

        dateModel.addListener(this);
    }

    private function update():Void
    {
        var year:UInt = cast datePicker.value.split(' ')[2];
        var month:UInt = monthNames.indexOf(datePicker.value.split(' ')[1].split(',')[0]);
        var day:UInt = cast datePicker.value.split(' ')[0];
        var hour:UInt = cast timePicker.value.split(':')[0];
        var minutes:UInt = cast timePicker.value.split(':')[1];

        controller.setDate(new Date(year, month, day, hour, minutes, 0));
    }

    /* INTERFACE module.ui.flightpanel.model.interfaces.IFlightModelListener */

    public function onSetDate():Void
    {
        datePicker.value = dateModel.date.getDate() + ' ' + monthNames[dateModel.date.getMonth()] + ', ' + dateModel.date.getFullYear();
        timePicker.value = StringUtil.leadingZero(dateModel.date.getHours(), 2) + ':' + StringUtil.leadingZero(dateModel.date.getMinutes(), 2);
    }
}
