package module.ui.modal.common.view;

import haxe.Template;
import hex.view.BasicView;
import hex.view.IView;
import js.Browser;
import js.html.DivElement;
import js.html.Element;
import js.html.InputElement;
import module.ui.modal.common.controller.interfaces.ICapacityController;
import module.ui.modal.common.model.interfaces.ICapacityModelListener;
import module.ui.modal.common.model.interfaces.ICapacityModelRO;
import module.ui.modal.common.model.interfaces.IEditModelListener;
import module.ui.modal.common.model.interfaces.IEditModelRO;
import module.ui.modal.common.template.CapacitySliderTemplate;

/**
 * ...
 * @author d3relict
 */
class CapacityView extends BasicView
    implements IView
    implements ICapacityModelListener
    implements IEditModelListener
{
    private var FLOAT_PRECISION(default, never):Float = 0.01;

    @Inject
    public var capacityModel:ICapacityModelRO;

    @Inject
    @Optional(true)
    public var editModel:IEditModelRO;

    @Inject
    public var controller:ICapacityController;

    private var modalId:String;

    private var container:DivElement;
    private var capacitySlider:Element;
    private var capacityInput:InputElement;

    private var capacitySliderDragging:Bool = false;

    public function new(modalId:String)
    {
        super();
        this.modalId = modalId;
    }

    @PostConstruct
    public function init():Void
    {
        container = cast Browser.document.getElementById(modalId).getElementsByClassName('capacity')[0];
        container.innerHTML = CapacitySliderTemplate.generate({modalId:modalId});

        capacitySlider = cast Browser.document.getElementById(modalId + '-capacitySlider');
        resetCapacitySlider();

        capacityInput  = cast Browser.document.getElementById(modalId + '-capacityInput');
        capacityInput.onchange = onCapacityInputChange;

        capacityModel.addListener(this);
        onSetCapacity();

        if (editModel != null)
            editModel.addListener(this);

        onEditingChange();
    }

    private function resetCapacitySlider():Void
    {
        destroySlider(capacitySlider);

        untyped noUiSlider.create(
            capacitySlider,
            {
                start: capacityModel.capacity,
                step: 1,
                range: {
                    min: capacityModel.minCapacity,
                    max: capacityModel.maxCapacity
                }
            }
        );

        untyped capacitySlider.noUiSlider.on('slide', onCapacitySliderSlide);
    }

    private function destroySlider(slider:Element):Void
    {
        if (untyped slider.noUiSlider != undefined)
        {
            untyped slider.noUiSlider.destroy();
        }
    }

    private function onCapacityInputChange():Void
    {
        controller.setCapacity(Std.parseInt(capacityInput.value));
    }

    private function onCapacitySliderSlide():Void
    {
        capacitySliderDragging = true;
        controller.setCapacity(Std.parseInt(untyped capacitySlider.noUiSlider.get()));
    }

    // ICapacityModelListener

    public function onSetCapacity():Void
    {
        capacityInput.value = cast capacityModel.capacity;
        if (!capacitySliderDragging)
        {
            untyped capacitySlider.noUiSlider.set(capacityModel.capacity);
        }
        capacitySliderDragging = false;
    }

    public function onSetMaxCapacity():Void
    {
        capacityInput.max = cast capacityModel.maxCapacity;
        resetCapacitySlider();
    }

    public function onSetMinCapacity():Void
    {
        capacityInput.min = cast capacityModel.minCapacity;
        resetCapacitySlider();
    }

    public function onEditingChange():Void
    {
        var visible:Bool = editModel == null || (editModel != null && editModel.editing == null);
        container.style.cssText = visible ? '' : 'display: none;';
    }
}
