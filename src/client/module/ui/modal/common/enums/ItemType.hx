package module.ui.modal.common.enums;

/**
 * @author d3relict
 */
enum ItemType
{
    aircraft;
    battery;
}
