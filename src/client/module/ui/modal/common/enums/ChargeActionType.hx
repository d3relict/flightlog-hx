package module.ui.modal.common.enums;

/**
 * @author d3relict
 */
enum ChargeActionType
{
    add;
    charge;
    discharge;
    flight;
}
