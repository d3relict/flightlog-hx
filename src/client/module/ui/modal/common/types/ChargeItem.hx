package module.ui.modal.common.types;

/**
 * @author d3relict
 */
typedef ChargeItem =
{
    var batteryId:String;
	var charge:UInt;
    var level:Float;

    var minCharge:UInt;
    var maxCharge:UInt;
    var minLevel:Float;
    var maxLevel:Float;
}
