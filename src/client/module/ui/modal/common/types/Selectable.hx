package module.ui.modal.common.types;

/**
 * @author d3relict
 */
typedef Selectable =
{
    var id:String;
    var name:String;
}
