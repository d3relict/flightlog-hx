package module.ui.modal.common.template;
import haxe.Template;

/**
 * ...
 * @author d3relict
 */
class DateSelectorTemplate
{
    private static var template:String =
        '<div class="input-field col s8">' +
            '<label for="::modalId::-datePicker">Date</label>' +
            '<input type="text" id="::modalId::-datePicker" value="01 January, 2017">' +
        '</div>' +
        '<div class="input-field col s4">' +
            '<label for="::modalId::-timePicker">Time</label>' +
            '<input type="text" id="::modalId::-timePicker" value="12:00">' +
        '</div>';

    public static function generate(context:Dynamic):String
    {
        return new Template(template).execute(context);
    }
}
