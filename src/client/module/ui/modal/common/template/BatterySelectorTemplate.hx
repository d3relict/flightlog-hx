package module.ui.modal.common.template;
import haxe.Template;

/**
 * ...
 * @author d3relict
 */
class BatterySelectorTemplate
{
    private static var template:String =
        '<label for="::modalId::-batterySelect">Select battery</label>' +
        '<select id="::modalId::-batterySelect" multiple></select>';

    public static function generate(context:Dynamic):String
    {
        return new Template(template).execute(context);
    }
}
