package module.ui.modal.common.template;
import haxe.Template;

/**
 * ...
 * @author d3relict
 */
class ModalButtonTemplate
{
    private static var template:String =
        '<button data-id="cancel" class="btn waves-effect waves-light red">Cancel<i class="material-icons right">cancel</i></button>' +
        '<button data-id="submit" class="btn waves-effect waves-light green">Submit<i class="material-icons right">send</i></button>';

    public static function generate(context:Dynamic):String
    {
        return new Template(template).execute(context);
    }
}
