package module.ui.modal.common.template;
import haxe.Template;

/**
 * ...
 * @author d3relict
 */
class AircraftSelectorTemplate
{
    private static var template:String =
        '<label for="::modalId::-modelSelect">Select model</label>' +
        '<select id="::modalId::-modelSelect"></select>';

    public static function generate(context:Dynamic):String
    {
        return new Template(template).execute(context);
    }
}
