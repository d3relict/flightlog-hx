package module.ui.modal.common.template;
import haxe.Template;

/**
 * ...
 * @author d3relict
 */
class ClassificationInputTemplate
{
    private static var template:String =
        '<div class="input-field col s12">' +
            '<label for="::modalId::-manufacturerInput">Manufacturer (required)</label>' +
            '<input type="text" id="::modalId::-manufacturerInput" class="autocomplete" required>' +
        '</div>' +
        '<div class="input-field col s12">' +
            '<label for="::modalId::-typeInput">Type (required)</label>' +
            '<input type="text" id="::modalId::-typeInput" class="autocomplete" required>' +
        '</div>' +
        '<div class="input-field col s12">' +
            '<label for="::modalId::-nameInput">' +
                '::if (itemType == "aircraft")::     Name' +
                '::elseif (itemType == "battery")::  Number (required)' +
                '::else::                            Serial (required)' +
                '::end::' +
            '</label>' +
            '<input type="text" id="::modalId::-nameInput">' +
        '</div>';

    public static function generate(context:Dynamic):String
    {
        return new Template(template).execute(context);
    }
}
