package module.ui.modal.common.template;
import haxe.Template;

/**
 * ...
 * @author d3relict
 */
class ChargeGroupTemplate
{
    private static var template:String =
        '::foreach items::' +
            '<div class="range-field col s12 ::if levelOnly::hide::end::"><h6>::name::</h6></div>' +
            '<div class="range-field col s9 l10 ::if levelOnly::hide::end::">' +
                '<label for="::modalId::-chargeSlider-::id::">Used charge (mAh)</label>' +
                '<div id="::modalId::-chargeSlider-::id::" class="nouislider"></div>' +
            '</div>' +
            '<div class="input-field col s3 l2 ::if levelOnly::hide::end::">' +
                '<input id="::modalId::-chargeInput-::id::" type="number" value="0">' +
            '</div>' +
            '<div class="range-field col s9 l10">' +
                '<label for="::modalId::-levelSlider-::id::">Measured level (percentage)</label>' +
                '<div id="::modalId::-levelSlider-::id::" class="nouislider"></div>' +
            '</div>' +
            '<div class="input-field col s3 l2">' +
                '<input id="::modalId::-levelInput-::id::" type="number" value="0" min="0" max="100">' +
            '</div>' +
        '::end::' +
        '<div class="range-field col s12 ::if levelOnly::hide::end::">' +
           '<div class="switch">' +
                '<label>' +
                    'Lock mAh to Percentage: Off' +
                    '<input id="::modalId::-lockCheck" type="checkbox">' +
                    '<span class="lever"></span>' +
                    'On' +
                '</label>' +
            '</div>' +
        '</div>';

    public static function generate(context:Dynamic):String
    {
        return new Template(template).execute(context);
    }
}
