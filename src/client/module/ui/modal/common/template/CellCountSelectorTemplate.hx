package module.ui.modal.common.template;
import haxe.Template;

/**
 * ...
 * @author d3relict
 */
class CellCountSelectorTemplate
{
    private static var template:String =
        '<div class="range-field col s12">' +
            '<label for="::modalId::-cellCountSelect">Cell count</label>' +
            '<select id="::modalId::-cellCountSelect"></select>' +
        '</div>';

    public static function generate(context:Dynamic):String
    {
        return new Template(template).execute(context);
    }
}
