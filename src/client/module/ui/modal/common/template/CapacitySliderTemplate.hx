package module.ui.modal.common.template;
import haxe.Template;

/**
 * ...
 * @author d3relict
 */
class CapacitySliderTemplate
{
    private static var template:String =
        '<div class="range-field col s9 l10">' +
            '<label for="::modalId::-capacitySlider">Capacity (mAh)</label>' +
            '<div id="::modalId::-capacitySlider" class="nouislider"></div>' +
        '</div>' +
        '<div class="input-field col s3 l2">' +
            '<input id="::modalId::-capacityInput" type="number" value="0">' +
        '</div>';

    public static function generate(context:Dynamic):String
    {
        return new Template(template).execute(context);
    }
}
