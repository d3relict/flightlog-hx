package module.ui.modal.common.template;
import haxe.Template;

/**
 * ...
 * @author d3relict
 */
class AssociationSelectorTemplate
{
    private static var template:String =
        '<div class="range-field col s12">' +
            '<label for="::modalId::-associationSelect">Association</label>' +
            '<select id="::modalId::-associationSelect" multiple></select>' +
        '</div>';

    public static function generate(context:Dynamic):String
    {
        return new Template(template).execute(context);
    }
}
