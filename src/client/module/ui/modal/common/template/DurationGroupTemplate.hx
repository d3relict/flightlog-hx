package module.ui.modal.common.template;
import haxe.Template;

/**
 * ...
 * @author d3relict
 */
class DurationGroupTemplate
{
    private static var template:String =
        '<div class="range-field col s12 m8">' +
            '<label for="::modalId::-durationSlider">Duration</label>' +
            '<div id="::modalId::-durationSlider" class="nouislider"></div>' +
        '</div>' +
        '<div class="input-field col s6 m2">' +
            '<label for="::modalId::-minuteInput">Minutes</label>' +
            '<input type="number" id="::modalId::-minuteInput" value="05" min="0" max="59">' +
        '</div>' +
        '<div class="input-field col s6 m2">' +
            '<label for="::modalId::-secondInput">Seconds</label>' +
            '<input type="number" id="::modalId::-secondInput" value="00" min="0" max="59">' +
        '</div>';

    public static function generate(context:Dynamic):String
    {
        return new Template(template).execute(context);
    }
}
