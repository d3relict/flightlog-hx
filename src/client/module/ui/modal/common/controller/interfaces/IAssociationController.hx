package module.ui.modal.common.controller.interfaces;

/**
 * @author d3relict
 */
interface IAssociationController
{
    function setAssociation(values:Array<String>):Void;
}
