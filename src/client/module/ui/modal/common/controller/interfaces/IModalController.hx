package module.ui.modal.common.controller.interfaces;

/**
 * @author d3relict
 */
interface IModalController 
{
    function open():Void;
    function close():Void;
    function init():Void;
    function update():Void;
    function submit():Void;
}
