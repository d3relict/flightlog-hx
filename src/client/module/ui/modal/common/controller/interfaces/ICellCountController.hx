package module.ui.modal.common.controller.interfaces;

/**
 * @author d3relict
 */
interface ICellCountController
{
    function setCellCount(count:UInt):Void;
}
