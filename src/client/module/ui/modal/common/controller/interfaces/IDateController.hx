package module.ui.modal.common.controller.interfaces;

/**
 * @author d3relict
 */
interface IDateController 
{
    function setDate(date:Date):Void;
}
