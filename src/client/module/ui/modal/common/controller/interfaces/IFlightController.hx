package module.ui.modal.common.controller.interfaces;

/**
 * @author d3relict
 */
interface IFlightController 
{
    function selectAircraft(aircraftId:String):Void;
    function setDuration(duration:UInt):Void;
}
