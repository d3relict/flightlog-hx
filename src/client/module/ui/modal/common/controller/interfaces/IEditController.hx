package module.ui.modal.common.controller.interfaces;

/**
 * @author d3relict
 */
interface IEditController
{
    function edit(id:String):Void;
}
