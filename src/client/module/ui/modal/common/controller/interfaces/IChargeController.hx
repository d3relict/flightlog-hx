package module.ui.modal.common.controller.interfaces;
import module.ui.modal.common.enums.ChargeActionType;

/**
 * @author d3relict
 */
interface IChargeController
{
    function setActionType(type:ChargeActionType):Void;
    function selectBatteries(ids:Array<String>):Void;
    function setCharge(id:String, charge:UInt):Void;
    function setLevel(id:String, level:Float):Void;
    function setLocked(locked:Bool):Void;
    function setCellCount(count:UInt):Void;
}
