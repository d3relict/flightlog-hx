package module.ui.modal.common.controller.interfaces;

/**
 * @author d3relict
 */
interface ICapacityController
{
    function setCapacity(value:UInt):Void;
}
