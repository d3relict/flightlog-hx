package module.ui.modal.common.controller.interfaces;

/**
 * @author d3relict
 */
interface IClassificationController
{
    function setManufacturer(value:String):Void;
    function setType(value:String):Void;
    function setName(value:String):Void;
}
