package module.ui.modal.common.controller.interfaces;

/**
 * @author d3relict
 */
interface IFormController 
{
    function validate():Void;
}