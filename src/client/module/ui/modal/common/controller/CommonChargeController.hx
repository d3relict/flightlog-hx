package module.ui.modal.common.controller;
import const.EventTypes;
import hex.di.IInjectorContainer;
import interfaces.logic.ISettingsProvider;
import module.ui.modal.common.controller.interfaces.*;
import module.ui.modal.common.enums.ChargeActionType;
import module.ui.modal.common.model.interfaces.*;
import types.complex.battery.Battery;

/**
 * ...
 * @author d3relict
 */
class CommonChargeController
    implements IInjectorContainer
    implements IChargeController
{
    @Inject
    public var chargeModel:IChargeModel;
    @Inject @Optional(true)
    public var batteryModel:IBatteryModel;
    @Inject @Optional(true)
    public var settings:ISettingsProvider;

    public function new()
    {

    }

    public function setActionType(type:ChargeActionType):Void
    {
        chargeModel.action = type;
    }

    public function selectBatteries(ids:Array<String>):Void
    {
        var i:UInt = 0;
        var idPosition:Int;
        while (i < chargeModel.items.length)
        {
            idPosition = ids.indexOf(chargeModel.items[i].batteryId);
            if (idPosition == -1)
            {
                chargeModel.removeItem(chargeModel.items[i].batteryId);
            }
            else
            {
                ids.splice(idPosition, 1);
                i++;
            }
        }

        var battery:Battery = null;
        var level:Float     = 0;
        var minLevel:Float  = 0;
        var maxLevel:Float  = 0;
        var charge:UInt     = 0;
        var minCharge:UInt  = 0;
        var maxCharge:UInt  = 0;

        for (id in ids)
        {
            battery = batteryModel != null ? batteryModel.getBatteryById(id) : null;

            level     = 0;
            minLevel  = 0;
            maxLevel  = 100;
            charge    = 0;
            minCharge = 0;
            maxCharge = battery != null ? battery.capacity : 100;

            switch (chargeModel.action)
            {
                case ChargeActionType.add:
                    //nothing to do here.

                case ChargeActionType.charge:
                    charge      = getChargeByPercentage(battery, 100);
                    maxCharge   = Math.floor(settings.overChargeMultiplier * (battery.capacity - battery.charge));
                    level       = 100;
                    minLevel    = battery.charge / battery.capacity * 100;

                case ChargeActionType.discharge:
                    charge      = getChargeByPercentage(battery, settings.defaultDischargeLevel);
                    maxCharge   = battery.charge;
                    level       = settings.defaultDischargeLevel;
                    maxLevel    = battery.charge / battery.capacity * 100;

                case ChargeActionType.flight:
                    charge      = getChargeByPercentage(battery, settings.defaultPostFlightLevel);
                    maxCharge   = battery.charge;
                    level       = settings.defaultPostFlightLevel;
                    maxLevel    = battery.charge / battery.capacity * 100;
            }

            chargeModel.addItem(id, charge, minCharge, maxCharge, level, minLevel, maxLevel);
        }
    }

    public function setCharge(batteryId:String, charge:UInt):Void
    {
        var battery:Battery = batteryModel != null ? batteryModel.getBatteryById(batteryId) : null;

        chargeModel.setCharge(batteryId, charge);
        if (chargeModel.locked)
            chargeModel.setLevel(batteryId, getPercentageByCharge(battery, charge));
    }

    public function setLevel(batteryId:String, level:Float):Void
    {
        var battery:Battery = batteryModel != null ? batteryModel.getBatteryById(batteryId) : null;

        chargeModel.setLevel(batteryId, level);
        if (chargeModel.locked)
            chargeModel.setCharge(batteryId, getChargeByPercentage(battery, level));
    }

    public function setLocked(locked:Bool):Void
    {
        chargeModel.locked = locked;
        if (chargeModel.locked)
            for (item in chargeModel.items)
                chargeModel.setCharge(item.batteryId, getChargeByPercentage(batteryModel.getBatteryById(item.batteryId), item.level));
    }

    public function setCellCount(count:UInt):Void
    {
        chargeModel.cellCount = count;
    }

    private function getPercentageByCharge(battery:Battery, charge:UInt):Float
    {
        return Math.min(100, switch (chargeModel.action) {
            case ChargeActionType.add:
                0;
            case ChargeActionType.charge:
                (battery.charge + charge) / battery.capacity * 100;
            case ChargeActionType.discharge:
                (battery.charge - charge) / battery.capacity * 100;
            case ChargeActionType.flight:
                (battery.charge - charge) / battery.capacity * 100;
        });
    }

    private function getChargeByPercentage(battery:Battery, level:Float):UInt
    {
        return switch (chargeModel.action) {
            case ChargeActionType.add:
                0;
            case ChargeActionType.charge:
                Math.round(battery.capacity * level / 100 - battery.charge);
            case ChargeActionType.discharge:
                Math.round(battery.charge - battery.capacity * level / 100);
            case ChargeActionType.flight:
                Math.round(battery.charge - battery.capacity * level / 100);
        }
    }
}
