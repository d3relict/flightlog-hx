package module.ui.modal.common.message;
import hex.event.MessageType;

/**
 * ...
 * @author d3relict
 */
class ModalMessage
{
    public static var AVAILABILITY(default, never):MessageType =    new MessageType('availability');
    public static var SUBMIT(default, never):MessageType =          new MessageType('submit');
}
