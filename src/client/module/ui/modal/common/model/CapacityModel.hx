package module.ui.modal.common.model;

import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.modal.common.model.interfaces.ICapacityModel;
import module.ui.modal.common.model.interfaces.ICapacityModelListener;

/**
 * ...
 * @author d3relict
 */
class CapacityModel extends BasicModel<CapacityModelDispatcher, ICapacityModelListener> implements ICapacityModel
{
    public var capacity(default, set):UInt = 0;
    public var maxCapacity(default, set):UInt = 1000;
    public var minCapacity(default, set):UInt = 0;

    public function new()
    {
        super();
    }

    function set_capacity(value:Int):Int
    {
        capacity = value;
        dispatcher.onSetCapacity();
        return value;
    }

    function set_maxCapacity(value:Int):Int
    {
        maxCapacity = value;
        dispatcher.onSetMaxCapacity();
        return value;
    }

    function set_minCapacity(value:Int):Int
    {
        minCapacity = value;
        dispatcher.onSetMinCapacity();
        return value;
    }
}

private class CapacityModelDispatcher extends ModelDispatcher<ICapacityModelListener> implements ICapacityModelListener
{
    public function onSetCapacity():Void;
    public function onSetMaxCapacity():Void;
    public function onSetMinCapacity():Void;
}
