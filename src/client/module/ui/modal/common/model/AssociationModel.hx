package module.ui.modal.common.model;

import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.modal.common.model.interfaces.IAssociationModel;
import module.ui.modal.common.model.interfaces.IAssociationModelListener;
import module.ui.modal.common.types.Selectable;

/**
 * ...
 * @author d3relict
 */
class AssociationModel extends BasicModel<AssociationModelDispatcher, IAssociationModelListener> implements IAssociationModel
{
    public var selectItems(default, set):Array<Selectable> = new Array<Selectable>();
    public var selectedIds(default, set):Array<String> = new Array<String>();

    public function new()
    {
        super();
    }

    function set_selectItems(value:Array<Selectable>):Array<Selectable>
    {
        selectItems = value;
        dispatcher.onSetSelectItems();
        return value;
    }

    function set_selectedIds(value:Array<String>):Array<String>
    {
        selectedIds = value;
        dispatcher.onSetSelectedIds();
        return value;
    }
}

private class AssociationModelDispatcher extends ModelDispatcher<IAssociationModelListener> implements IAssociationModelListener
{
    public function onSetSelectedIds():Void;
    public function onSetSelectItems():Void;
}
