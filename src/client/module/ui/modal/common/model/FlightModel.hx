package module.ui.modal.common.model;

import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.modal.common.model.interfaces.IFlightModel;
import module.ui.modal.common.model.interfaces.IFlightModelListener;

/**
 * ...
 * @author d3relict
 */
class FlightModel extends BasicModel<FlightModelDispatcher, IFlightModelListener> implements IFlightModel
{
    public var aircraftId(default, set):String;
    public var duration(default, set):UInt = 0;
    
    public function new() 
    {
        super();
    }
    
    function set_aircraftId(value:String):String 
    {
        aircraftId = value;
        dispatcher.onSelectAircraft();
        return value;
    }
        
    function set_duration(value:UInt):UInt 
    {
        duration = value;
        dispatcher.onSetDuration();
        return value;
    }
}

private class FlightModelDispatcher extends ModelDispatcher<IFlightModelListener> implements IFlightModelListener
{
    public function onSelectAircraft():Void;
    public function onSetDuration():Void;
}
