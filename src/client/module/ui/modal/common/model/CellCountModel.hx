package module.ui.modal.common.model;

import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.modal.common.model.interfaces.ICellCountModel;
import module.ui.modal.common.model.interfaces.ICellCountModelListener;

/**
 * ...
 * @author d3relict
 */
class CellCountModel extends BasicModel<CellCountModelDispatcher, ICellCountModelListener> implements ICellCountModel
{
    public var cellCount(default, set):UInt = 1;

    public function new()
    {
        super();
    }

    function set_cellCount(value:UInt):UInt
    {
        cellCount = value;
        dispatcher.onSetCount();
        return value;
    }
}

private class CellCountModelDispatcher extends ModelDispatcher<ICellCountModelListener> implements ICellCountModelListener
{
    public function onSetCount():Void;
}
