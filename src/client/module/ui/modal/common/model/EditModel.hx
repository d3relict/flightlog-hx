package module.ui.modal.common.model;

import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.modal.common.model.interfaces.IEditModel;
import module.ui.modal.common.model.interfaces.IEditModelListener;

/**
 * ...
 * @author d3relict
 */
class EditModel extends BasicModel<EditModelDispatcher, IEditModelListener> implements IEditModel
{
    public var editing(default, set):String = null;

    public function new()
    {
        super();
    }

    function set_editing(value)
    {
        editing = value;
        dispatcher.onEditingChange();
        return value;
    }
}

private class EditModelDispatcher extends ModelDispatcher<IEditModelListener> implements IEditModelListener
{
    public function onEditingChange():Void;
}
