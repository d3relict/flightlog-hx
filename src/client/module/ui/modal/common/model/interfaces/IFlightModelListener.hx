package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface IFlightModelListener 
{
    function onSelectAircraft():Void;
    function onSetDuration():Void;
}
