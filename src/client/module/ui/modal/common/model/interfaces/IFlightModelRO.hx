package module.ui.modal.common.model.interfaces;
import hex.model.IModelRO;
import module.ui.modal.common.model.interfaces.IFlightModelListener;

/**
 * @author d3relict
 */
interface IFlightModelRO extends IModelRO<IFlightModelListener>
{
    var aircraftId(default, never):String;
    var duration(default, never):UInt;
}
