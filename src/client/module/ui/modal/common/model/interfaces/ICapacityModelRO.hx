package module.ui.modal.common.model.interfaces;
import hex.model.IModelRO;
import module.ui.modal.common.model.interfaces.ICapacityModelListener;

/**
 * @author d3relict
 */
interface ICapacityModelRO extends IModelRO<ICapacityModelListener>
{
    var capacity(default, never):UInt;
    var maxCapacity(default, never):UInt;
    var minCapacity(default, never):UInt;
}
