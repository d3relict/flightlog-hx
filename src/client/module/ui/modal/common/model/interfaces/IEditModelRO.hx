package module.ui.modal.common.model.interfaces;
import hex.model.IModelRO;
import module.ui.modal.common.model.interfaces.IModalModelListener;

/**
 * @author d3relict
 */
interface IEditModelRO extends IModelRO<IEditModelListener>
{
    var editing(default, never):String;
}
