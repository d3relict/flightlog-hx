package module.ui.modal.common.model.interfaces;
import types.complex.aircraft.AircraftList;

/**
 * @author d3relict
 */
interface IAircraftModel extends IAircraftModelRO
{
    var aircrafts(default, set):AircraftList;
}
