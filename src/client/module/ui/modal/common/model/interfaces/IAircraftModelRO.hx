package module.ui.modal.common.model.interfaces;
import hex.model.IModelRO;
import module.ui.modal.common.model.interfaces.IAircraftModelListener;
import types.complex.aircraft.Aircraft;
import types.complex.aircraft.AircraftList;

/**
 * @author d3relict
 */
interface IAircraftModelRO extends IModelRO<IAircraftModelListener>
{
    var aircrafts(default, never):AircraftList;

    function getAircraftById(id:String):Aircraft;
}
