package module.ui.modal.common.model.interfaces;
import hex.model.IModelRO;
import module.ui.modal.common.model.interfaces.IDateModelListener;

/**
 * @author d3relict
 */
interface IDateModelRO extends IModelRO<IDateModelListener>
{
    var date(default, never):Date;
}
