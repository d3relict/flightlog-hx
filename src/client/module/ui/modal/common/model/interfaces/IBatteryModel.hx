package module.ui.modal.common.model.interfaces;
import types.complex.battery.BatteryList;

/**
 * @author d3relict
 */
interface IBatteryModel extends IBatteryModelRO
{
    var batteries(get, set):BatteryList;
}
