package module.ui.modal.common.model.interfaces;
import hex.model.IModelRO;
import module.ui.modal.common.model.interfaces.IAssociationModelListener;
import module.ui.modal.common.types.Selectable;

/**
 * @author d3relict
 */
interface IAssociationModelRO extends IModelRO<IAssociationModelListener>
{
    var selectItems(default, never):Array<Selectable>;
    var selectedIds(default, never):Array<String>;
}
