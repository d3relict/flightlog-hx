package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface IModalModelListener 
{
    function onStateChange(open:Bool):Void;
    function onSetSubmittable():Void;
}
