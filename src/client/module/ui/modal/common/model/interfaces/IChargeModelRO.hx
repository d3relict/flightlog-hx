package module.ui.modal.common.model.interfaces;
import hex.model.IModelRO;
import module.ui.modal.common.enums.ChargeActionType;
import module.ui.modal.common.model.interfaces.IChargeModelListener;
import module.ui.modal.common.types.ChargeItem;

/**
 * @author d3relict
 */
interface IChargeModelRO extends IModelRO<IChargeModelListener>
{
    var action          (default, null):ChargeActionType;
    var locked          (default, null):Bool;
    var cellCount       (default, null):UInt;
    var items           (default, null):Array<ChargeItem>;

    function getItem    (id:String):ChargeItem;
}
