package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface IFlightModel extends IFlightModelRO
{
    var aircraftId(default, set):String;
    var duration(default, set):UInt;
}
