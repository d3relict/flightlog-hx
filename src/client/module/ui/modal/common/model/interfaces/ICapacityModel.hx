package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface ICapacityModel extends ICapacityModelRO
{
    var capacity(default, set):UInt;
    var maxCapacity(default, set):UInt;
    var minCapacity(default, set):UInt;
}
