package module.ui.modal.common.model.interfaces;
import module.ui.modal.common.enums.ChargeActionType;

/**
 * @author d3relict
 */
interface IChargeModel extends IChargeModelRO
{
    var action          (default, default):ChargeActionType;
    var locked          (default, set):Bool;
    var cellCount       (default, default):UInt;

    function addItem    (id:String, charge:UInt, minCharge:UInt, maxCharge:UInt, level:Float, minLevel:Float, maxLevel:Float):Void;
    function removeItem (id:String):Void;

    function setLevel   (id:String, level:Float):Void;
    function setCharge  (id:String, charge:UInt):Void;
}
