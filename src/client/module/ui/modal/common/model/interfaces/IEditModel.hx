package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface IEditModel extends IEditModelRO
{
    var editing(default, set):String;
}
