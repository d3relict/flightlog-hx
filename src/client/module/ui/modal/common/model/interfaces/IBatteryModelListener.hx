package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface IBatteryModelListener 
{
    function onUpdate():Void;
}
