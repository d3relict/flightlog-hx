package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface ICellCountModelListener
{
    function onSetCount():Void;
}
