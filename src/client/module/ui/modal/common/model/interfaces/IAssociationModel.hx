package module.ui.modal.common.model.interfaces;
import module.ui.modal.common.types.Selectable;

/**
 * @author d3relict
 */
interface IAssociationModel extends IAssociationModelRO
{
    var selectItems(default, set):Array<Selectable>;
    var selectedIds(default, set):Array<String>;
}
