package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface IClassificationModelListener
{
    function onSetManufacturer():Void;
    function onSetType():Void;
    function onSetName():Void;

    function onSetKnownManufacturers():Void;
    function onSetKnownTypes():Void;
}
