package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface IDateModel extends IDateModelRO
{
    var date(default, set):Date;
}
