package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface IDateModelListener 
{
    function onSetDate():Void;
}
