package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface IModalModel extends IModalModelRO
{
    var panelId(default, set):String;
    var open(default, set):Bool;
    var available(default, set):Bool;
    var submittable(default, set):Bool;
}
