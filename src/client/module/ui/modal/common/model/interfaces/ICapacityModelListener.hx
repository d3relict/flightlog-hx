package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface ICapacityModelListener
{
    function onSetCapacity():Void;
    function onSetMaxCapacity():Void;
    function onSetMinCapacity():Void;
}
