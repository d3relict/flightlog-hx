package module.ui.modal.common.model.interfaces;
import types.complex.battery.BatteryList;

/**
 * @author d3relict
 */
interface ICellCountModel extends ICellCountModelRO
{
    var cellCount(default, set):UInt;
}
