package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface IChargeModelListener
{
    function onSetLocked():Void;

    function onCountChange():Void;

    function onSetCharge(id:String):Void;
    function onSetLevel(id:String):Void;
}
