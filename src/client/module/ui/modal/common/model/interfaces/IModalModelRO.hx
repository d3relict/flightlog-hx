package module.ui.modal.common.model.interfaces;
import hex.model.IModelRO;
import module.ui.modal.common.model.interfaces.IModalModelListener;

/**
 * @author d3relict
 */
interface IModalModelRO extends IModelRO<IModalModelListener>
{
    var panelId(default, never):String;
    var open(default, never):Bool;
    var available(default, never):Bool;
    var submittable(default, never):Bool;
}
