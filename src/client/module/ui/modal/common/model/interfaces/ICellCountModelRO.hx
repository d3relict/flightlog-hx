package module.ui.modal.common.model.interfaces;
import hex.model.IModelRO;
import module.ui.modal.common.model.interfaces.IBatteryModelListener;
import types.complex.battery.Battery;
import types.complex.battery.BatteryList;

/**
 * @author d3relict
 */
interface ICellCountModelRO extends IModelRO<ICellCountModelListener>
{
    var cellCount(default, null):UInt;
}
