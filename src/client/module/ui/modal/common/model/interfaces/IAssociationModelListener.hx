package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface IAssociationModelListener
{
    function onSetSelectedIds():Void;
    function onSetSelectItems():Void;
}
