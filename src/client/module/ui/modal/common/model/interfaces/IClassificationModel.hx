package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface IClassificationModel extends IClassificationModelRO
{
    var manufacturer(default, set):String;
    var type(default, set):String;
    var name(default, set):String;

    var knownManufacturers(default, set):Array<String>;
    var knownTypes(default, set):Array<String>;
}
