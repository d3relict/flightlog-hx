package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface IAircraftModelListener 
{
    function onUpdate():Void;
}
