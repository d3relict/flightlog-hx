package module.ui.modal.common.model.interfaces;
import hex.model.IModelRO;
import module.ui.modal.common.model.interfaces.IClassificationModelListener;

/**
 * @author d3relict
 */
interface IClassificationModelRO extends IModelRO<IClassificationModelListener>
{
    var manufacturer(default, never):String;
    var type(default, never):String;
    var name(default, never):String;

    var knownManufacturers(default, never):Array<String>;
    var knownTypes(default, never):Array<String>;

}
