package module.ui.modal.common.model.interfaces;

/**
 * @author d3relict
 */
interface IEditModelListener
{
    function onEditingChange():Void;
}
