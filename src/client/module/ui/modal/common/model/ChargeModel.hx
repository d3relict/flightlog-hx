package module.ui.modal.common.model;

import hex.error.Exception;
import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.modal.common.enums.ChargeActionType;
import module.ui.modal.common.model.interfaces.IChargeModel;
import module.ui.modal.common.model.interfaces.IChargeModelListener;
import module.ui.modal.common.types.ChargeItem;

/**
 * ...
 * @author d3relict
 */
class ChargeModel extends BasicModel<ChargeModelDispatcher, IChargeModelListener>
    implements IChargeModel
{
    public var action   (default, default):ChargeActionType;
    public var cellCount(default, default):UInt;
    public var locked   (default, set):Bool = false;
    public var items    (default, null):Array<ChargeItem>;

    public function new()
    {
        items = new Array<ChargeItem>();
        super();
    }

    public function getItem(id:String):ChargeItem
    {
        for (item in items)
            if (item.batteryId == id)
                return item;

        return null;
    }

    private function validateItem(id:String):ChargeItem
    {
        var item:ChargeItem = getItem(id);
        if (item != null)
            return item;

        throw new Exception('id ' + id + ' does not exist');
    }

    public function addItem(id:String, charge:UInt, minCharge:UInt, maxCharge:UInt, level:Float, minLevel:Float, maxLevel:Float)
    {
        if (getItem(id) == null)
        {
            items.push({
                batteryId:  id,
                charge:     charge,
                minCharge:  minCharge,
                maxCharge:  maxCharge,
                level:      level,
                minLevel:   minLevel,
                maxLevel:   maxLevel
            });
            dispatcher.onCountChange();
        }
        else
        {
            throw new Exception('id ' + id + ' already exists');
        }
    }

    public function removeItem(id:String)
    {
        var item:ChargeItem = validateItem(id);
        items.remove(item);
        dispatcher.onCountChange();
    }

    public function setLevel(id:String, level:Float)
    {
        validateItem(id).level = level;
        dispatcher.onSetLevel(id);
    }

    public function setCharge(id:String, charge:UInt)
    {
        validateItem(id).charge = charge;
        dispatcher.onSetCharge(id);
    }

    function set_locked(value:Bool):Bool
    {
        locked = value;
        dispatcher.onSetLocked();
        return value;
    }
}

private class ChargeModelDispatcher extends ModelDispatcher<IChargeModelListener> implements IChargeModelListener
{
    public function onSetLocked():Void;
    public function onCountChange():Void;
    public function onSetCharge(id:String):Void;
    public function onSetLevel(id:String):Void;
}
