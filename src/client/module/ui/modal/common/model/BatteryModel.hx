package module.ui.modal.common.model;

import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.modal.common.model.interfaces.IBatteryModel;
import module.ui.modal.common.model.interfaces.IBatteryModelListener;
import types.complex.battery.Battery;
import types.complex.battery.BatteryList;

/**
 * ...
 * @author d3relict
 */
class BatteryModel extends BasicModel<BatteryModelDispatcher, IBatteryModelListener> implements IBatteryModel
{
    @:isVar public var batteries(get, set):BatteryList = new BatteryList();

    public function new()
    {
        super();
    }

    function get_batteries()
    {
        return batteries;
    }

    function set_batteries(value)
    {
        batteries = value;
        dispatcher.onUpdate();
        return value;
    }

    public function getBatteryById(id:String):Battery
    {
        for (battery in batteries)
        {
            if (battery.id == id)
            {
                return battery;
            }
        }
        return null;
    }
}

private class BatteryModelDispatcher extends ModelDispatcher<IBatteryModelListener> implements IBatteryModelListener
{
    public function onUpdate():Void;
}
