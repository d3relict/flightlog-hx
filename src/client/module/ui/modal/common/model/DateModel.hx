package module.ui.modal.common.model;

import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.modal.common.model.interfaces.IDateModel;
import module.ui.modal.common.model.interfaces.IDateModelListener;

/**
 * ...
 * @author d3relict
 */
class DateModel extends BasicModel<DateModelDispatcher, IDateModelListener> implements IDateModel
{
    public var date(default, set):Date;
    
    public function new() 
    {
        super();
    }
        
    function set_date(value:Date):Date 
    {
        date = value;
        dispatcher.onSetDate();
        return value;
    }
    
}

private class DateModelDispatcher extends ModelDispatcher<IDateModelListener> implements IDateModelListener
{
    public function onSetDate():Void;
}
