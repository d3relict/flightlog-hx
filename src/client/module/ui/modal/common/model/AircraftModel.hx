package module.ui.modal.common.model;

import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.modal.common.model.interfaces.IAircraftModel;
import module.ui.modal.common.model.interfaces.IAircraftModelListener;
import types.complex.aircraft.Aircraft;
import types.complex.aircraft.AircraftList;

/**
 * ...
 * @author d3relict
 */
class AircraftModel extends BasicModel<AircraftModelDispatcher, IAircraftModelListener> implements IAircraftModel
{
    public var aircrafts(default, set):AircraftList = new AircraftList();

    public function new()
    {
        super();
    }

    function set_aircrafts(value)
    {
        aircrafts = value;
        dispatcher.onUpdate();
        return value;
    }


    public function getAircraftById(id:String):Aircraft
    {
        for (aircraft in aircrafts)
        {
            if (aircraft.id == id)
            {
                return aircraft;
            }
        }
        return null;
    }
}

private class AircraftModelDispatcher extends ModelDispatcher<IAircraftModelListener> implements IAircraftModelListener
{
    public function onUpdate():Void;
}
