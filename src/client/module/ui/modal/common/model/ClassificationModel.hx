package module.ui.modal.common.model;

import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.modal.common.model.interfaces.IClassificationModel;
import module.ui.modal.common.model.interfaces.IClassificationModelListener;

/**
 * ...
 * @author d3relict
 */
class ClassificationModel extends BasicModel<ClassificationModelDispatcher, IClassificationModelListener> implements IClassificationModel
{
    public var manufacturer(default, set):String;
    public var type(default, set):String;
    public var name(default, set):String;

    public var knownManufacturers(default, set):Array<String> = new Array<String>();
    public var knownTypes(default, set):Array<String> =         new Array<String>();

    public function new()
    {
        super();
    }

    function set_manufacturer(value:String):String
    {
        manufacturer = value;
        dispatcher.onSetManufacturer();
        return value;
    }

    function set_type(value:String):String
    {
        type = value;
        dispatcher.onSetType();
        return value;
    }

    function set_name(value:String):String
    {
        name = value;
        dispatcher.onSetName();
        return value;
    }

    function set_knownManufacturers(value:Array<String>):Array<String>
    {
        knownManufacturers = value;
        dispatcher.onSetKnownManufacturers();
        return value;
    }

    function set_knownTypes(value:Array<String>):Array<String>
    {
        knownTypes = value;
        dispatcher.onSetKnownTypes();
        return value;
    }
}

private class ClassificationModelDispatcher extends ModelDispatcher<IClassificationModelListener> implements IClassificationModelListener
{
    public function onSetManufacturer():Void;
    public function onSetType():Void;
    public function onSetName():Void;

    public function onSetKnownManufacturers():Void;
    public function onSetKnownTypes():Void;
}
