package module.ui.modal.common.model;

import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.modal.common.model.interfaces.IModalModel;
import module.ui.modal.common.model.interfaces.IModalModelListener;

/**
 * ...
 * @author d3relict
 */
class ModalModel extends BasicModel<PanelModelDispatcher, IModalModelListener> implements IModalModel
{
    public var open(default, set):Bool = false;
    public var available(default, set):Bool = false;
    public var panelId(default, set):String;
    public var submittable(default, set):Bool = true;
    
    public function new() 
    {
        super();
    }
    
    function set_open(value)
    {
        open = value;
        dispatcher.onStateChange(open);
        return value;
    }
    
    function set_available(value)
    {
        available = value;
        return value;
    }
    
    function set_panelId(value)
    {
        panelId = value;
        return value;
    }
    
    function set_submittable(value)
    {
        submittable = value;
        dispatcher.onSetSubmittable();
        return value;
    }
}

private class PanelModelDispatcher extends ModelDispatcher<IModalModelListener> implements IModalModelListener
{
    public function onStateChange(open:Bool):Void;
    public function onSetSubmittable():Void;
}
