package module.ui.modal.flightmodal.controller;
import const.EventTypes;
import const.TransactionTypes;
import hex.di.IInjectorContainer;
import hex.module.IModule;
import interfaces.logic.IAircraftProvider;
import interfaces.logic.IBatteryProvider;
import interfaces.logic.ISettingsProvider;
import module.ui.modal.common.controller.interfaces.*;
import module.ui.modal.common.enums.ChargeActionType;
import module.ui.modal.common.message.ModalMessage;
import module.ui.modal.common.model.interfaces.*;
import types.complex.aircraft.Aircraft;
import types.complex.battery.Battery;
import types.complex.battery.BatteryList;
import types.complex.event.ChargeList;
import types.complex.transaction.EventTransaction;
import util.StringUtil;

/**
 * ...
 * @author d3relict
 */
class FlightModalController
    implements IModalController
    implements IFlightController
    implements IDateController
    implements IFormController
    implements IInjectorContainer
{
    @Inject
    public var panelModel:IModalModel;
    @Inject
    public var aircraftModel:IAircraftModel;
    @Inject
    public var batteryModel:IBatteryModel;
    @Inject
    public var flightModel:IFlightModel;
    @Inject
    public var dateModel:IDateModel;
    @Inject
    public var chargeModel:IChargeModel;
    @Inject
    public var batteryData:IBatteryProvider;
    @Inject
    public var aircraftData:IAircraftProvider;
    @Inject
    public var settings:ISettingsProvider;
    @Inject
    public var module:IModule;
    @Inject
    public var chargeController:IChargeController;

    public function open():Void
    {
        panelModel.open = true;
    }

    public function close():Void
    {
        panelModel.open = false;
    }

    public function init():Void
    {
        chargeController.setActionType(ChargeActionType.flight);
        chargeController.setLocked(true);

        aircraftModel.aircrafts = [];
        batteryModel.batteries = [];
        chargeModel.locked = true;
        flightModel.duration = 0;

        update();
    }

    public function update():Void
    {
        dateModel.date = Date.now();

        if (checkAvailability())
        {
            aircraftModel.aircrafts = getAvailableAircrafts();
            selectAircraft(aircraftModel.aircrafts[0].id);
        }
    }

    public function submit():Void
    {
        var charges:ChargeList = new ChargeList();
        for (chargeItem in chargeModel.items)
        {
            charges.push({
                id:         chargeItem.batteryId,
                charge:    -chargeItem.charge,
                level:      chargeItem.level
            });
        }

        var transaction:EventTransaction = {
            txType:         TransactionTypes.ADD_EVENT,

            id:             StringUtil.generateId(),
            eventType:      EventTypes.FLIGHT,
            timestamp:      dateModel.date.getTime(),

            flight: {
                id:         flightModel.aircraftId,
                duration:   flightModel.duration
            },

            charges:        charges
        }

        close();

        module.dispatchPublicMessage(ModalMessage.SUBMIT, [transaction]);
    }

    private function checkAvailability():Bool
    {
        panelModel.available = getAvailableAircrafts().length > 0;

        module.dispatchPublicMessage(ModalMessage.AVAILABILITY);

        return panelModel.available;
    }

    private function getAvailableAircrafts():Array<Aircraft>
    {
        var totalCellCount:UInt = 0;
        var aircrafts:Array<Aircraft> = new Array<Aircraft>();
        for (aircraft in aircraftData.aircrafts)
        {
            totalCellCount = 0;

            for (battery in getChargedBatteries(aircraft.id))
                totalCellCount += battery.cellCount;

            if (totalCellCount >= aircraft.cellCount)
                aircrafts.push(aircraft);
        }
        return aircrafts;
    }

    private function getChargedBatteries(aircraftId:String):BatteryList
    {
        var batteries:BatteryList = batteryData.getBatteriesByAssociation(aircraftId);
        return batteries.filter(function(battery){
            return (battery.charge / battery.capacity * 100) >= settings.minChargeToEnableFlight;
        });
    }

    //IFlightController

    public function selectAircraft(aircraftId:String):Void
    {
        var aircraft:Aircraft = aircraftModel.getAircraftById(aircraftId);

        flightModel.aircraftId = aircraftId;

        batteryModel.batteries = getChargedBatteries(aircraftId);

        chargeController.setCellCount(aircraft.cellCount);
        chargeController.selectBatteries(getSelectedBatteries(aircraftId));

        flightModel.duration = aircraft.usualDuration;
    }

    public function getSelectedBatteries(aircraftId:String):Array<String>
    {
        var selectedCellCount:UInt = 0;
        var selectedBatteries:Array<String> = [];
        var i:UInt = 0;

        while (selectedCellCount < chargeModel.cellCount && i < batteryModel.batteries.length)
        {
            selectedBatteries.push(batteryModel.batteries[i].id);
            selectedCellCount += batteryModel.batteries[i].cellCount;
            i++;
        }

        return selectedBatteries;
    }

    public function setDuration(duration:UInt):Void
    {
        flightModel.duration = duration;
    }

    //IDateController

    public function setDate(date:Date):Void
    {
        dateModel.date = date;
    }

    //IFormController

    public function validate():Void
    {
        var selectedCellCount:UInt = 0;

        for (item in chargeModel.items)
            selectedCellCount += batteryModel.getBatteryById(item.batteryId).cellCount;

        panelModel.submittable = selectedCellCount == aircraftModel.getAircraftById(flightModel.aircraftId).cellCount;
    }
}
