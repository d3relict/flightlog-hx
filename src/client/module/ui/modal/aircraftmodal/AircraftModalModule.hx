package module.ui.modal.aircraftmodal;
import hex.config.stateless.StatelessModuleConfig;
import hex.module.IModule;
import hex.module.Module;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.RuntimeDependencies;
import hex.view.IView;
import interfaces.logic.IAircraftProvider;
import interfaces.logic.IBatteryProvider;
import interfaces.ui.IEdit;
import interfaces.ui.IModal;
import module.ui.modal.common.enums.ItemType;
import module.ui.modal.common.view.AssociationView;
import module.ui.modal.common.view.ClassificationView;
import module.ui.modal.aircraftmodal.controller.AircraftModalController;
import module.ui.modal.common.controller.interfaces.IAssociationController;
import module.ui.modal.common.controller.interfaces.IClassificationController;
import module.ui.modal.common.model.AssociationModel;
import module.ui.modal.common.model.interfaces.IAssociationModel;
import module.ui.modal.common.model.ClassificationModel;
import module.ui.modal.common.model.interfaces.IClassificationModel;
import module.ui.modal.aircraftpanel.view.*;
import module.ui.modal.common.controller.interfaces.*;
import module.ui.modal.common.model.*;
import module.ui.modal.common.model.interfaces.*;
import module.ui.modal.common.view.*;
import module.ui.modal.common.view.TitleView;
import module.ui.modal.common.view.interfaces.*;

/**
 * ...
 * @author d3relict
 */
class AircraftModalModule extends Module
    implements IModal
    implements IEdit
    implements IModule
{
    public var available(get, never):Bool;

    private var views:Array<IView> = new Array<IView>();

    public function new(panelId:String, aircraftProvider:IAircraftProvider, batteryProvider:IBatteryProvider)
    {
        super();

        _injector.mapToValue(IAircraftProvider,  aircraftProvider);
        _injector.mapToValue(IBatteryProvider,  batteryProvider);

        views = [
            new ModalView(panelId),
            //TODO: separate template
            new TitleView(panelId, '<h4>::if (editing)::Edit::else::Add::end:: Aircraft</h4>'),
            new ClassificationView(panelId, ItemType.aircraft),
            new AssociationView(panelId),
            new CellCountView(panelId)
        ];

        _addStatelessConfigClasses([AircraftModalModuleConfig]);

        for (view in views) {
            _injector.injectInto(view);
        }

        _get(IModalModel).panelId = panelId;
        _get(IModalModel).available = true;

        _get(IModalController).init();
        _get(IFormController).validate();
    }

    override function _getRuntimeDependencies():IRuntimeDependencies
    {
        return new RuntimeDependencies();
    }

    public function open ():Void
    {
        _get(IModalController).update();
        _get(IModalController).open();
    }

    public function close ():Void
    {
        _get(IModalController).close();
    }

    public function update():Void
    {
        _get(IModalController).update();
    }

    public function edit(id:String):Void
    {
        _get(IModalController).update();
        _get(IEditController).edit(id);
    }

    function get_available():Bool
    {
        return _get(IModalModel).available;
    }
}

private class AircraftModalModuleConfig extends StatelessModuleConfig
{
    override public function configure():Void
    {
        this.mapModel(IModalModel,          ModalModel);
        this.mapModel(IEditModel,           EditModel);
        this.mapModel(IClassificationModel, ClassificationModel);
        this.mapModel(IAssociationModel,    AssociationModel);
        this.mapModel(ICellCountModel,      CellCountModel);

        this.mapController(IModalController,            AircraftModalController);
        this.mapController(IEditController,             AircraftModalController);
        this.mapController(IFormController,             AircraftModalController);
        this.mapController(IClassificationController,   AircraftModalController);
        this.mapController(IAssociationController,      AircraftModalController);
        this.mapController(ICellCountController,        AircraftModalController);
    }
}
