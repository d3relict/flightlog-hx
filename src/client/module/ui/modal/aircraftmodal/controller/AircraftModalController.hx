package module.ui.modal.aircraftmodal.controller;
import const.TransactionTypes;
import hex.di.IInjectorContainer;
import hex.module.IModule;
import interfaces.logic.IAircraftProvider;
import interfaces.logic.IBatteryProvider;
import module.ui.modal.common.model.interfaces.IAssociationModel;
import module.ui.modal.common.model.interfaces.IClassificationModel;
import module.ui.modal.common.controller.interfaces.*;
import module.ui.modal.common.controller.interfaces.IAssociationController;
import module.ui.modal.common.controller.interfaces.IClassificationController;
import module.ui.modal.common.message.ModalMessage;
import module.ui.modal.common.model.interfaces.*;
import module.ui.modal.common.types.Selectable;
import types.complex.aircraft.Aircraft;
import types.complex.transaction.AircraftTransaction;
import types.complex.battery.BatteryList;
import util.StringUtil;

/**
 * ...
 * @author d3relict
 */
class AircraftModalController
    implements IModalController
    implements IEditController
    implements IClassificationController
    implements IAssociationController
    implements IInjectorContainer
    implements IFormController
    implements ICellCountController
{
    @Inject
    public var aircraftProvider:IAircraftProvider;
    @Inject
    public var batteryProvider:IBatteryProvider;
    @Inject
    public var panelModel:IModalModel;
    @Inject
    public var editModel:IEditModel;
    @Inject
    public var classificationModel:IClassificationModel;
    @Inject
    public var associationModel:IAssociationModel;
    @Inject
    public var cellCountModel:ICellCountModel;
    @Inject
    public var module:IModule;

    public function open():Void
    {
        editModel.editing = null;

        classificationModel.manufacturer = null;
        classificationModel.type = null;
        classificationModel.name = null;

        associationModel.selectedIds = [];

        cellCountModel.cellCount = 1;

        panelModel.open = true;
    }

    public function edit(id:String):Void
    {
        editModel.editing = id;

        var aircraft:Aircraft = aircraftProvider.getAircraftById(id);
        classificationModel.manufacturer = aircraft.manufacturer;
        classificationModel.type = aircraft.type;
        classificationModel.name = aircraft.name;

        var batteries:BatteryList = batteryProvider.getBatteriesByAssociation(id);
        var idList:Array<String> = [];
        for (battery in batteries)
        {
            idList.push(battery.id);
        }

        associationModel.selectedIds = idList;
        cellCountModel.cellCount = aircraft.cellCount;

        validate();

        panelModel.open = true;
    }

    public function close():Void
    {
        panelModel.open = false;
    }

    public function init():Void
    {
        update();
    }

    public function update():Void
    {
        var knownManufacturers:Array<String> = new Array<String>();
        for (aircraft in aircraftProvider.aircrafts)
        {
            if (knownManufacturers.indexOf(aircraft.manufacturer) == -1)
            {
                knownManufacturers.push(aircraft.manufacturer);
            }
        }
        classificationModel.knownManufacturers = knownManufacturers;

        var selectItems:Array<Selectable> = new Array<Selectable>();
        for (battery in batteryProvider.batteries)
        {
            selectItems.push({
                id:     battery.id,
                name:   StringUtil.batteryLabel(battery, false)
            });
        }
        associationModel.selectItems = selectItems;
    }

    public function submit():Void
    {
        var aircraftId:String = StringUtil.generateId();
        if (editModel.editing != null)
            aircraftId = editModel.editing;

        var transaction:AircraftTransaction = {
            txType:         TransactionTypes.ADD_AIRCRAFT,

            id:             aircraftId,

            manufacturer:   classificationModel.manufacturer,
            type:           classificationModel.type,
            name:           classificationModel.name,

            cellCount:      cellCountModel.cellCount,

            associations:   associationModel.selectedIds
        }

        module.dispatchPublicMessage(ModalMessage.SUBMIT, [transaction]);

        classificationModel.manufacturer = '';
        classificationModel.type = '';
        classificationModel.name = '';
        associationModel.selectedIds = [];

        close();
    }

    public function validate():Void
    {
        panelModel.submittable = validateString(classificationModel.manufacturer) && validateString(classificationModel.type);
    }

    private function validateString(value:String):Bool
    {
        return value != null && value.length > 0;
    }

    public function setManufacturer(value:String):Void
    {
        classificationModel.manufacturer = value;
        validate();
    }

    public function setType(value:String):Void
    {
        classificationModel.type = value;
        validate();
    }

    public function setName(value:String):Void
    {
        classificationModel.name = value;
    }

    public function setAssociation(values:Array<String>):Void
    {
        associationModel.selectedIds = values;
    }

    public function setCellCount(count:UInt):Void
    {
        cellCountModel.cellCount = count;
    }

}
