package module.ui.modal.batterymodal;
import hex.config.stateless.StatelessModuleConfig;
import hex.module.IModule;
import hex.module.Module;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.RuntimeDependencies;
import hex.view.IView;
import interfaces.logic.IAircraftProvider;
import interfaces.logic.IBatteryProvider;
import interfaces.ui.IEdit;
import interfaces.ui.IModal;
import module.ui.modal.batterymodal.controller.BatteryModalController;
import module.ui.modal.common.controller.CommonChargeController;
import module.ui.modal.common.controller.interfaces.*;
import module.ui.modal.common.enums.ItemType;
import module.ui.modal.common.model.*;
import module.ui.modal.common.model.interfaces.*;
import module.ui.modal.common.view.*;
import module.ui.modal.common.view.interfaces.*;

/**
 * ...
 * @author d3relict
 */
class BatteryModalModule extends Module
    implements IModal
    implements IEdit
    implements IModule
{
    public var available(get, never):Bool;

    private var views:Array<IView> = new Array<IView>();

    public function new(panelId:String, aircraftProvider:IAircraftProvider, batteryProvider:IBatteryProvider)
    {
        super();

        _injector.mapToValue(IAircraftProvider, aircraftProvider);
        _injector.mapToValue(IBatteryProvider,  batteryProvider);

        _addStatelessConfigClasses([BatteryModalModuleConfig]);

        views = [
            new ModalView(panelId),
            new TitleView(panelId, '<h4>::if (editing)::Edit::else::Add::end:: Battery</h4>'),
            new ClassificationView(panelId, ItemType.battery),
            new AssociationView(panelId),
            new CellCountView(panelId),
            new ChargeView(panelId, true),
            new CapacityView(panelId)
        ];

        for (view in views) {
            _injector.injectInto(view);
        }

        _get(IModalModel).panelId = panelId;
        _get(IModalModel).available = true;

        _get(IModalController).init();
        _get(IFormController).validate();
    }

    override function _getRuntimeDependencies():IRuntimeDependencies
    {
        return new RuntimeDependencies();
    }

    public function open ():Void
    {
        _get(IModalController).update();
        _get(IModalController).open();
    }

    public function close ():Void
    {
        _get(IModalController).close();
    }

    public function edit(id:String):Void
    {
        _get(IModalController).update();
        _get(IEditController).edit(id);
    }

    public function update():Void
    {
        _get(IModalController).update();
    }

    function get_available():Bool
    {
        return _get(IModalModel).available;
    }
}

private class BatteryModalModuleConfig extends StatelessModuleConfig
{
    override public function configure():Void
    {
        this.mapModel(IModalModel,          ModalModel);
        this.mapModel(IClassificationModel, ClassificationModel);
        this.mapModel(IAssociationModel,    AssociationModel);
        this.mapModel(IChargeModel,         ChargeModel);
        this.mapModel(ICapacityModel,       CapacityModel);
        this.mapModel(IEditModel,           EditModel);
        this.mapModel(ICellCountModel,      CellCountModel);

        this.mapController(IChargeController,           CommonChargeController);
        this.mapController(IModalController,            BatteryModalController);
        this.mapController(IEditController,             BatteryModalController);
        this.mapController(IFormController,             BatteryModalController);
        this.mapController(IClassificationController,   BatteryModalController);
        this.mapController(IAssociationController,      BatteryModalController);
        this.mapController(ICapacityController,         BatteryModalController);
        this.mapController(ICellCountController,        BatteryModalController);
    }
}
