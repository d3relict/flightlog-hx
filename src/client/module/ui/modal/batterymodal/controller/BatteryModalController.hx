package module.ui.modal.batterymodal.controller;
import const.TransactionTypes;
import hex.di.IInjectorContainer;
import hex.module.IModule;
import interfaces.logic.IAircraftProvider;
import interfaces.logic.IBatteryProvider;
import module.ui.modal.common.controller.interfaces.ICapacityController;
import module.ui.modal.common.enums.ChargeActionType;
import module.ui.modal.common.model.interfaces.IAssociationModel;
import module.ui.modal.common.model.interfaces.IClassificationModel;
import module.ui.modal.common.controller.interfaces.*;
import module.ui.modal.common.message.ModalMessage;
import module.ui.modal.common.model.interfaces.*;
import module.ui.modal.common.types.Selectable;
import types.complex.battery.Battery;
import types.complex.battery.BatteryList;
import types.complex.transaction.BatteryTransaction;
import util.StringUtil;

/**
 * ...
 * @author d3relict
 */
class BatteryModalController
    implements IModalController
    implements IEditController
    implements IClassificationController
    implements IAssociationController
    implements IInjectorContainer
    implements IFormController
    implements ICapacityController
    implements ICellCountController
{
    private var DEFAULT_CAPACITY(default, never):UInt = 3000;
    private var DEFAULT_CHARGE_LEVEL(default, never):Float = 40;

    @Inject
    public var aircaftProvider:IAircraftProvider;
    @Inject
    public var batteryProvider:IBatteryProvider;
    @Inject
    public var panelModel:IModalModel;
    @Inject
    public var editModel:IEditModel;
    @Inject
    public var classificationModel:IClassificationModel;
    @Inject
    public var associationModel:IAssociationModel;
    @Inject
    public var cellCountModel:ICellCountModel;
    @Inject
    public var chargeModel:IChargeModel;
    @Inject
    public var capacityModel:ICapacityModel;
    @Inject
    public var module:IModule;
    @Inject
    public var chargeController:IChargeController;

    public function open():Void
    {
        editModel.editing = null;

        classificationModel.manufacturer = null;
        classificationModel.type = null;
        classificationModel.name = null;

        associationModel.selectedIds = [];

        cellCountModel.cellCount = 1;

        panelModel.open = true;
    }

    public function edit(id:String):Void
    {
        editModel.editing = id;

        var battery:Battery                 = batteryProvider.getBatteryById(id);

        classificationModel.manufacturer    = battery.manufacturer;
        classificationModel.type            = battery.type;
        classificationModel.name            = battery.name;

        associationModel.selectedIds        = battery.associations;
        cellCountModel.cellCount            = battery.cellCount;

        validate();

        panelModel.open = true;
    }

    public function close():Void
    {
        panelModel.open = false;
    }

    public function init():Void
    {
        chargeController.setActionType(ChargeActionType.add);
        chargeController.setLocked(false);
        chargeController.selectBatteries(['new']);
        chargeController.setLevel('new', DEFAULT_CHARGE_LEVEL);

        capacityModel.capacity = DEFAULT_CAPACITY;
        capacityModel.minCapacity = 100;
        capacityModel.maxCapacity = 10000;

        update();
    }

    public function update():Void
    {
        var knownManufacturers:Array<String> = new Array<String>();
        var knownTypes:Array<String> = new Array<String>();
        for (battery in batteryProvider.batteries)
        {
            if (knownManufacturers.indexOf(battery.manufacturer) == -1)
            {
                knownManufacturers.push(battery.manufacturer);
            }
            if (knownTypes.indexOf(battery.type) == -1)
            {
                knownTypes.push(battery.type);
            }
        }
        classificationModel.knownManufacturers =    knownManufacturers;
        classificationModel.knownTypes =            knownTypes;

        var selectItems:Array<Selectable> = new Array<Selectable>();
        for (aircraft in aircaftProvider.aircrafts)
        {
            var selectable:Selectable = {
                id:     aircraft.id,
                name:   StringUtil.aircraftLabel(aircraft)
            }
            selectItems.push(selectable);
        }
        associationModel.selectItems = selectItems;
    }

    public function submit():Void
    {
        var batteryId:String = StringUtil.generateId();
        if (editModel.editing != null)
            batteryId = editModel.editing;

        var transaction:BatteryTransaction = {
            txType:         TransactionTypes.ADD_BATTERY,

            id:             batteryId,

            manufacturer:   classificationModel.manufacturer,
            type:           classificationModel.type,
            name:           classificationModel.name,

            associations:   associationModel.selectedIds,

            cellCount:      cellCountModel.cellCount,
            charge:         Math.floor(capacityModel.capacity * chargeModel.items[0].level / 100),
            capacity:       capacityModel.capacity
        };

        module.dispatchPublicMessage(ModalMessage.SUBMIT, [transaction]);

        classificationModel.manufacturer = '';
        classificationModel.type = '';
        classificationModel.name = '';
        associationModel.selectedIds = [];
        capacityModel.capacity = DEFAULT_CAPACITY;

        close();
    }

    public function validate():Void
    {
        panelModel.submittable =
            validateString(classificationModel.manufacturer) &&
            validateString(classificationModel.type) &&
            validateString(classificationModel.name);
    }

    private function validateString(value:String):Bool
    {
        return value != null && value.length > 0;
    }

    public function setManufacturer(value:String):Void
    {
        classificationModel.manufacturer = value;
        validate();
    }

    public function setType(value:String):Void
    {
        classificationModel.type = value;
        validate();
    }

    public function setName(value:String):Void
    {
        classificationModel.name = value;
        validate();
    }

    public function setAssociation(values:Array<String>):Void
    {
        associationModel.selectedIds = values;
    }

    public function setCapacity(value:UInt):Void
    {
        capacityModel.capacity = value;
    }

    public function setCellCount(count:UInt):Void
    {
        cellCountModel.cellCount = count;
    }
}
