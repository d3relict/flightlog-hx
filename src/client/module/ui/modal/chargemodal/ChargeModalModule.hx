package module.ui.modal.chargemodal;
import hex.config.stateless.StatelessModuleConfig;
import hex.module.IModule;
import hex.module.Module;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.RuntimeDependencies;
import hex.view.IView;
import interfaces.logic.IBatteryProvider;
import interfaces.logic.ISettingsProvider;
import interfaces.ui.IModal;
import module.ui.modal.chargemodal.controller.ChargeModalController;
import module.ui.modal.common.controller.CommonChargeController;
import module.ui.modal.common.controller.interfaces.*;
import module.ui.modal.common.model.*;
import module.ui.modal.common.model.interfaces.*;
import module.ui.modal.common.view.*;
import module.ui.modal.common.view.interfaces.*;

/**
 * ...
 * @author d3relict
 */
class ChargeModalModule extends Module implements IModal implements IModule
{
    public var available(get, never):Bool;

    var views:Array<IView> = new Array<IView>();

    public function new(panelId:String, batteryProvider:IBatteryProvider, settingsProvider:ISettingsProvider)
    {
        super();

        _injector.mapToValue(IBatteryProvider, batteryProvider);
        _injector.mapToValue(ISettingsProvider, settingsProvider);

        views = [
            new ModalView(panelId),
            new BatteryView(panelId),
            new DateView(panelId),
            new ChargeView(panelId),
        ];

        _addStatelessConfigClasses([ChargeModalModuleConfig]);

        for (view in views) {
            _injector.injectInto(view);
        }

        _get(IModalModel).panelId = panelId;
        _get(IModalController).init();
    }

    override function _getRuntimeDependencies():IRuntimeDependencies
    {
        return new RuntimeDependencies();
    }

    public function open ():Void
    {
        _get(IModalController).update();
        _get(IModalController).open();
    }

    public function close ():Void
    {
        _get(IModalController).close();
    }

    public function update():Void
    {
        _get(IModalController).update();
    }

    function get_available():Bool
    {
        return _get(IModalModel).available;
    }
}

private class ChargeModalModuleConfig extends StatelessModuleConfig
{
    override public function configure():Void
    {
        this.mapModel(IModalModel,     ModalModel);
        this.mapModel(IDateModel,      DateModel);
        this.mapModel(IChargeModel,    ChargeModel);
        this.mapModel(IBatteryModel,   BatteryModel);

        this.mapController(IChargeController,  CommonChargeController);
        this.mapController(IModalController,   ChargeModalController);
        this.mapController(IDateController,    ChargeModalController);
    }
}
