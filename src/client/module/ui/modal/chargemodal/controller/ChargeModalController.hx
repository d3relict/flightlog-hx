package module.ui.modal.chargemodal.controller;
import const.EventTypes;
import const.TransactionTypes;
import hex.di.IInjectorContainer;
import hex.module.IModule;
import interfaces.logic.IBatteryProvider;
import interfaces.logic.ISettingsProvider;
import module.ui.modal.common.controller.interfaces.*;
import module.ui.modal.common.enums.ChargeActionType;
import module.ui.modal.common.message.ModalMessage;
import module.ui.modal.common.model.interfaces.*;
import types.complex.battery.BatteryList;
import types.complex.event.ChargeList;
import types.complex.transaction.EventTransaction;
import util.StringUtil;
import types.complex.battery.Battery;

/**
 * ...
 * @author d3relict
 */
class ChargeModalController
    implements IModalController
    implements IDateController
    implements IInjectorContainer
{
    @Inject
    public var panelModel:IModalModel;
    @Inject
    public var batteryModel:IBatteryModel;
    @Inject
    public var dateModel:IDateModel;
    @Inject
    public var chargeModel:IChargeModel;
    @Inject
    public var batteryData:IBatteryProvider;
    @Inject
    public var settings:ISettingsProvider;
    @Inject
    public var module:IModule;
    @Inject
    public var chargeController:IChargeController;

    public function open():Void
    {
        panelModel.open = true;
    }

    public function close():Void
    {
        panelModel.open = false;
    }

    public function init():Void
    {
        chargeController.setActionType(ChargeActionType.charge);
        chargeController.setLocked(false);
        chargeController.setCellCount(0);
        batteryModel.batteries = [];
        update();
    }

    public function update():Void
    {
        dateModel.date = Date.now();

        if (checkAvailability())
        {
            batteryModel.batteries = getAvailableBatteries();
            chargeController.selectBatteries([batteryModel.batteries[0].id]);
        }
    }

    public function submit():Void
    {
        var charges:ChargeList = new ChargeList();
        for (chargeItem in chargeModel.items)
        {
            charges.push({
                id:         chargeItem.batteryId,
                charge:     chargeItem.charge,
                level:      chargeItem.level
            });
        }

        var transaction:EventTransaction = {
            txType:     TransactionTypes.ADD_EVENT,

            id:         StringUtil.generateId(),
            eventType:  EventTypes.CHARGE,
            timestamp:  dateModel.date.getTime(),

            flight:     null,

            charges:    charges
        }

        module.dispatchPublicMessage(ModalMessage.SUBMIT, [transaction]);

        close();
    }

    private function getAvailableBatteries():BatteryList
    {
        return batteryData.getBatteriesByCharge(0, 99);
    }

    private function checkAvailability():Bool
    {
        panelModel.available = getAvailableBatteries().length > 0;

        module.dispatchPublicMessage(ModalMessage.AVAILABILITY);

        return panelModel.available;
    }

    //IDateController

    public function setDate(date:Date):Void
    {
        dateModel.date = date;
    }
}
