package module.ui.floatingbutton;

import hex.config.stateless.StatelessModuleConfig;
import hex.module.IModule;
import hex.module.Module;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.RuntimeDependencies;
import module.ui.floatingbutton.controller.FABController;
import module.ui.floatingbutton.controller.IFABController;
import module.ui.floatingbutton.model.FABModel;
import module.ui.floatingbutton.model.IFABModel;
import module.ui.floatingbutton.view.FABView;
import module.ui.floatingbutton.vo.FABElement;

/**
 * ...
 * @author d3relict
 */
class FABModule extends Module implements IFABModule implements IModule
{
    var view:FABView;

    public function new(containerId:String, templateId:String, elements:Array<FABElement>)
    {
        super();

        _addStatelessConfigClasses([FABModuleConfig]);

        view = new FABView(containerId, templateId);
        _injector.injectInto(view);

        _get(IFABController).setElements(elements);
    }

    override function _getRuntimeDependencies():IRuntimeDependencies
    {
        return new RuntimeDependencies();
    }

    public function show ():Void
    {
        _get(IFABController).show();
    }

    public function hide ():Void
    {
        _get(IFABController).hide();
    }

    public function open ():Void
    {
        _get(IFABController).open();
    }

    public function close ():Void
    {
        _get(IFABController).close();
    }

    public function updateModalAvailability():Void
    {
        _get(IFABController).updateModalAvailability();
    }
}

private class FABModuleConfig extends StatelessModuleConfig
{
    override public function configure():Void
    {
        this.mapModel(IFABModel, FABModel);

        this.mapController(IFABController, FABController);
    }
}
