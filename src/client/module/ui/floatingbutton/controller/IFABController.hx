package module.ui.floatingbutton.controller;
import module.ui.floatingbutton.vo.FABElement;

/**
 * @author d3relict
 */
interface IFABController
{
    function open():Void;
    function close():Void;

    function show():Void;
    function hide():Void;

    function setElements(elements:Array<FABElement>):Void;
    function openModal(id:String):Void;
    function updateModalAvailability():Void;
}
