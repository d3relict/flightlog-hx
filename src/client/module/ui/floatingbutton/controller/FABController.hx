package module.ui.floatingbutton.controller;
import hex.di.IInjectorContainer;
import hex.error.IllegalArgumentException;
import module.ui.floatingbutton.model.IFABModel;
import module.ui.floatingbutton.vo.FABElement;

/**
 * ...
 * @author d3relict
 */
class FABController implements IFABController implements IInjectorContainer
{
    @Inject
    public var model:IFABModel;

    public function open():Void
    {
        model.open = true;
    }

    public function close():Void
    {
        model.open = false;
    }

    public function show():Void
    {
        model.visible = true;
    }

    public function hide():Void
    {
        model.visible = false;
    }

    public function setElements(elements:Array<FABElement>):Void
    {
        var available:Array<String> = new Array<String>();

        for (element in elements)
        {
            if (element.modal.available)
            {
                available.push(element.modalId);
            }
        }

        model.available = available;

        model.elements = elements;
    }

    public function openModal(id:String):Void
    {
        for (element in model.elements)
        {
            if (element.modalId == id)
            {
                element.modal.open();
                return;
            }
        }
        throw new IllegalArgumentException('Unknown modal id: ' + id);
    }

    public function updateModalAvailability():Void
    {
        var available:Array<String> = new Array<String>();
        for (element in model.elements)
        {
            if (element.modal.available)
            {
                available.push(element.modalId);
            }
        }
        model.available = available;
    }
}
