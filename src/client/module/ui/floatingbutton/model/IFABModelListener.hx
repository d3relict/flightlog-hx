package module.ui.floatingbutton.model;

/**
 * @author d3relict
 */
interface IFABModelListener 
{
    function onSetOpen():Void;
    function onSetVisible():Void;
    function onSetElements():Void;
    function onSetAvailability():Void;
}
