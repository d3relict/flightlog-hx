package module.ui.floatingbutton.model;
import module.ui.floatingbutton.vo.FABElement;

/**
 * @author d3relict
 */
interface IFABModel extends IFABModelRO
{
    var open(default, set):Bool;
    var visible(default, set):Bool;
    var elements(default, set):Array<FABElement>;
    var available(default, set):Array<String>;
}
