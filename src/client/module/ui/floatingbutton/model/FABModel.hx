package module.ui.floatingbutton.model;
import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.floatingbutton.vo.FABElement;

/**
 * ...
 * @author d3relict
 */
class FABModel extends BasicModel<FABModelDispatcher, IFABModelListener> implements IFABModel
{
    public var open(default, set):Bool = false;
    public var visible(default, set):Bool = true;
    public var elements(default, set):Array<FABElement> = [];
    public var available(default, set):Array<String> = [];
    
    public function new() 
    {
        super();
    }
    
    function set_open(value)
    {
        open = value;
        dispatcher.onSetOpen();
        return value;
    }
    
    function set_visible(value)
    {
        visible = value;
        dispatcher.onSetVisible();
        return value;
    }
    
    function set_elements(value:Array<FABElement>):Array<FABElement> 
    {
        elements = value;
        dispatcher.onSetElements();
        return value;
    }
    
    function set_available(value:Array<String>):Array<String> 
    {
        available = value;
        dispatcher.onSetAvailability();
        return value;
    }
}

private class FABModelDispatcher extends ModelDispatcher<IFABModelListener> implements IFABModelListener
{
    public function onSetOpen():Void;
    public function onSetVisible():Void;
    public function onSetElements():Void;
    public function onSetAvailability():Void;
}
