package module.ui.floatingbutton.model;
import hex.model.IModelRO;
import module.ui.floatingbutton.vo.FABElement;

/**
 * @author d3relict
 */
interface IFABModelRO extends IModelRO<IFABModelListener> 
{
    var open(default, never):Bool;
    var visible(default, never):Bool;
    var elements(default, never):Array<FABElement>;
    var available(default, never):Array<String>;
}
