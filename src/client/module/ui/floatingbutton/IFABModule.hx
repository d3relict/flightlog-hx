package module.ui.floatingbutton;
import interfaces.ui.IOpenClose;
import interfaces.ui.IShowHide;

/**
 * @author d3relict
 */
interface IFABModule extends IOpenClose extends IShowHide
{
    function updateModalAvailability():Void;
}
