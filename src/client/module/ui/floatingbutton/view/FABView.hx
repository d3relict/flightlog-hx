package module.ui.floatingbutton.view;
import haxe.Template;
import hex.view.BasicView;
import hex.view.IView;
import js.Browser;
import js.html.AnchorElement;
import js.html.DivElement;
import js.html.Element;
import js.html.HTMLCollection;
import js.html.LIElement;
import js.html.MouseEvent;
import js.html.UListElement;
import js.jquery.JQuery;
import module.ui.floatingbutton.controller.IFABController;
import module.ui.floatingbutton.model.IFABModelListener;
import module.ui.floatingbutton.model.IFABModelRO;

/**
 * ...
 * @author d3relict
 */

typedef FABTemplateItem = {
    var modalId:String;
    var color:String;
    var icon:String;
};
    
typedef FABTemplateList = {
    var elements:Array<FABTemplateItem>;
};

class FABView extends BasicView implements IView implements IFABModelListener
{
    @Inject
    public var model:IFABModelRO;
    
    @Inject
    public var controller:IFABController;
    
    private var container:DivElement;
    private var template:Template;
    
    public function new(containerId:String, templateId:String) 
    {
        super();
        container = cast Browser.document.getElementById(containerId);
        template = new Template(Browser.document.getElementById(templateId).innerHTML);
    }
    
    @PostConstruct
    public function init():Void
    {
        onSetAvailability();
        
        onSetOpen();
        onSetVisible();
        
        container.addEventListener('click', onClick);        
        
        model.addListener(this);
    }
    
    function onClick(event:MouseEvent):Void
    {
        var target:Element = cast event.target;
        var id:String = target.getAttribute('data-id');
        
        if (id == 'action')
        {
            model.open ? controller.close() : controller.open();
        }
        else
        {
            controller.close();
            controller.openModal(id);
        }
    }
    
    public function onSetVisible():Void 
    {
        container.hidden = !model.visible;
    }
    
    public function onSetOpen():Void 
    {
        untyped model.open ? new JQuery(fabContainer).openFAB() : new JQuery(fabContainer).closeFAB();
    }
    
    public function onSetElements():Void
    {
        var templateData:FABTemplateList = {
            elements: new Array<FABTemplateItem>()
        };
        
        for (element in model.elements)
        {
            templateData.elements.push(
            {
                modalId: element.modalId,
                icon: element.icon,
                color: element.color
            });
        }
        
        container.innerHTML = template.execute(templateData);
        onSetAvailability();
    }
    
    public function onSetAvailability():Void
    {
        var elements:HTMLCollection = container.getElementsByTagName('li');
        var i:Element;
        for (element in elements)
        {
            i = element.getElementsByTagName('i')[0];
            element.hidden = model.available.indexOf(i.getAttribute('data-id')) == -1;
        }
    }
}
