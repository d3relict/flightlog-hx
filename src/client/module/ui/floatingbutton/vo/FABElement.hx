package module.ui.floatingbutton.vo;
import interfaces.ui.IModal;

/**
 * ...
 * @author d3relict
 */
class FABElement
{
    public var modal:IModal;
    public var modalId:String;
    public var icon:String;
    public var color:String;
    
    public function new(modal:IModal, modalId:String, icon:String, color:String = 'grey') 
    {
        this.modal = modal;
        this.modalId = modalId;
        this.icon = icon;
        this.color = color;
    }
}
