package module.ui.syncstatus.view;
import haxe.Template;
import hex.log.Logger;
import hex.view.BasicView;
import hex.view.IView;
import interfaces.ui.IModal;
import js.Browser;
import js.html.Element;
import js.html.MouseEvent;
import module.ui.syncstatus.controller.IModalController;
import module.ui.syncstatus.model.ISyncModelListener;
import module.ui.syncstatus.model.ISyncModelRO;

/**
 * ...
 * @author d3relict
 */

typedef ButtonTemplateData = {
    var state:Int;
};

class SyncStatusButtonView extends BasicView implements IView implements ISyncModelListener
{
    @Inject
    public var controller:IModalController;

    @Inject
    public var syncModel:ISyncModelRO;

    private var template:Template;
    private var container:Element;

    public function new(containerId:String, templateId:String)
    {
        super();

        template = new Template(Browser.document.getElementById(templateId).innerHTML);
        container = Browser.document.getElementById(containerId);
        container.onclick = onClick;
    }

    @PostConstruct
    public function init():Void
    {
        syncModel.addListener(this);
        onSetStatus();
    }

    private function onClick(event:MouseEvent):Void
    {
        Logger.debug('onClick');
        controller.open();
    }

    public function onSetStatus():Void
    {
        if (syncModel.status == null)
            return;

        var templateData:ButtonTemplateData = {
            state: syncModel.status
        };

        container.innerHTML = template.execute(templateData);
    }

    public function onSetUserId():Void
    {
    }

    public function onSetAuthenticated():Void
    {
    }

    public function onSetLastSuccess():Void
    {
    }

    public function onSetLastAttempt():Void
    {
    }

    public function onSetModalOpen():Void
    {
    }
}
