package module.ui.syncstatus.view;
import haxe.Template;
import hex.log.Logger;
import hex.view.BasicView;
import hex.view.IView;
import interfaces.ui.IModal;
import js.Browser;
import js.html.Attr;
import js.html.Element;
import js.html.Event;
import js.html.InputElement;
import js.html.MouseEvent;
import js.jquery.JQuery;
import module.ui.syncstatus.controller.IModalController;
import module.ui.syncstatus.controller.ISynchronizerController;
import module.ui.syncstatus.model.ISyncModelListener;
import module.ui.syncstatus.model.ISyncModelRO;

/**
 * ...
 * @author d3relict
 */

typedef ModalTemplateData = {
    var userId:String;
    var state:UInt;
    var modalId:String;
    var lastSuccess:String;
    var lastAttempt:String;
};

class SyncStatusModalView extends BasicView implements IView implements ISyncModelListener
{
    @Inject
    public var modalController:IModalController;

    @Inject
    public var syncController:ISynchronizerController;

    @Inject
    public var syncModel:ISyncModelRO;

    private var template:Template;
    private var container:Element;

    private var documentReady:Bool = false;

    public function new(containerId:String, templateId:String)
    {
        super();

        template = new Template(Browser.document.getElementById(templateId).innerHTML);
        container = Browser.document.getElementById(containerId);

        //materializecss is not present before onready
        //TODO: find a less hacky solution
        new JQuery('document').ready(function(){
            documentReady = true;
            updateTextFields();
        });
    }

    @PostConstruct
    public function init():Void
    {
        syncModel.addListener(this);
        onSetModalOpen();
    }

    private function updateTemplate():Void
    {
        var templateData:ModalTemplateData = {
            userId: syncModel.userId != null ? syncModel.userId : '',
            modalId: container.id,
            state: syncModel.status != null ? syncModel.status : 0,
            lastSuccess: syncModel.lastSuccess != null ? syncModel.lastSuccess.toString() : 'never',
            lastAttempt: syncModel.lastAttempt != null ? syncModel.lastAttempt.toString() : 'never'
        };

        container.innerHTML = template.execute(templateData);
        container.onclick = onClick;

        //float up label
        var idInput:InputElement = cast Browser.document.getElementById(container.id + '-userId');
        updateTextFields();
        idInput.disabled = syncModel.userId != null;
    }

    private function onClick(event:MouseEvent):Void
    {
        var action:Attr = cast(event.target, Element).attributes.getNamedItem('data-action');
        if (action == null)
            return;

        switch(action.value)
        {
            case 'setUId':
                syncController.setUserId(cast(Browser.document.getElementById(container.id + '-userId'), InputElement).value);
            case 'clearUId':
                syncController.setUserId(null);
            case 'sync':
                syncController.sync();
        }
    }

    public function onSetStatus():Void
    {
        updateTemplate();
    }

    public function onSetUserId():Void
    {
        updateTemplate();
    }

    public function onSetAuthenticated():Void
    {
        updateTemplate();
    }

    public function onSetLastSuccess():Void
    {
        updateTemplate();
    }

    public function onSetLastAttempt():Void
    {
        updateTemplate();
    }

    public function onSetModalOpen():Void
    {
        updateTemplate();
        untyped new JQuery(container).modal(syncModel.modalOpen ? 'open' : 'close');
    }

    private function updateTextFields():Void
    {
        if (documentReady == true)
            untyped Materialize.updateTextFields();
    }
}
