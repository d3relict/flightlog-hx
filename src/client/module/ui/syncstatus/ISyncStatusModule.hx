package module.ui.syncstatus;

/**
 * @author d3relict
 */
interface ISyncStatusModule
{
    function updateStatus():Void;
}
