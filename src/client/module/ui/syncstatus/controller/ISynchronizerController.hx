package module.ui.syncstatus.controller;

/**
 * @author d3relict
 */
interface ISynchronizerController
{
    function setUserId(uid:String):Void;
    function sync():Void;
}
