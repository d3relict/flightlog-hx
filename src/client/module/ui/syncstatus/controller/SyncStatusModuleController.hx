package module.ui.syncstatus.controller;
import hex.di.IInjectorContainer;
import hex.log.Logger;
import hex.module.IModule;
import interfaces.logic.ISynchronizer;
import module.ui.syncstatus.controller.ISyncStatusController;
import module.ui.syncstatus.controller.ISynchronizerController;
import module.ui.syncstatus.model.ISyncModel;

/**
 * ...
 * @author d3relict
 */
class SyncStatusModuleController
    implements ISyncStatusController
    implements ISynchronizerController
    implements IModalController
    implements IInjectorContainer
{
    @Inject
    public var syncModel:ISyncModel;

    @Inject
    public var synchronizer:ISynchronizer;

    @Inject
    public var module:IModule;


    public function update():Void
    {
        syncModel.status =          synchronizer.status;
        syncModel.userId =          synchronizer.userId;
        syncModel.lastSuccess =     synchronizer.lastSuccess;
        syncModel.lastAttempt =     synchronizer.lastAttempt;
    }

    public function setUserId(uid:String):Void
    {
        synchronizer.userId = uid;
    }

    public function sync():Void
    {
        synchronizer.sync();
    }

    public function open():Void
    {
        syncModel.modalOpen = true;
    }

    public function close():Void
    {
        syncModel.modalOpen = false;
    }
}
