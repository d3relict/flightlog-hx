package module.ui.syncstatus.controller;

/**
 * @author d3relict
 */
interface ISyncStatusController
{
    function update():Void;
}
