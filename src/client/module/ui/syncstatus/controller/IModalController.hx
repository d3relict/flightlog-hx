package module.ui.syncstatus.controller;

/**
 * @author d3relict
 */
interface IModalController
{
    function open():Void;
    function close():Void;
}
