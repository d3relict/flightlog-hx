package module.ui.syncstatus.model;
import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.syncstatus.model.ISyncModel;
import module.ui.syncstatus.model.ISyncModelListener;

/**
 * ...
 * @author d3relict
 */
class SyncModel extends BasicModel<SyncModelDispatcher, ISyncModelListener> implements ISyncModel
{
    public var userId(default, set):String;
    public var status(default, set):UInt;
    public var lastSuccess(default, set):Date;
    public var lastAttempt(default, set):Date;
    public var modalOpen(default, set):Bool;

    public function new()
    {
        super();
    }

    function set_userId(value:String):String
    {
        userId = value;
        dispatcher.onSetStatus();
        return value;
    }

    function set_status(value:UInt):UInt
    {
        status = value;
        dispatcher.onSetUserId();
        return value;
    }

    function set_lastSuccess(value:Date):Date
    {
        lastSuccess = value;
        dispatcher.onSetLastSuccess();
        return value;
    }

    function set_lastAttempt(value:Date):Date
    {
        lastAttempt = value;
        dispatcher.onSetLastAttempt();
        return value;
    }

    function set_modalOpen(value:Bool):Bool
    {
        modalOpen = value;
        dispatcher.onSetModalOpen();
        return value;
    }
}

private class SyncModelDispatcher extends ModelDispatcher<ISyncModelListener> implements ISyncModelListener
{
    public function onSetUserId():Void;
    public function onSetStatus():Void;
    public function onSetLastSuccess():Void;
    public function onSetLastAttempt():Void;
    public function onSetModalOpen():Void;
}
