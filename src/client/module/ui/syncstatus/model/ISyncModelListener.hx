package module.ui.syncstatus.model;

/**
 * @author d3relict
 */
interface ISyncModelListener
{
    function onSetUserId():Void;
    function onSetStatus():Void;
    function onSetLastSuccess():Void;
    function onSetLastAttempt():Void;
    function onSetModalOpen():Void;
}
