package module.ui.syncstatus.model;
import hex.model.IModelRO;
import types.complex.transaction.TransactionList;

/**
 * @author d3relict
 */
interface ISyncModelRO extends IModelRO<ISyncModelListener>
{
    var userId(default, null):String;
    var status(default, null):UInt;
    var lastSuccess(default, null):Date;
    var lastAttempt(default, null):Date;
    var modalOpen(default, null):Bool;
}
