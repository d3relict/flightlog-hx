package module.ui.syncstatus.model;
import module.ui.syncstatus.model.ISyncModelRO;

/**
 * @author d3relict
 */
interface ISyncModel extends ISyncModelRO
{
    var userId(default, set):String;
    var status(default, set):UInt;
    var lastSuccess(default, set):Date;
    var lastAttempt(default, set):Date;
    var modalOpen(default, set):Bool;
}
