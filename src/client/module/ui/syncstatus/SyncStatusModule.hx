package module.ui.syncstatus;
import hex.config.stateless.StatelessModuleConfig;
import hex.module.IModule;
import hex.module.Module;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.RuntimeDependencies;
import hex.view.IView;
import interfaces.logic.ISynchronizer;
import module.ui.syncstatus.controller.IModalController;
import module.ui.syncstatus.controller.ISyncStatusController;
import module.ui.syncstatus.controller.ISynchronizerController;
import module.ui.syncstatus.controller.SyncStatusModuleController;
import module.ui.syncstatus.model.ISyncModel;
import module.ui.syncstatus.model.SyncModel;
import module.ui.syncstatus.view.SyncStatusButtonView;
import module.ui.syncstatus.view.SyncStatusModalView;

/**
 * ...
 * @author d3relict
 */
class SyncStatusModule extends Module implements ISyncStatusModule implements IModule
{
    var views:Array<IView>;

    public function new(synchronizer:ISynchronizer, buttonContainerId:String, buttonTemplateId:String, modalContainerId:String, modalTemplateId:String)
    {
        super();

        views = [
            new SyncStatusButtonView(buttonContainerId, buttonTemplateId),
            new SyncStatusModalView(modalContainerId, modalTemplateId)
        ];

        _injector.mapToValue(ISynchronizer, synchronizer);
        _addStatelessConfigClasses([SyncStatusModuleConfig]);

        for (view in views) {
            _injector.injectInto(view);
        }

        _get(ISyncStatusController).update();
    }

    override function _getRuntimeDependencies():IRuntimeDependencies
    {
        return new RuntimeDependencies();
    }

    public function updateStatus():Void
    {
        _get(ISyncStatusController).update();
    }
}

private class SyncStatusModuleConfig extends StatelessModuleConfig
{
    override public function configure():Void
    {
        mapModel(ISyncModel, SyncModel);

        mapController(ISyncStatusController,    SyncStatusModuleController);
        mapController(ISynchronizerController,  SyncStatusModuleController);
        mapController(IModalController,         SyncStatusModuleController);
    }
}
