package module.ui.collection.eventcollection.view;
import const.EventTypes;
import haxe.Template;
import js.Browser;
import js.html.Element;
import module.ui.collection.common.view.CollectionViewBase;
import module.ui.collection.eventcollection.model.IEventModelListener;
import module.ui.collection.eventcollection.model.IEventModelRO;
import util.StringUtil;

/**
 * ...
 * @author d3relict
 */

typedef EventTemplateItem = {
    var deletable:Bool;
    var id:String;
    var type:String;
    var date:String;

    var aircraftName:String;
    var duration:String;

    var charges:ChargeTemplateList;
};

typedef ChargeTemplateItem = {
    var shortName:String;
    var longName:String;
    var charge:Int;
    var percentage:Float;
};

typedef ChargeTemplateList = Array<ChargeTemplateItem>;

typedef EventTemplateList = {
    var events:Array<EventTemplateItem>;
};

class EventCollectionView extends CollectionViewBase implements IEventModelListener
{
    @Inject
    public var eventModel:IEventModelRO;

    public function new(containerId:String, templateId:String)
    {
        super(containerId, templateId);
    }

    @PostConstruct
    public function init():Void
    {
        eventModel.addListener(this);
        onSetEvents();
    }

    public function onSetEvents():Void
    {
        var batteries:Array<String> = [];
        var templateData:EventTemplateList = {
            events:new Array<EventTemplateItem>()
        };
        var chargeItems:ChargeTemplateList;

        var typeMap:Map<UInt, String> = new Map<UInt, String>();
            typeMap.set(EventTypes.FLIGHT,      'flight');
            typeMap.set(EventTypes.CHARGE,      'charge');
            typeMap.set(EventTypes.DISCHARGE,   'discharge');

        var deletable:Bool;

        for (event in eventModel.events)
        {
            deletable = true;
            chargeItems = new ChargeTemplateList();

            for (charge in event.charges)
            {
                deletable = deletable && (charge.battery == null || batteries.indexOf(charge.battery.id) == -1);

                if (deletable && charge.battery != null)
                    batteries.push(charge.battery.id);

                chargeItems.push({
                    shortName:      StringUtil.batteryLabel(charge.battery, false, true),
                    longName:       StringUtil.batteryLabel(charge.battery, false),
                    charge:         charge.charge,
                    percentage:     Math.floor(charge.percentage * 10) / 10,
                });
            }

            templateData.events.push({
                deletable:          deletable,

                id:                 event.id,
                type:               typeMap.get(event.eventType),
                date:               Date.fromTime(event.timestamp).toString(),

                aircraftName:       StringUtil.aircraftLabel(event.flight != null ? event.flight.aircraft : null),
                duration:           StringUtil.formatDuration(event.flight != null ? event.flight.duration : 0),

                charges:            chargeItems
            });
        }

        container.innerHTML = template.execute(templateData);
    }
}
