package module.ui.collection.eventcollection;
import haxe.Template;
import hex.config.stateless.StatelessModuleConfig;
import hex.module.IModule;
import hex.module.Module;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.RuntimeDependencies;
import hex.view.IView;
import interfaces.logic.IAircraftProvider;
import interfaces.logic.IBatteryProvider;
import interfaces.logic.IEventProvider;
import js.Browser;
import js.html.Element;
import module.ui.collection.common.controller.IActionForwarder;
import module.ui.collection.common.controller.ICollectionUpdater;
import module.ui.collection.eventcollection.controller.EventCollectionController;
import module.ui.collection.eventcollection.model.EventModel;
import module.ui.collection.eventcollection.model.IEventModel;
import module.ui.collection.eventcollection.view.EventCollectionView;
import util.StringUtil;

/**
 * ...
 * @author d3relict
 */


class EventCollectionModule extends Module implements IModule
{
    private var view:IView;

    public function new(containerId:String, templateId:String, eventProvider:IEventProvider, aircraftProvider:IAircraftProvider, batteryProvider:IBatteryProvider)
    {
        super();

        _injector.mapToValue(IEventProvider,    eventProvider);
        _injector.mapToValue(IAircraftProvider, aircraftProvider);
        _injector.mapToValue(IBatteryProvider,  batteryProvider);

        _addStatelessConfigClasses([EventCollectionModuleConfig]);

        view = new EventCollectionView(containerId, templateId);
        _injector.injectInto(view);

        update();
    }

    override function _getRuntimeDependencies():IRuntimeDependencies
    {
        return new RuntimeDependencies();
    }

    public function update():Void
    {
        _get(ICollectionUpdater).update();
    }
}

private class EventCollectionModuleConfig extends StatelessModuleConfig
{
    override public function configure():Void
    {
        mapModel(IEventModel, EventModel);

        mapController(ICollectionUpdater, EventCollectionController);
        mapController(IActionForwarder,   EventCollectionController);
    }
}
