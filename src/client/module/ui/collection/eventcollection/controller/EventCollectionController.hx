package module.ui.collection.eventcollection.controller;
import const.TransactionTypes;
import hex.di.IInjectorContainer;
import hex.module.IModule;
import interfaces.logic.IAircraftProvider;
import interfaces.logic.IBatteryProvider;
import interfaces.logic.IEventProvider;
import module.ui.collection.common.controller.IActionForwarder;
import module.ui.collection.common.controller.ICollectionUpdater;
import module.ui.collection.common.message.CollectionMessage;
import module.ui.collection.eventcollection.model.IEventModel;
import module.ui.collection.eventcollection.type.EventCollectionItem;
import types.complex.battery.Battery;
import types.complex.event.Event;
import types.complex.transaction.RemovalTransaction;

/**
 * ...
 * @author d3relict
 */
class EventCollectionController
    implements ICollectionUpdater
    implements IActionForwarder
    implements IInjectorContainer
{
    @Inject
    public var eventProvider:IEventProvider;
    @Inject
    public var aircraftProvider:IAircraftProvider;
    @Inject
    public var batteryProvider:IBatteryProvider;
    @Inject
    public var eventModel:IEventModel;
    @Inject
    public var module:IModule;

    public function update():Void
    {
        var collectionItems:Array<EventCollectionItem> = new Array<EventCollectionItem>();
        var collectionItem:EventCollectionItem;

        var event:Event;
        var battery:Battery;
        var eventCharges:EventCharges;

        var i = eventProvider.events.length;
        while (i-->0)
        {
            event = eventProvider.events[i];

            eventCharges = new EventCharges();

            for (charge in event.charges)
            {
                battery = batteryProvider.getBatteryById(charge.id);

                eventCharges.push({
                    battery:    battery,
                    percentage: battery != null ? Math.abs(charge.charge) / battery.capacity * 100 : null,
                    charge:     cast Math.abs(charge.charge)
                });
            }

            collectionItem = {
                id:             event.id,
                eventType:      event.eventType,
                timestamp:      event.timestamp,

                flight: event.flight != null ? {
                    aircraft:   aircraftProvider.getAircraftById(event.flight.id),
                    duration:   event.flight.duration
                } : null,
                charges:        eventCharges
            }

            collectionItems.push(collectionItem);
        }

        eventModel.events = collectionItems;
    }

    public function edit(id:String):Void
    {
        module.dispatchPublicMessage(CollectionMessage.EDIT, [id]);
    }

    public function delete(id:String):Void
    {
        var transaction:RemovalTransaction = {
            txType:     TransactionTypes.REMOVE_EVENT,
            id:         id
        }
        module.dispatchPublicMessage(CollectionMessage.DELETE, [transaction]);
    }
}
