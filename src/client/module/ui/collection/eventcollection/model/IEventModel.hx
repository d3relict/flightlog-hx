package module.ui.collection.eventcollection.model;
import module.ui.collection.eventcollection.type.EventCollectionItem;

/**
 * @author d3relict
 */
interface IEventModel extends IEventModelRO
{
    var events(default, set):Array<EventCollectionItem>;
}
