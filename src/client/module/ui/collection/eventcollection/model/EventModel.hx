package module.ui.collection.eventcollection.model;

import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.collection.eventcollection.model.IEventModel;
import module.ui.collection.eventcollection.model.IEventModelListener;
import module.ui.collection.eventcollection.type.EventCollectionItem;

/**
 * ...
 * @author d3relict
 */
class EventModel extends BasicModel<EventModelDispatcher, IEventModelListener> implements IEventModel
{
    public var events(default, set):Array<EventCollectionItem> = new Array<EventCollectionItem>();

    public function new()
    {
        super();
    }

    function set_events(value)
    {
        events = value;
        dispatcher.onSetEvents();
        return value;
    }
}

private class EventModelDispatcher extends ModelDispatcher<IEventModelListener> implements IEventModelListener
{
    public function onSetEvents():Void;
}
