package module.ui.collection.eventcollection.model;
import hex.model.IModelRO;
import module.ui.collection.eventcollection.model.IEventModelListener;
import module.ui.collection.eventcollection.type.EventCollectionItem;

/**
 * @author d3relict
 */
interface IEventModelRO extends IModelRO<IEventModelListener>
{
    var events(default, never):Array<EventCollectionItem>;
}
