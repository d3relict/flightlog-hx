package module.ui.collection.eventcollection.model;

/**
 * @author d3relict
 */
interface IEventModelListener 
{
    function onSetEvents():Void;
}
