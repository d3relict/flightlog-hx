package module.ui.collection.eventcollection.type;
import types.complex.*;
import types.primitive.*;
import types.complex.aircraft.*;
import types.complex.battery.*;
/**
 * @author d3relict
import types.complex.aircraft.Aircraft;
import types.complex.battery.Battery;
 */
typedef EventCollectionItem =
{
    var id:String;
    var eventType:UInt;
    var timestamp:Float;

    var flight:EventFlight;
    var charges:EventCharges;
}

typedef EventFlight =
{
    var aircraft:Aircraft;
    var duration:UInt;
}

typedef EventCharges = Array<EventCharge>;

typedef EventCharge =
{
    var battery:Battery;
    var charge:Int;
    var percentage:Float;
};
