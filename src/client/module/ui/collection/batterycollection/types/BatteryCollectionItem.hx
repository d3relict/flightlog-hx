package module.ui.collection.batterycollection.types;
import types.complex.*;
import types.primitive.*;
import types.complex.battery.*;
/**
 * @author d3relict
 */
typedef BatteryCollectionItem =
{
    > Battery,
	> EventProperties,
}
