package module.ui.collection.batterycollection.model;

/**
 * @author d3relict
 */
interface IBatteryModelListener 
{
    function onSetBatteries():Void;
}
