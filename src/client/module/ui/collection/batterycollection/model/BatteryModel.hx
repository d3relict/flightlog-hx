package module.ui.collection.batterycollection.model;

import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.collection.batterycollection.model.IBatteryModel;
import module.ui.collection.batterycollection.model.IBatteryModelListener;
import module.ui.collection.batterycollection.types.BatteryCollectionItem;

/**
 * ...
 * @author d3relict
 */
class BatteryModel extends BasicModel<BatteryModelDispatcher, IBatteryModelListener> implements IBatteryModel
{
    public var batteries(default, set):Array<BatteryCollectionItem> = new Array<BatteryCollectionItem>();

    public function new()
    {
        super();
    }

    function set_batteries(value)
    {
        batteries = value;
        dispatcher.onSetBatteries();
        return value;
    }
}

private class BatteryModelDispatcher extends ModelDispatcher<IBatteryModelListener> implements IBatteryModelListener
{
    public function onSetBatteries():Void;
}
