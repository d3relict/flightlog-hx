package module.ui.collection.batterycollection.model;
import hex.model.IModelRO;
import module.ui.collection.batterycollection.model.IBatteryModelListener;
import module.ui.collection.batterycollection.types.BatteryCollectionItem;

/**
 * @author d3relict
 */
interface IBatteryModelRO extends IModelRO<IBatteryModelListener>
{
    var batteries(default, never):Array<BatteryCollectionItem>;
}
