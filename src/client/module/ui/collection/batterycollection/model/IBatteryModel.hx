package module.ui.collection.batterycollection.model;
import module.ui.collection.batterycollection.types.BatteryCollectionItem;

/**
 * @author d3relict
 */
interface IBatteryModel extends IBatteryModelRO
{
    var batteries(default, set):Array<BatteryCollectionItem>;
}
