package module.ui.collection.batterycollection.view;
import const.EventTypes;
import haxe.Template;
import hex.view.BasicView;
import hex.view.IView;
import js.Browser;
import js.html.Element;
import module.ui.collection.batterycollection.model.IBatteryModelListener;
import module.ui.collection.batterycollection.model.IBatteryModelRO;
import module.ui.collection.common.view.CollectionViewBase;
import util.StringUtil;
import types.complex.battery.Battery;

/**
 * ...
 * @author d3relict
 */

typedef BatteryTemplateItem = {
    var id:String;
    var name:String;
    var capacity:UInt;
    var charge:UInt;
    var percentage:Float;
    var color:String;
    var icon:String;
    var lastEventDate:String;
    var lastEventType:String;
};

typedef BatteryTemplateList = {
    var batteries:Array<BatteryTemplateItem>;
};

class BatteryCollectionView extends CollectionViewBase implements IBatteryModelListener
{
    @Inject
    public var batteryModel:IBatteryModelRO;

    public function new(containerId:String, templateId:String)
    {
        super(containerId, templateId);
    }

    @PostConstruct
    public function init():Void
    {
        batteryModel.addListener(this);
        onSetBatteries();
    }

    public function onSetBatteries():Void
    {
        var templateData:BatteryTemplateList = {
            batteries:new Array<BatteryTemplateItem>()
        };

        var percentage:Float;
        var stateIndex:UInt;

        for (battery in batteryModel.batteries)
        {
            percentage = battery.charge / battery.capacity * 100;
            stateIndex = percentage >= 35 ? (percentage >= 80 ? 2 : 1) : 0;

            var typeMap:Map<UInt, String> = new Map<UInt, String>();
                typeMap.set(EventTypes.FLIGHT,      'flight');
                typeMap.set(EventTypes.CHARGE,      'charge');
                typeMap.set(EventTypes.DISCHARGE,   'discharge');

            templateData.batteries.push({
                id: battery.id,
                name: StringUtil.batteryLabel(battery, false),
                charge: battery.charge,
                capacity: battery.capacity,
                percentage: Math.floor(percentage * 10) / 10,
                icon : ['battery_alert', 'battery_std', 'battery_charging_full'][stateIndex],
                color : ['red', 'grey', 'green'][stateIndex],
                lastEventType: typeMap.get(battery.eventType),
                lastEventDate: battery.timestamp > 0 ? Date.fromTime(battery.timestamp).toString() : null
            });
        }

        container.innerHTML = template.execute(templateData);
    }
}
