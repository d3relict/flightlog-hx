package module.ui.collection.batterycollection;
import haxe.Template;
import hex.config.stateless.StatelessModuleConfig;
import hex.module.IModule;
import hex.module.Module;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.RuntimeDependencies;
import hex.view.IView;
import interfaces.logic.IBatteryProvider;
import interfaces.logic.IEventProvider;
import js.Browser;
import js.html.Element;
import module.ui.collection.common.controller.IActionForwarder;
import module.ui.collection.common.controller.ICollectionUpdater;
import module.ui.collection.batterycollection.controller.BatteryCollectionController;
import module.ui.collection.batterycollection.model.BatteryModel;
import module.ui.collection.batterycollection.model.IBatteryModel;
import module.ui.collection.batterycollection.view.BatteryCollectionView;
import util.StringUtil;
import types.complex.battery.Battery;

/**
 * ...
 * @author d3relict
 */


class BatteryCollectionModule extends Module implements IModule
{
    private var view:IView;

    public function new(containerId:String, templateId:String, batteryProvider:IBatteryProvider, eventProvider:IEventProvider)
    {
        super();

        _injector.mapToValue(IBatteryProvider, batteryProvider);
        _injector.mapToValue(IEventProvider,   eventProvider);

        _addStatelessConfigClasses([BatteryCollectionModuleConfig]);

        view = new BatteryCollectionView(containerId, templateId);
        _injector.injectInto(view);

        update();
    }

    override function _getRuntimeDependencies():IRuntimeDependencies
    {
        return new RuntimeDependencies();
    }

    public function update():Void
    {
        _get(ICollectionUpdater).update();
    }
}

private class BatteryCollectionModuleConfig extends StatelessModuleConfig
{
    override public function configure():Void
    {
        mapModel(IBatteryModel, BatteryModel);

        mapController(ICollectionUpdater,   BatteryCollectionController);
        mapController(IActionForwarder,     BatteryCollectionController);
    }
}
