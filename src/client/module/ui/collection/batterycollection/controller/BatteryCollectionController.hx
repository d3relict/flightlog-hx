package module.ui.collection.batterycollection.controller;
import const.TransactionTypes;
import hex.di.IInjectorContainer;
import hex.module.IModule;
import interfaces.logic.IBatteryProvider;
import interfaces.logic.IEventProvider;
import module.ui.collection.batterycollection.model.IBatteryModel;
import module.ui.collection.batterycollection.types.BatteryCollectionItem;
import module.ui.collection.common.controller.IActionForwarder;
import module.ui.collection.common.controller.ICollectionUpdater;
import module.ui.collection.common.message.CollectionMessage;
import types.complex.event.Event;
import types.complex.transaction.RemovalTransaction;

/**
 * ...
 * @author d3relict
 */
class BatteryCollectionController
    implements ICollectionUpdater
    implements IActionForwarder
    implements IInjectorContainer
{
    @Inject
    public var batteryProvider:IBatteryProvider;
    @Inject
    public var eventProvider:IEventProvider;
    @Inject
    public var batteryModel:IBatteryModel;
    @Inject
    public var module:IModule;

    public function update():Void
    {
        var itemList:Array<BatteryCollectionItem> = new Array<BatteryCollectionItem>();
        var item:BatteryCollectionItem;
        var event:Event;

        for (battery in batteryProvider.batteries)
        {
            item = {
                id:             battery.id,

                cellCount:      battery.cellCount,
                capacity:       battery.capacity,
                charge:         battery.charge,

                manufacturer:   battery.manufacturer,
                name:           battery.name,
                type:           battery.type,

                associations:   battery.associations,

                eventType:      null,
                timestamp:      0
            }

            event = eventProvider.getLastEventForBattery(battery.id);
            if (event != null)
            {
                item.eventType = event.eventType;
                item.timestamp = event.timestamp;
            }

            itemList.push(item);
        }

        batteryModel.batteries = itemList;
    }

    public function edit(id:String):Void
    {
        module.dispatchPublicMessage(CollectionMessage.EDIT, [id]);
    }

    public function delete(id:String):Void
    {
        var transaction:RemovalTransaction = {
            txType:     TransactionTypes.REMOVE_BATTERY,
            id:         id
        }
        module.dispatchPublicMessage(CollectionMessage.DELETE, [transaction]);
    }
}
