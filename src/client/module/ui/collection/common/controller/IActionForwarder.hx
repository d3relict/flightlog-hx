package module.ui.collection.common.controller;

/**
 * @author d3relict
 */
interface IActionForwarder
{
    function edit(id:String):Void;
    function delete(id:String):Void;
}
