package module.ui.collection.common.controller;

/**
 * @author d3relict
 */
interface ICollectionUpdater 
{
    function update():Void;
}
