package module.ui.collection.common.view;
import haxe.Template;
import hex.view.BasicView;
import hex.view.IView;
import js.Browser;
import js.html.Element;
import js.html.Event;
import module.ui.collection.common.controller.IActionForwarder;

/**
 * ...
 * @author d3relict
 */
class CollectionViewBase extends BasicView implements IView
{
    private var template:Template;
    private var container:Element;

    @Inject
    public var actionForwarder:IActionForwarder;

    private function new(containerId:String, templateId:String)
    {
        super();
        template = new Template(Browser.document.getElementById(templateId).innerHTML);
        container = Browser.document.getElementById(containerId);
        container.onclick = onClick;
    }

    private function onClick(e:Event):Void
    {
        var action:String = null;
        var id:String = null;
        var element:Element = cast e.target;

        while (element != null && (action == null || id == null))
        {
            if (element.attributes.getNamedItem('data-id') != null)
                id = cast element.attributes.getNamedItem('data-id').value;

            if (element.attributes.getNamedItem('data-action') != null)
                action = cast element.attributes.getNamedItem('data-action').value;

            element = element.parentElement;
        }

        switch (action)
        {
            case 'edit':
                actionForwarder.edit(id);
            case 'delete':
                actionForwarder.delete(id);
        }
    }
}
