package module.ui.collection.common.message;
import hex.event.MessageType;

/**
 * ...
 * @author d3relict
 */
class CollectionMessage
{
    public static var EDIT(default, never):MessageType =    new MessageType('edit');
    public static var DELETE(default, never):MessageType =  new MessageType('delete');}
