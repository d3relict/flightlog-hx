package module.ui.collection.aircraftcollection.types;
import types.complex.*;
import types.primitive.*;
import types.complex.aircraft.*;
/**
 * @author d3relict
 */
typedef AircraftCollectionItem =
{
    > Aircraft,
	> EventProperties,
}
