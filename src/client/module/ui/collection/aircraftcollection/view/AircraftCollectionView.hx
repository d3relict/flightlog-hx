package module.ui.collection.aircraftcollection.view;
import module.ui.collection.aircraftcollection.model.IAircraftModelListener;
import module.ui.collection.aircraftcollection.model.IAircraftModelRO;
import module.ui.collection.common.view.CollectionViewBase;
import util.StringUtil;

/**
 * ...
 * @author d3relict
 */

typedef AircraftTemplateItem = {
    var id:String;
    var name:String;
    var totalFlightCount:UInt;
    var totalFlightDuration:String;
    var lastEventDate:String;
};

typedef AircraftTemplateList = {
    var aircrafts:Array<AircraftTemplateItem>;
};

class AircraftCollectionView extends CollectionViewBase implements IAircraftModelListener
{
    @Inject
    public var aircraftModel:IAircraftModelRO;

    public function new(containerId:String, templateId:String)
    {
        super(containerId, templateId);
    }

    @PostConstruct
    public function init():Void
    {
        aircraftModel.addListener(this);
        onSetAircrafts();
    }

    public function onSetAircrafts():Void
    {
        var templateData:AircraftTemplateList = {
            aircrafts:new Array<AircraftTemplateItem>()
        };

        for (aircraft in aircraftModel.aircrafts)
        {
            templateData.aircrafts.push(
            {
                id: aircraft.id,
                name: StringUtil.aircraftLabel(aircraft),
                totalFlightCount: aircraft.totalCount,
                totalFlightDuration: StringUtil.formatDuration(aircraft.totalDuration),
                lastEventDate: aircraft.timestamp > 0 ? Date.fromTime(aircraft.timestamp).toString() : null
            });
        }

        container.innerHTML = template.execute(templateData);
    }
}
