package module.ui.collection.aircraftcollection.controller;
import const.TransactionTypes;
import hex.di.IInjectorContainer;
import hex.module.IModule;
import interfaces.logic.IAircraftProvider;
import interfaces.logic.IEventProvider;
import module.ui.collection.aircraftcollection.model.IAircraftModel;
import module.ui.collection.aircraftcollection.types.AircraftCollectionItem;
import module.ui.collection.common.controller.IActionForwarder;
import module.ui.collection.common.controller.ICollectionUpdater;
import module.ui.collection.common.message.CollectionMessage;
import types.complex.event.Event;
import types.complex.transaction.RemovalTransaction;

/**
 * ...
 * @author d3relict
 */
class AircraftCollectionController
    implements ICollectionUpdater
    implements IActionForwarder
    implements IInjectorContainer
{
    @Inject
    public var aircraftProvider:IAircraftProvider;
    @Inject
    public var eventProvider:IEventProvider;
    @Inject
    public var aircraftModel:IAircraftModel;
    @Inject
    public var module:IModule;

    public function update():Void
    {
        var itemList:Array<AircraftCollectionItem> = new Array<AircraftCollectionItem>();
        var item:AircraftCollectionItem;
        var event:Event;

        for (aircraft in aircraftProvider.aircrafts)
        {
            item = {
                id:                     aircraft.id,

                manufacturer:           aircraft.manufacturer,
                name:                   aircraft.name,
                type:                   aircraft.type,

                totalCount:             aircraft.totalCount,
                usualDuration:          aircraft.usualDuration,
                totalDuration:          aircraft.totalDuration,

                cellCount:              aircraft.cellCount,

                eventType:              null,
                timestamp:              0
            }

            event = eventProvider.getLastEventForAircraft(aircraft.id);
            if (event != null)
            {
                item.eventType = event.eventType;
                item.timestamp = event.timestamp;
            }

            itemList.push(item);
        }

        aircraftModel.aircrafts = itemList;
    }

    public function edit(id:String):Void
    {
        module.dispatchPublicMessage(CollectionMessage.EDIT, [id]);
    }

    public function delete(id:String):Void
    {
        var transaction:RemovalTransaction = {
            txType:     TransactionTypes.REMOVE_AIRCRAFT,
            id:         id
        }
        module.dispatchPublicMessage(CollectionMessage.DELETE, [transaction]);
    }

}
