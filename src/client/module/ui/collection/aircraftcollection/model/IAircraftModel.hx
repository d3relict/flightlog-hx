package module.ui.collection.aircraftcollection.model;
import module.ui.collection.aircraftcollection.types.AircraftCollectionItem;

/**
 * @author d3relict
 */
interface IAircraftModel extends IAircraftModelRO
{
    var aircrafts(default, set):Array<AircraftCollectionItem>;
}
