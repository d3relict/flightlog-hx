package module.ui.collection.aircraftcollection.model;

/**
 * @author d3relict
 */
interface IAircraftModelListener 
{
    function onSetAircrafts():Void;
}
