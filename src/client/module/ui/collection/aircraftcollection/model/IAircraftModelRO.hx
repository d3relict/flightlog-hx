package module.ui.collection.aircraftcollection.model;
import hex.model.IModelRO;
import module.ui.collection.aircraftcollection.model.IAircraftModelListener;
import module.ui.collection.aircraftcollection.types.AircraftCollectionItem;

/**
 * @author d3relict
 */
interface IAircraftModelRO extends IModelRO<IAircraftModelListener>
{
    var aircrafts(default, never):Array<AircraftCollectionItem>;
}
