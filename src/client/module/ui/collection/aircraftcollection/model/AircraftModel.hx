package module.ui.collection.aircraftcollection.model;

import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.ui.collection.aircraftcollection.model.IAircraftModel;
import module.ui.collection.aircraftcollection.model.IAircraftModelListener;
import module.ui.collection.aircraftcollection.types.AircraftCollectionItem;

/**
 * ...
 * @author d3relict
 */
class AircraftModel extends BasicModel<AircraftModelDispatcher, IAircraftModelListener> implements IAircraftModel
{
    public var aircrafts(default, set):Array<AircraftCollectionItem> = new Array<AircraftCollectionItem>();

    public function new()
    {
        super();
    }

    function set_aircrafts(value)
    {
        aircrafts = value;
        dispatcher.onSetAircrafts();
        return value;
    }
}

private class AircraftModelDispatcher extends ModelDispatcher<IAircraftModelListener> implements IAircraftModelListener
{
    public function onSetAircrafts():Void;
}
