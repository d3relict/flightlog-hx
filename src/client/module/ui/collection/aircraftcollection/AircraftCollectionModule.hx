package module.ui.collection.aircraftcollection;
import haxe.Template;
import hex.config.stateless.StatelessModuleConfig;
import hex.module.IModule;
import hex.module.Module;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.RuntimeDependencies;
import hex.view.IView;
import interfaces.logic.IAircraftProvider;
import interfaces.logic.IEventProvider;
import js.Browser;
import js.html.Element;
import module.ui.collection.aircraftcollection.controller.AircraftCollectionController;
import module.ui.collection.common.controller.IActionForwarder;
import module.ui.collection.common.controller.ICollectionUpdater;
import module.ui.collection.aircraftcollection.model.AircraftModel;
import module.ui.collection.aircraftcollection.model.IAircraftModel;
import module.ui.collection.aircraftcollection.view.AircraftCollectionView;
import util.StringUtil;
import types.complex.aircraft.Aircraft;

/**
 * ...
 * @author d3relict
 */


class AircraftCollectionModule extends Module implements IModule
{
    private var view:IView;

    public function new(containerId:String, templateId:String, aircraftProvider:IAircraftProvider, eventProvider:IEventProvider)
    {
        super();

        _injector.mapToValue(IAircraftProvider,    aircraftProvider);
        _injector.mapToValue(IEventProvider,       eventProvider);

        _addStatelessConfigClasses([AircraftCollectionModuleConfig]);

        view = new AircraftCollectionView(containerId, templateId);
        _injector.injectInto(view);

        update();
    }

    override function _getRuntimeDependencies():IRuntimeDependencies
    {
        return new RuntimeDependencies();
    }

    public function update():Void
    {
        _get(ICollectionUpdater).update();
    }
}

private class AircraftCollectionModuleConfig extends StatelessModuleConfig
{
    override public function configure():Void
    {
        mapModel(IAircraftModel, AircraftModel);

        mapController(ICollectionUpdater,   AircraftCollectionController);
        mapController(IActionForwarder,     AircraftCollectionController);
    }
}
