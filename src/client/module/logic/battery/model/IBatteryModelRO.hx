package module.logic.battery.model;
import hex.model.IModelRO;
import types.complex.battery.BatteryList;

/**
 * @author d3relict
 */
interface IBatteryModelRO extends IModelRO<IBatteryModelListener>
{
    var batteries(default, never):BatteryList;
}
