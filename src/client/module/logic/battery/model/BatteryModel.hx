package module.logic.battery.model;
import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.logic.battery.model.IBatteryModel;
import module.logic.battery.model.IBatteryModelListener;
import types.complex.battery.BatteryList;

/**
 * ...
 * @author d3relict
 */
class BatteryModel extends BasicModel<BatteryModelDispatcher, IBatteryModelListener> implements IBatteryModel
{
    public var batteries(default, default):BatteryList;

    public function new()
    {
        super();

        batteries = new BatteryList();
    }

}

private class BatteryModelDispatcher extends ModelDispatcher<IBatteryModelListener> implements IBatteryModelListener
{
}
