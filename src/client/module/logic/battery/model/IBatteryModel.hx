package module.logic.battery.model;
import module.logic.battery.model.IBatteryModelRO;
import types.complex.battery.BatteryList;

/**
 * @author d3relict
 */
interface IBatteryModel extends IBatteryModelRO
{
    var batteries(default, default):BatteryList;
}
