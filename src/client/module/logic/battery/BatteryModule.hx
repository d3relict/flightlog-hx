package module.logic.battery;
import hex.config.stateless.StatelessModuleConfig;
import hex.module.IModule;
import hex.module.Module;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.RuntimeDependencies;
import interfaces.logic.IAssociator;
import interfaces.logic.IBatteryProvider;
import interfaces.logic.IBatteryUpdater;
import interfaces.util.ILocalStorage;
import module.logic.battery.controller.BatteryController;
import module.logic.battery.controller.IBatteryProviderController;
import module.logic.battery.controller.IBatteryUpdaterController;
import module.logic.battery.model.BatteryModel;
import module.logic.battery.model.IBatteryModel;
import module.logic.common.interfaces.IStorageController;
import types.complex.transaction.AircraftTransaction;
import types.complex.battery.Battery;
import types.complex.battery.BatteryList;
import types.complex.transaction.BatteryTransaction;
import types.complex.event.Event;
import types.complex.transaction.EventTransaction;
import types.complex.transaction.RemovalTransaction;
import types.primitive.Identity;

/**
 * ...
 * @author d3relict
 */
class BatteryModule extends Module
implements IBatteryProvider
implements IBatteryUpdater
implements IAssociator
implements IModule
{
    public var batteries(get, null):BatteryList;

    public function new(localStorage:ILocalStorage)
    {
        super();

        _injector.mapToValue(ILocalStorage, localStorage);

        _addStatelessConfigClasses([BatteryModuleConfig]);

        _get(IStorageController).fetch();
    }

    override function _getRuntimeDependencies():IRuntimeDependencies
    {
        return new RuntimeDependencies();
    }

    public function get_batteries():BatteryList
    {
        return _get(IBatteryModel).batteries;
    }

    public function getBatteryById(id:String):Battery
    {
        return _get(IBatteryProviderController).getBatteryById(id);
    }

    public function getBatteriesByAssociation(id:String):BatteryList
    {
        return _get(IBatteryProviderController).getBatteriesByAssociation(id);
    }

    public function getBatteriesByCharge(minPercent:Float = 0, maxPercent:Float = 100):BatteryList
    {
        return _get(IBatteryProviderController).getBatteriesByCharge(minPercent, maxPercent);
    }

    public function addBattery(battery:Battery):Void
    {
        //TODO: better solution for update
        if (getBatteryById(battery.id) != null)
            _get(IBatteryUpdaterController).updateBattery(battery);
        else
            _get(IBatteryUpdaterController).addBattery(battery);

        _get(IStorageController).store();
    }

    public function removeBattery(identity:Identity):Void
    {
        _get(IBatteryUpdaterController).removeBattery(identity);
        _get(IStorageController).store();
    }

    public function applyCharge(charge:Event):Void
    {
        _get(IBatteryUpdaterController).applyCharge(charge);
        _get(IStorageController).store();
    }

    public function undoCharge(charge:Event):Void
    {
        _get(IBatteryUpdaterController).undoCharge(charge);
        _get(IStorageController).store();
    }


    /* INTERFACE interfaces.data.IAssociator */

    public function setAssociationByAircraft(request:AircraftTransaction):Void
    {
        _get(IBatteryUpdaterController).setAssociationByAircraft(request.id, request.associations);
        _get(IStorageController).store();
    }

    public function setAssociationByBattery(request:BatteryTransaction):Void
    {
        _get(IBatteryUpdaterController).setAssociationByBattery(request.id, request.associations);
        _get(IStorageController).store();
    }
}

private class BatteryModuleConfig extends StatelessModuleConfig
{
    override public function configure():Void
    {
        this.mapModel(IBatteryModel, BatteryModel);

        this.mapController(IBatteryProviderController, BatteryController);
        this.mapController(IBatteryUpdaterController,  BatteryController);
        this.mapController(IStorageController,         BatteryController);
    }
}
