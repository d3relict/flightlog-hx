package module.logic.battery.controller;
import const.EventTypes;
import hex.di.IInjectorContainer;
import hex.module.IModule;
import interfaces.util.ILocalStorage;
import module.logic.battery.model.IBatteryModel;
import module.logic.common.interfaces.IStorageController;
import module.logic.common.message.ProviderMessage;
import types.complex.battery.Battery;
import types.complex.battery.BatteryList;
import types.complex.event.Event;
import types.complex.transaction.EventTransaction;
import types.complex.transaction.RemovalTransaction;
import types.primitive.Identity;
import util.ObjectHelper;

/**
 * ...
 * @author d3relict
 */
class BatteryController
    implements IBatteryProviderController
    implements IBatteryUpdaterController
    implements IStorageController
    implements IInjectorContainer
{

    @Inject
    public var batteryModel:IBatteryModel;
    @Inject
    public var module:IModule;
    @Inject
    public var storage:ILocalStorage;

    public function getBatteryById(id:String):Battery
    {
        for (battery in batteryModel.batteries)
        {
            if (battery.id == id)
            {
                return battery;
            }
        }
        return null;
    }

    public function getBatteriesByAssociation(id:String):BatteryList
    {
        var batteryList:BatteryList = new BatteryList();
        for (battery in batteryModel.batteries)
        {
            if (battery.associations.indexOf(id) != -1)
            {
                batteryList.push(battery);
            }
        }
        return batteryList;
    }

    public function getBatteriesByCharge(minPercent:Float = 0, maxPercent:Float = 100):BatteryList
    {
        var batteryList:BatteryList = new BatteryList();
        var percentage:Float;
        for (battery in batteryModel.batteries)
        {
            percentage = battery.charge / battery.capacity * 100;

            if (percentage >= minPercent && percentage <= maxPercent)
            {
                batteryList.push(battery);
            }
        }
        return batteryList;
    }

    public function setAssociationByAircraft(aircraftId:String, batteryIds:Array<String>):Void
    {
        for (battery in batteryModel.batteries)
        {
            var needsAssociation:Bool = batteryIds.indexOf(battery.id) >= 0;
            var associated:Bool = battery.associations.indexOf(aircraftId) >= 0;
            if (needsAssociation && !associated)
            {
                battery.associations.push(aircraftId);
            }
            else if (!needsAssociation && associated)
            {
                battery.associations.splice(battery.associations.indexOf(aircraftId), 1);
            }
        }
        module.dispatchPublicMessage(ProviderMessage.UPDATE);
    }

    public function setAssociationByBattery(batteryId:String, aircraftIds:Array<String>):Void
    {
        var battery:Battery = getBatteryById(batteryId);
        if (battery != null)
        {
            battery.associations = aircraftIds;
        }
        module.dispatchPublicMessage(ProviderMessage.UPDATE);
    }

    public function addBattery(battery:Battery):Void
    {
        var clonedBattery:Battery = {
            id:                     battery.id,

            manufacturer:           battery.manufacturer,
            type:                   battery.type,
            name:                   battery.name,

            cellCount:              battery.cellCount,
            charge:                 battery.charge,
            capacity:               battery.capacity,

            associations:           battery.associations
        };

        var defaultValues:Dynamic = {
            associations: []
        };

        ObjectHelper.setFieldDefaults(clonedBattery, defaultValues);

        batteryModel.batteries.push(clonedBattery);

        module.dispatchPublicMessage(ProviderMessage.UPDATE);
    }

    public function updateBattery(battery:Battery):Void
    {
        var existing:Battery = getBatteryById(battery.id);

        existing.name           = battery.name;
        existing.manufacturer   = battery.manufacturer;
        existing.type           = battery.type;
        existing.associations   = battery.associations;
        existing.cellCount      = battery.cellCount;

        module.dispatchPublicMessage(ProviderMessage.UPDATE);
    }

    public function removeBattery(identity:Identity):Void
    {
        for (battery in batteryModel.batteries)
            if (battery.id == identity.id)
            {
                batteryModel.batteries.splice(batteryModel.batteries.indexOf(battery), 1);
                break;
            }

        module.dispatchPublicMessage(ProviderMessage.UPDATE);
    }

    public function applyCharge(event:Event):Void
    {
        for (charge in event.charges)
        {
            var battery:Battery = getBatteryById(charge.id);
            battery.charge = Math.round(battery.capacity * charge.level / 100);
            module.dispatchPublicMessage(ProviderMessage.UPDATE);
        }
    }

    public function undoCharge(event:Event):Void
    {
        for (charge in event.charges)
        {
            var battery:Battery = getBatteryById(charge.id);
            //TODO: set battery level based on previous event
            battery.charge -= charge.charge;
            module.dispatchPublicMessage(ProviderMessage.UPDATE);
        }
    }

    public function fetch():Void
    {
        var stored:Dynamic = storage.get('batteries');
        if (stored != null)
        {
            batteryModel.batteries = cast stored;
        }
    }

    public function store():Void
    {
        storage.set('batteries', batteryModel.batteries);
    }
}
