package module.logic.battery.controller;
import types.complex.battery.Battery;
import types.complex.event.Event;
import types.primitive.Identity;

/**
 * @author d3relict
 */
interface IBatteryUpdaterController
{
    function setAssociationByAircraft(aircraftId:String, batteryIds:Array<String>):Void;
    function setAssociationByBattery(batteryId:String, aircraftIds:Array<String>):Void;

    function addBattery(battery:Battery):Void;
    function updateBattery(battery:Battery):Void;
    function removeBattery(id:Identity):Void;

    function applyCharge(chargeEvent:Event):Void;
    function undoCharge(chargeEvent:Event):Void;
}
