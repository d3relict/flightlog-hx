package module.logic.battery.controller;
import types.complex.battery.Battery;
import types.complex.battery.BatteryList;

/**
 * @author d3relict
 */
interface IBatteryProviderController
{
    function getBatteryById(id:String):Battery;
    function getBatteriesByAssociation(id:String):BatteryList;
    function getBatteriesByCharge(minPercent:Float = 0, maxPercent:Float = 100):BatteryList;
}
