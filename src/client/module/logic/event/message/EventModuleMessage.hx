package module.logic.event.message;
import hex.event.MessageType;

/**
 * ...
 * @author d3relict
 */
class EventModuleMessage
{
    public static var ADD_FLIGHT    (default, never):MessageType = new MessageType('addFlight');
    public static var ADD_CHARGE    (default, never):MessageType = new MessageType('addCharge');

    public static var REMOVE_FLIGHT (default, never):MessageType = new MessageType('removeFlight');
    public static var REMOVE_CHARGE (default, never):MessageType = new MessageType('removeCharge');
}

