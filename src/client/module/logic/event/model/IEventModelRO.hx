package module.logic.event.model;
import hex.model.IModelRO;
import types.complex.event.EventList;

/**
 * @author d3relict
 */
interface IEventModelRO extends IModelRO<IEventModelListener>
{
    var events(default, never):EventList;
}
