package module.logic.event.model;
import module.logic.event.model.IEventModelRO;
import types.complex.event.EventList;

/**
 * @author d3relict
 */
interface IEventModel extends IEventModelRO
{
    var events(default, default):EventList;
}
