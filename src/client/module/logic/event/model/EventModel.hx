package module.logic.event.model;
import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.logic.event.model.IEventModel;
import module.logic.event.model.IEventModelListener;
import types.complex.event.EventList;

/**
 * ...
 * @author d3relict
 */
class EventModel extends BasicModel<EventModelDispatcher, IEventModelListener> implements IEventModel
{
    public var events:EventList;

    public function new()
    {
        super();

        events = new EventList();
    }

}

private class EventModelDispatcher extends ModelDispatcher<IEventModelListener> implements IEventModelListener
{
}
