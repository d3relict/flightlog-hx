package module.logic.event.controller;
import types.complex.event.Event;
import types.complex.event.EventList;

/**
 * @author d3relict
 */
interface IEventProviderController
{
    function getLastEventForBattery(id:String):Event;
    function getLastEventForAircraft(id:String):Event;

    function getAllEventsForBattery(id:String):EventList;
    function getAllEventsForAircraft(id:String):EventList;
}
