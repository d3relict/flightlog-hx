package module.logic.event.controller;
import types.complex.event.Event;
import types.complex.transaction.EventTransaction;
import types.complex.transaction.RemovalTransaction;
import types.primitive.Identity;

/**
 * @author d3relict
 */
interface IEventUpdaterController
{
    function addEvent(event:Event):Void;
    function removeEvent(id:Identity):Void;

    function scrutinize():Void;
}
