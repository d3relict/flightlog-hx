package module.logic.event.controller;

import const.EventTypes;
import hex.di.IInjectorContainer;
import hex.module.IModule;
import interfaces.logic.IAircraftProvider;
import interfaces.logic.IBatteryProvider;
import interfaces.util.ILocalStorage;
import module.logic.common.interfaces.IStorageController;
import module.logic.common.message.ProviderMessage;
import module.logic.event.message.EventModuleMessage;
import module.logic.event.model.IEventModel;
import types.complex.aircraft.Aircraft;
import types.complex.battery.Battery;
import types.complex.event.ChargeList;
import types.complex.event.Event;
import types.complex.event.EventList;
import types.complex.transaction.EventTransaction;
import types.complex.event.Flight;
import types.complex.transaction.RemovalTransaction;
import types.primitive.Identity;

/**
 * ...
 * @author d3relict
 */
class EventModuleController
    implements IStorageController
    implements IEventProviderController
    implements IEventUpdaterController
    implements IInjectorContainer
{
    @Inject
    public var eventModel:IEventModel;
    @Inject
    public var storage:ILocalStorage;
    @Inject
    public var module:IModule;
    @Inject
    public var aircraftProvider:IAircraftProvider;
    @Inject
    public var batteryProvider:IBatteryProvider;

    public function addEvent(event:Event):Void
    {
        var clonedFlight:Flight = null;
        var clonedCharges:ChargeList = null;

        if (event.flight != null)
        {
            clonedFlight = {
                id:         event.flight.id,
                duration:   event.flight.duration
            };
        }

        if (event.charges != null)
        {
            clonedCharges = new ChargeList();
            for (charge in event.charges)
            {
                clonedCharges.push({
                    id:     charge.id,
                    charge: charge.charge,
                    level:  charge.level
                });
            }
        }

        var clonedEvent:Event = {
            id:             event.id,
            eventType:      event.eventType,
            timestamp:      event.timestamp,

            flight:         clonedFlight,

            charges:        clonedCharges
        }

        eventModel.events.push(clonedEvent);

        switch (event.eventType)
        {
            case EventTypes.FLIGHT:
                module.dispatchPublicMessage(EventModuleMessage.ADD_FLIGHT, [event]);
            default:
                module.dispatchPublicMessage(EventModuleMessage.ADD_CHARGE, [event]);
        }

        module.dispatchPublicMessage(ProviderMessage.UPDATE);
    }

    public function removeEvent(identity:Identity):Void
    {
        var removedEvent:Event;
        for (event in eventModel.events)
            if (event.id == identity.id)
            {
                removedEvent = eventModel.events.splice(eventModel.events.indexOf(event), 1)[0];
                switch (removedEvent.eventType)
                {
                    case EventTypes.FLIGHT:
                        module.dispatchPublicMessage(EventModuleMessage.REMOVE_FLIGHT, [removedEvent]);
                    default:
                        module.dispatchPublicMessage(EventModuleMessage.REMOVE_CHARGE, [removedEvent]);
                }
                break;
            }

        module.dispatchPublicMessage(ProviderMessage.UPDATE);
    }

    public function scrutinize():Void
    {
        var aircrafts:Map<String,Aircraft> = new Map<String, Aircraft>();
        var batteries:Map<String,Battery> = new Map<String, Battery>();
        var event:Event;

        var orphaned:Bool;

        var i:Int = 0;
        while (i < eventModel.events.length)
        {
            event = eventModel.events[i];

            orphaned = true;

            if (event.flight != null)
            {
                if (aircrafts[event.flight.id] == null)
                    aircrafts[event.flight.id] = aircraftProvider.getAircraftById(event.flight.id);

                if (aircrafts[event.flight.id] != null)
                    orphaned = false;
            }

            for (charge in event.charges)
            {
                if (batteries[charge.id] == null)
                    batteries[charge.id] = batteryProvider.getBatteryById(charge.id);

                if (batteries[charge.id] != null)
                    orphaned = false;
            }

            if (orphaned)
            {
                eventModel.events.splice(i, 1);
                continue;
            }

            i++;
        }
    }

    public function fetch():Void
    {
        var stored:EventList = storage.get('events');
        eventModel.events = stored != null ? stored : new EventList();
    }

    public function store():Void
    {
        storage.set('events', eventModel.events);
    }

    public function getLastEventForBattery(id:String):Event
    {
        var i:Int = eventModel.events.length;
        while (i-- > 0)
            for (charge in eventModel.events[i].charges)
                if (charge.id == id)
                    return eventModel.events[i];

        return null;
    }

    public function getLastEventForAircraft(id:String):Event
    {
        var i:Int = eventModel.events.length;
        while(i-- > 0)
            if (eventModel.events[i].flight != null && eventModel.events[i].flight.id == id)
                return eventModel.events[i];
        return null;
    }

    public function getAllEventsForBattery(id:String):EventList
    {
        return eventModel.events.filter(
            function(event)
            {
                for (charge in event.charges)
                    if (charge.id == id)
                        return true;

                return false;
            }
        );
    }

    public function getAllEventsForAircraft(id:String):EventList
    {
        return eventModel.events.filter(
            function(event)
            {
                return event.flight != null && event.flight.id == id;
            }
        );
    }
}
