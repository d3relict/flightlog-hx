package module.logic.event;
import hex.config.stateless.StatelessModuleConfig;
import hex.module.IModule;
import hex.module.Module;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.RuntimeDependencies;
import interfaces.logic.IAircraftProvider;
import interfaces.logic.IBatteryProvider;
import interfaces.logic.IEventProvider;
import interfaces.logic.IEventUpdater;
import interfaces.util.ILocalStorage;
import module.logic.common.interfaces.IStorageController;
import module.logic.event.controller.EventModuleController;
import module.logic.event.controller.IEventProviderController;
import module.logic.event.controller.IEventUpdaterController;
import module.logic.event.model.EventModel;
import module.logic.event.model.IEventModel;
import types.complex.event.Event;
import types.complex.event.EventList;
import types.complex.transaction.EventTransaction;
import types.complex.transaction.RemovalTransaction;
import types.primitive.Identity;

/**
 * ...
 * @author d3relict
 */
class EventModule extends Module
    implements IModule
    implements IEventProvider
    implements IEventUpdater
{
    public var events(get, never):EventList;

    public function new(localStorage:ILocalStorage, aircraftProvider:IAircraftProvider, batteryProvider:IBatteryProvider)
    {
        super();

        _injector.mapToValue(ILocalStorage,     localStorage);
        _injector.mapToValue(IAircraftProvider, aircraftProvider);
        _injector.mapToValue(IBatteryProvider,  batteryProvider);

        _addStatelessConfigClasses([EventModuleConfig]);

        _get(IStorageController).fetch();
    }

    override function _getRuntimeDependencies():IRuntimeDependencies
    {
        return new RuntimeDependencies();
    }

    public function get_events():EventList
    {
        return _get(IEventModel).events;
    }

    public function addEvent(event:Event):Void
    {
        _get(IEventUpdaterController).addEvent(event);
        _get(IStorageController).store();
    }

    public function removeEvent(identity:Identity):Void
    {
        _get(IEventUpdaterController).removeEvent(identity);
        _get(IStorageController).store();
    }

    public function scrutinize():Void
    {
        _get(IEventUpdaterController).scrutinize();
        _get(IStorageController).store();
    }

    public function getLastEventForBattery(id:String):Event
    {
        return _get(IEventProviderController).getLastEventForBattery(id);
    }

    public function getLastEventForAircraft(id:String):Event
    {
        return _get(IEventProviderController).getLastEventForAircraft(id);
    }

    public function getAllEventsForBattery(id:String):EventList
    {
        return _get(IEventProviderController).getAllEventsForBattery(id);
    }

    public function getAllEventsForAircraft(id:String):EventList
    {
        return _get(IEventProviderController).getAllEventsForAircraft(id);
    }
}

private class EventModuleConfig extends StatelessModuleConfig
{
    override public function configure():Void
    {
        mapModel(IEventModel, EventModel);

        mapController(IEventProviderController, EventModuleController);
        mapController(IEventUpdaterController,  EventModuleController);
        mapController(IStorageController,       EventModuleController);
    }
}
