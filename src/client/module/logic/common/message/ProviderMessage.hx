package module.logic.common.message;
import hex.event.MessageType;

/**
 * ...
 * @author d3relict
 */
class ProviderMessage
{
    public static var UPDATE(default, never):MessageType = new MessageType('update');
}
