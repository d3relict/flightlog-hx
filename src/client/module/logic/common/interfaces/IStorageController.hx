package module.logic.common.interfaces;

/**
 * @author d3relict
 */
interface IStorageController 
{
    function fetch():Void;
    function store():Void;
}
