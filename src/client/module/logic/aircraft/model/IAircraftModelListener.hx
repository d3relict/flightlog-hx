package module.logic.aircraft.model;

/**
 * @author d3relict
 */
interface IAircraftModelListener 
{
    function onUpdate():Void;
}
