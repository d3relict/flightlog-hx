package module.logic.aircraft.model;
import hex.model.IModelRO;
import module.logic.aircraft.model.IAircraftModelListener;
import types.complex.aircraft.AircraftList;

/**
 * @author d3relict
 */
interface IAircraftModelRO extends IModelRO<IAircraftModelListener>
{
    var aircrafts(default, never):AircraftList;
}
