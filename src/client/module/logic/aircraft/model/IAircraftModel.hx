package module.logic.aircraft.model;
import types.complex.aircraft.AircraftList;

/**
 * @author d3relict
 */
interface IAircraftModel extends IAircraftModelRO
{
    var aircrafts(default, set):AircraftList;
}
