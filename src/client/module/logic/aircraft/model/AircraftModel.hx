package module.logic.aircraft.model;

import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.logic.aircraft.model.IAircraftModel;
import module.logic.aircraft.model.IAircraftModelListener;
import types.complex.aircraft.AircraftList;

/**
 * ...
 * @author d3relict
 */
class AircraftModel extends BasicModel<AircraftModelDispatcher, IAircraftModelListener> implements IAircraftModel
{
    public var aircrafts(default, set):AircraftList = new AircraftList();

    public function new()
    {
        super();
    }

    function set_aircrafts(value)
    {
        aircrafts = value;
        dispatcher.onUpdate();
        return value;
    }
}

private class AircraftModelDispatcher extends ModelDispatcher<IAircraftModelListener> implements IAircraftModelListener
{
    public function onUpdate():Void;
}
