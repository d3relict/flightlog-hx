package module.logic.aircraft;
import hex.config.stateless.StatelessModuleConfig;
import hex.module.IModule;
import hex.module.Module;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.RuntimeDependencies;
import interfaces.logic.IAircraftProvider;
import interfaces.logic.IAircraftUpdater;
import interfaces.logic.ISettingsProvider;
import interfaces.util.ILocalStorage;
import module.logic.aircraft.controller.AircraftModuleController;
import module.logic.aircraft.controller.IAircraftProviderController;
import module.logic.aircraft.controller.IAircraftUpdaterController;
import module.logic.aircraft.model.AircraftModel;
import module.logic.aircraft.model.IAircraftModel;
import module.logic.common.interfaces.IStorageController;
import types.complex.aircraft.Aircraft;
import types.complex.aircraft.AircraftList;
import types.complex.transaction.AircraftTransaction;
import types.complex.event.Event;
import types.complex.transaction.EventTransaction;
import types.complex.transaction.RemovalTransaction;
import types.primitive.Identity;

/**
 * ...
 * @author d3relict
 */
class AircraftModule extends Module
implements IAircraftProvider
implements IAircraftUpdater
implements IModule
{
    public var aircrafts(get, never):AircraftList;

    public function new(settingsProvider:ISettingsProvider, localStorage:ILocalStorage)
    {
        super();

        _injector.mapToValue(ISettingsProvider, settingsProvider);
        _injector.mapToValue(ILocalStorage,     localStorage);

        _addStatelessConfigClasses([AircraftModuleConfig]);

        _get(IStorageController).fetch();
    }

    override function _getRuntimeDependencies():IRuntimeDependencies
    {
        return new RuntimeDependencies();
    }

    public function get_aircrafts():AircraftList
    {
        return _get(IAircraftModel).aircrafts;
    }

    public function getAircraftById(id:String):Aircraft
    {
        return _get(IAircraftProviderController).getAircraftById(id);
    }

    public function addAircraft(aircraft:Aircraft):Void
    {
        //TODO: better solution for update
        if (getAircraftById(aircraft.id) != null)
            _get(IAircraftUpdaterController).updateAircraft(aircraft);
        else
            _get(IAircraftUpdaterController).addAircraft(aircraft);

        _get(IStorageController).store();
    }

    public function removeAircraft(identity:Identity):Void
    {
        _get(IAircraftUpdaterController).removeAircraft(identity);
        _get(IStorageController).store();
    }

    public function applyFlight(flight:Event):Void
    {
        _get(IAircraftUpdaterController).applyFlight(flight);
        _get(IStorageController).store();
    }

    public function undoFlight(flight:Event):Void
    {
        _get(IAircraftUpdaterController).undoFlight(flight);
        _get(IStorageController).store();
    }
}

private class AircraftModuleConfig extends StatelessModuleConfig
{
    override public function configure():Void
    {
        this.mapModel(IAircraftModel,  AircraftModel);

        this.mapController(IAircraftProviderController,    AircraftModuleController);
        this.mapController(IAircraftUpdaterController,     AircraftModuleController);
        this.mapController(IStorageController,             AircraftModuleController);
    }
}
