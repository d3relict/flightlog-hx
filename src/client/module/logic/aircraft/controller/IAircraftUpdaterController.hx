package module.logic.aircraft.controller;
import types.complex.aircraft.Aircraft;
import types.complex.event.Event;
import types.primitive.Identity;

/**
 * @author d3relict
 */
interface IAircraftUpdaterController
{
    function addAircraft(aircraft:Aircraft):Void;
    function updateAircraft(aircraft:Aircraft):Void;
    function removeAircraft(identity:Identity):Void;

    function applyFlight(flight:Event):Void;
    function undoFlight(flight:Event):Void;
}
