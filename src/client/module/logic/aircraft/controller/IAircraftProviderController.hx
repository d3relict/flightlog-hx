package module.logic.aircraft.controller;
import types.complex.aircraft.Aircraft;

/**
 * @author d3relict
 */
interface IAircraftProviderController
{
    function getAircraftById(aircraftId:String):Aircraft;
}
