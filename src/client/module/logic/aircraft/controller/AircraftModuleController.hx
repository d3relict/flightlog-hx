package module.logic.aircraft.controller;
import hex.di.IInjectorContainer;
import hex.module.IModule;
import interfaces.logic.ISettingsProvider;
import interfaces.util.ILocalStorage;
import module.logic.aircraft.model.IAircraftModel;
import module.logic.common.interfaces.IStorageController;
import module.logic.common.message.ProviderMessage;
import types.complex.aircraft.Aircraft;
import types.complex.event.Event;
import types.complex.transaction.EventTransaction;
import types.primitive.Identity;
import util.ObjectHelper;

/**
 * ...
 * @author d3relict
 */
class AircraftModuleController
    implements IAircraftProviderController
    implements IAircraftUpdaterController
    implements IStorageController
    implements IInjectorContainer
{
    @Inject
    public var aircraftModel:IAircraftModel;
    @Inject
    public var settingsProvider:ISettingsProvider;
    @Inject
    public var storage:ILocalStorage;
    @Inject
    public var module:IModule;

    /* INTERFACE module.data.aircraft.controller.IAircraftUpdaterController */

    public function addAircraft(aircraft:Aircraft):Void
    {
        var clonedAircraft:Aircraft = {
            id:                     aircraft.id,

            manufacturer:           aircraft.manufacturer,
            type:                   aircraft.type,
            name:                   aircraft.name,

            usualDuration:          aircraft.usualDuration,
            totalDuration:          aircraft.totalDuration,
            totalCount:             aircraft.totalCount,

            cellCount:              aircraft.cellCount
        };

        var defaultValues:Dynamic = {
            usualDuration:          Math.floor(settingsProvider.defaultMaxFlightDuration / 2),
            totalDuration:          0,
            totalCount:             0
        };

        ObjectHelper.setFieldDefaults(clonedAircraft, defaultValues);

        aircraftModel.aircrafts.push(clonedAircraft);

        module.dispatchPublicMessage(ProviderMessage.UPDATE);
    }

    public function updateAircraft(aircraft:Aircraft):Void
    {
        var existing:Aircraft = getAircraftById(aircraft.id);

        existing.name =         aircraft.name;
        existing.manufacturer = aircraft.manufacturer;
        existing.type =         aircraft.type;
        existing.cellCount =    aircraft.cellCount;

        module.dispatchPublicMessage(ProviderMessage.UPDATE);
    }

    public function removeAircraft(identity:Identity):Void
    {
        for (aircraft in aircraftModel.aircrafts)
            if (aircraft.id == identity.id)
            {
                aircraftModel.aircrafts.splice(aircraftModel.aircrafts.indexOf(aircraft), 1);
                break;
            }

        module.dispatchPublicMessage(ProviderMessage.UPDATE);
    }

    public function applyFlight(event:Event):Void
    {
        var aircraft:Aircraft = getAircraftById(event.flight.id);
        aircraft.totalCount++;
        aircraft.totalDuration += event.flight.duration;
        module.dispatchPublicMessage(ProviderMessage.UPDATE);
    }

    public function undoFlight(event:Event):Void
    {
        var aircraft:Aircraft = getAircraftById(event.flight.id);
        aircraft.totalCount--;
        aircraft.totalDuration -= event.flight.duration;
        module.dispatchPublicMessage(ProviderMessage.UPDATE);
    }

    /* INTERFACE module.data.aircraft.controller.IAircraftProviderController */

    public function getAircraftById(id:String):Aircraft
    {
        for (aircraft in aircraftModel.aircrafts)
        {
            if (aircraft.id == id)
            {
                return aircraft;
            }
        }
        return null;
    }


    public function fetch():Void
    {
        var stored:Dynamic = storage.get('aircrafts');
        if (stored != null)
        {
            aircraftModel.aircrafts = cast stored;
        }
    }

    public function store():Void
    {
        storage.set('aircrafts', aircraftModel.aircrafts);
    }

}
