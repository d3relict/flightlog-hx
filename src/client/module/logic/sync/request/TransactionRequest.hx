package module.logic.sync.request;

import hex.control.Request;
import types.primitive.TransactionProperties;

/**
 * ...
 * @author d3relict
 */
class TransactionRequest extends Request
{
    public var transaction(default, null):TransactionProperties;

    public function new(transaction:TransactionProperties)
    {
        super();

        this.transaction = transaction;
    }
}
