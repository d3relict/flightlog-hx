package module.logic.sync.model;
import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.logic.sync.model.interfaces.ITransactionModel;
import module.logic.sync.model.interfaces.ITransactionModelListener;
import types.complex.transaction.TransactionList;

/**
 * ...
 * @author d3relict
 */
class TransactionModel extends BasicModel<TransactionModelDispatcher, ITransactionModelListener> implements ITransactionModel
{
    public var queue:TransactionList;
    public var pending:TransactionList;

    public function new()
    {
        super();

        queue = new TransactionList();
        pending = new TransactionList();
    }

}

private class TransactionModelDispatcher extends ModelDispatcher<ITransactionModelListener> implements ITransactionModelListener
{
}
