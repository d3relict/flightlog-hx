package module.logic.sync.model.interfaces;
import hex.model.IModelRO;

/**
 * @author d3relict
 */
interface IAuthenticationModelRO extends IModelRO<IAuthenticationModelListener>
{
    var userId(default, null):String;
}
