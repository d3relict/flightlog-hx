package module.logic.sync.model.interfaces;
import module.logic.sync.model.interfaces.IAuthenticationModelRO;

/**
 * @author d3relict
 */
interface IAuthenticationModel extends IAuthenticationModelRO
{
    var userId(default, default):String;
}
