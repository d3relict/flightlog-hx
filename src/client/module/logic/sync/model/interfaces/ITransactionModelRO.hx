package module.logic.sync.model.interfaces;
import hex.model.IModelRO;
import types.complex.transaction.TransactionList;

/**
 * @author d3relict
 */
interface ITransactionModelRO extends IModelRO<ITransactionModelListener>
{
    var queue(default, null):TransactionList;
    var pending(default, null):TransactionList;
}
