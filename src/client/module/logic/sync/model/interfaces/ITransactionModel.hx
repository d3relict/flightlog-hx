package module.logic.sync.model.interfaces;
import module.logic.sync.model.interfaces.ITransactionModelRO;
import types.complex.transaction.TransactionList;

/**
 * @author d3relict
 */
interface ITransactionModel extends ITransactionModelRO
{
    var queue(default, default):TransactionList;
    var pending(default, default):TransactionList;
}
