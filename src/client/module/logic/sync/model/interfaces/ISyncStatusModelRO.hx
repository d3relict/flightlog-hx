package module.logic.sync.model.interfaces;
import hex.model.IModelRO;

/**
 * @author d3relict
 */
interface ISyncStatusModelRO extends IModelRO<ISyncStatusModelListener>
{
    var status(default, null):UInt;
    var syncId(default, null):UInt;
    var lastSuccess(default, null):Date;
    var lastAttempt(default, null):Date;
}
