package module.logic.sync.model.interfaces;
import module.logic.sync.model.interfaces.ISyncStatusModelRO;

/**
 * @author d3relict
 */
interface ISyncStatusModel extends ISyncStatusModelRO
{
    var status(default, default):UInt;
    var syncId(default, default):UInt;
    var lastSuccess(default, default):Date;
    var lastAttempt(default, default):Date;
}
