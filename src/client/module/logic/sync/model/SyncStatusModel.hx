package module.logic.sync.model;
import const.SyncStatii;
import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.logic.sync.model.interfaces.ISyncStatusModel;
import module.logic.sync.model.interfaces.ISyncStatusModelListener;
import types.complex.transaction.TransactionList;

/**
 * ...
 * @author d3relict
 */
class SyncStatusModel extends BasicModel<SyncStatusModelDispatcher, ISyncStatusModelListener> implements ISyncStatusModel
{
    public var status:UInt;
    public var syncId:UInt;
    public var lastSuccess:Date;
    public var lastAttempt:Date;

    public function new()
    {
        super();

        syncId = 0;
        status = SyncStatii.UNIDENTIFIED;
    }

}

private class SyncStatusModelDispatcher extends ModelDispatcher<ISyncStatusModelListener> implements ISyncStatusModelListener
{
}
