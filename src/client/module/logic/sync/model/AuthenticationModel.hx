package module.logic.sync.model;
import hex.model.BasicModel;
import hex.model.ModelDispatcher;
import module.logic.sync.model.interfaces.IAuthenticationModel;
import module.logic.sync.model.interfaces.IAuthenticationModelListener;

/**
 * ...
 * @author d3relict
 */
class AuthenticationModel extends BasicModel<AuthenticationModelDispatcher, IAuthenticationModelListener> implements IAuthenticationModel
{
    public var userId:String;
    public var authenticated:Bool;

    public function new()
    {
        super();

        userId = null;
        authenticated = false;
    }

}

private class AuthenticationModelDispatcher extends ModelDispatcher<IAuthenticationModelListener> implements IAuthenticationModelListener
{
}
