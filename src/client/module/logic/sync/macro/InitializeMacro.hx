package module.logic.sync.macro;

import hex.control.macro.Macro;
import module.logic.sync.controller.*;

/**
 * ...
 * @author d3relict
 */
class InitializeMacro extends Macro
{

    public function new()
    {
        super();
    }

    override function _prepare():Void
    {
        add(FetchDataCommand);
        add(SetInitialStatusCommand);
        add(SynchronizeMacro);
    }
}
