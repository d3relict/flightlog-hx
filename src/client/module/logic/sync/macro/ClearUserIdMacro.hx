package module.logic.sync.macro;

import hex.control.macro.Macro;
import module.logic.sync.controller.AddToQueueCommand;
import module.logic.sync.controller.ClearUserIdCommand;
import module.logic.sync.controller.SetUserIdCommand;
import module.logic.sync.controller.StoreDataCommand;
import module.logic.sync.controller.SynchronizeCommand;
import module.logic.sync.guard.UserIdSetGuard;

/**
 * ...
 * @author d3relict
 */
class ClearUserIdMacro extends Macro
{
    public function new()
    {
        super();
    }

    override function _prepare():Void
    {
        add(ClearUserIdCommand);
        add(StoreDataCommand);
    }
}
