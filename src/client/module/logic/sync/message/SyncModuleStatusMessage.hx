package module.logic.sync.message;
import hex.event.MessageType;

/**
 * ...
 * @author d3relict
 */
class SyncModuleStatusMessage
{
    public static var USER_ID_ACCEPTED  (default, never):MessageType = new MessageType('userIdAccepter');
    public static var USER_ID_REJECTED  (default, never):MessageType = new MessageType('userIdRejected');
    public static var USER_ID_CLEARED   (default, never):MessageType = new MessageType('userIdCleared');

    public static var SYNC_STARTED      (default, never):MessageType = new MessageType('syncStarted');
    public static var SYNC_COMPLETE     (default, never):MessageType = new MessageType('syncComplete');
    public static var SYNC_FAILED       (default, never):MessageType = new MessageType('syncFailed');
}
