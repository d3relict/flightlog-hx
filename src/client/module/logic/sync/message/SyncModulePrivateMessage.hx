package module.logic.sync.message;
import hex.event.MessageType;

/**
 * ...
 * @author d3relict
 */
class SyncModulePrivateMessage
{
    public static var INITIALIZE(default, never):MessageType =      new MessageType('initialize');

    public static var SET_USER_ID(default, never):MessageType =     new MessageType('setUserId');
    public static var CLEAR_USER_ID(default, never):MessageType =   new MessageType('clearUserId');

    public static var FETCH(default, never):MessageType =           new MessageType('fetch');
    public static var STORE(default, never):MessageType =           new MessageType('store');

    public static var ADD_TO_QUEUE(default, never):MessageType =    new MessageType('addToQueue');
    public static var START_SYNC(default, never):MessageType =      new MessageType('startSync');
}
