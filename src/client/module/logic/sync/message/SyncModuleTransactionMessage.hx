package module.logic.sync.message;
import hex.event.MessageType;

/**
 * ...
 * @author d3relict
 */
class SyncModuleTransactionMessage
{
    public static var ADD_AIRCRAFT      (default, never):MessageType = new MessageType('addAircraft');
    public static var ADD_BATTERY       (default, never):MessageType = new MessageType('addBattery');
    public static var ADD_EVENT         (default, never):MessageType = new MessageType('addEvent');

    public static var REMOVE_AIRCRAFT   (default, never):MessageType = new MessageType('removeAircraft');
    public static var REMOVE_BATTERY    (default, never):MessageType = new MessageType('removeBattery');
    public static var REMOVE_EVENT      (default, never):MessageType = new MessageType('removeEvent');
}
