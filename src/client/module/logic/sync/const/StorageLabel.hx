package module.logic.sync.const;

/**
 * ...
 * @author d3relict
 */
class StorageLabel
{
    public static var TRANSACTIONS(default, never):String = 'trs';

    public static var USER_ID(default, never):String =      'uid';
    public static var SYNC_ID(default, never):String =      'sid';

    public static var LAST_SUCCESS(default, never):String = 'lsu';
    public static var LAST_ATTEMPT(default, never):String = 'lat';
}
