package module.logic.sync.controller;

import hex.control.command.BasicCommand;
import interfaces.util.ILocalStorage;
import module.logic.sync.const.StorageLabel;
import module.logic.sync.model.interfaces.IAuthenticationModel;
import module.logic.sync.model.interfaces.ISyncStatusModel;
import module.logic.sync.model.interfaces.ITransactionModel;
import types.complex.transaction.TransactionList;

/**
 * ...
 * @author d3relict
 */
class FetchDataCommand extends BasicCommand
{
    @Inject
    public var transactionModel:ITransactionModel;

    @Inject
    public var authenticationModel:IAuthenticationModel;

    @Inject
    public var syncStatusModel:ISyncStatusModel;

    @Inject
    public var storage:ILocalStorage;

    public function new()
    {
        super();
    }

    public function execute():Void
    {
        getLogger().debug('fetch data');

        var transactions:TransactionList = storage.get(StorageLabel.TRANSACTIONS);
        transactionModel.queue = transactions != null ? transactions : new TransactionList();

        authenticationModel.userId = storage.get(StorageLabel.USER_ID);

        syncStatusModel.lastSuccess = storage.get(StorageLabel.LAST_SUCCESS) != null ? storage.get(StorageLabel.LAST_SUCCESS) : null;
        syncStatusModel.lastAttempt = storage.get(StorageLabel.LAST_ATTEMPT) != null ? storage.get(StorageLabel.LAST_ATTEMPT) : null;
        syncStatusModel.syncId = storage.get(StorageLabel.SYNC_ID) != null ? storage.get(StorageLabel.SYNC_ID) : 0;
    }
}
