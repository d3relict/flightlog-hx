package module.logic.sync.controller;
import hex.control.command.BasicCommand;
import module.logic.sync.model.interfaces.IAuthenticationModel;

/**
 * ...
 * @author d3relict
 */
class SetUserIdCommand extends BasicCommand
{
    @Inject
    public var userId:String;

    @Inject
    public var authenticationModel:IAuthenticationModel;

    public function new()
    {
        super();
    }

    public function execute():Void
    {
        authenticationModel.userId = userId;
    }
}
