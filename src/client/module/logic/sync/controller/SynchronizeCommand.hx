package module.logic.sync.controller;
import const.SyncStatii;
import const.TransactionTypes;
import haxe.Serializer;
import haxe.Unserializer;
import hex.control.async.AsyncCommand;
import hex.error.IllegalArgumentException;
import hex.event.MessageType;
import hex.module.IModule;
import hex.service.stateless.IAsyncStatelessService;
import hex.service.stateless.IAsyncStatelessServiceListener;
import interfaces.service.ISyncService;
import module.logic.sync.message.SyncModuleStatusMessage;
import module.logic.sync.message.SyncModuleTransactionMessage;
import module.logic.sync.model.interfaces.IAuthenticationModel;
import module.logic.sync.model.interfaces.ISyncStatusModel;
import module.logic.sync.model.interfaces.ITransactionModel;
import service.sync.SyncServiceParameters;
import service.sync.request.RequestType;
import service.sync.request.SyncRequest;
import service.sync.response.SyncResponse;
import types.primitive.TransactionProperties;

/**
 * ...
 * @author d3relict
 */
class SynchronizeCommand extends AsyncCommand implements IAsyncStatelessServiceListener
{
    @Inject
    public var syncService:ISyncService;

    @Inject
    public var transactionModel:ITransactionModel;

    @Inject
    public var syncStatusModel:ISyncStatusModel;

    @Inject
    public var authenticationModel:IAuthenticationModel;

    @Inject
    public var module:IModule;

    public function new()
    {
        super();
    }

    public function execute():Void
    {
        getLogger().debug('SynchronizeCommand');
        var serializedTransactions:Array<String> = [];
        for (transaction in transactionModel.queue)
        {
            transactionModel.pending.push(transaction);
            serializedTransactions.push(Serializer.run(transaction));
        }

        var syncRequest:SyncRequest = {
            type: RequestType.sync,
            userId: authenticationModel.userId,
            lastSyncId: syncStatusModel.syncId,
            transactions: serializedTransactions
        };

        syncService.setParameters(new SyncServiceParameters(syncRequest));
        syncService.addListener(this);
		syncService.call();

        syncStatusModel.status = SyncStatii.PROGRESS;
        syncStatusModel.lastAttempt = Date.now();
        getOwner().dispatchPublicMessage(SyncModuleStatusMessage.SYNC_STARTED);
    }

    private function handleResponse(response:SyncResponse):Void
    {
        var transaction:TransactionProperties;
        var messageMap:Map<UInt, MessageType> = new Map<UInt, MessageType>();
            messageMap.set(TransactionTypes.ADD_AIRCRAFT,       SyncModuleTransactionMessage.ADD_AIRCRAFT);
            messageMap.set(TransactionTypes.REMOVE_AIRCRAFT,    SyncModuleTransactionMessage.REMOVE_AIRCRAFT);
            messageMap.set(TransactionTypes.ADD_BATTERY,        SyncModuleTransactionMessage.ADD_BATTERY);
            messageMap.set(TransactionTypes.REMOVE_BATTERY,     SyncModuleTransactionMessage.REMOVE_BATTERY);
            messageMap.set(TransactionTypes.ADD_EVENT,          SyncModuleTransactionMessage.ADD_EVENT);
            messageMap.set(TransactionTypes.REMOVE_EVENT,       SyncModuleTransactionMessage.REMOVE_EVENT);

        var message:MessageType;

        syncStatusModel.syncId = response.syncId;

        for (serializedTransaction in response.transactions)
        {
            transaction = Unserializer.run(serializedTransaction);
            message = messageMap.get(transaction.txType);

            if (message != null)
                module.dispatchPublicMessage(message, [transaction]);
            else
                throw new IllegalArgumentException('Unknown transaction type: ' + transaction.txType);
        }
    }

    private function removePendingTransactions():Void
    {
        while (transactionModel.pending.length > 0)
        {
            transactionModel.queue.splice(transactionModel.queue.indexOf(transactionModel.pending[0]), 1);
            transactionModel.pending.splice(0, 1);
        }
    }

    private function clearPendingTransactions():Void
    {
        transactionModel.pending = [];
    }

    public function onServiceComplete(service:IAsyncStatelessService):Void
    {
        getLogger().debug('service complete');

        handleResponse(cast syncService.result);

        //TODO: move to command
        removePendingTransactions();

        syncStatusModel.status = SyncStatii.WAITING;
        syncStatusModel.lastSuccess = Date.now();

        getOwner().dispatchPublicMessage(SyncModuleStatusMessage.SYNC_COMPLETE);

        _handleComplete();
    }

    public function onServiceFail(service:IAsyncStatelessService):Void
    {
        getLogger().error('service failed');

        syncStatusModel.status = SyncStatii.FAILED;

        syncFailed();
    }

    public function onServiceCancel(service:IAsyncStatelessService):Void
    {
        getLogger().warn('service canceled');

        //TODO: move to command
        clearPendingTransactions();

        _handleCancel();
    }

    public function onServiceTimeout(service:IAsyncStatelessService):Void
    {
        getLogger().warn('service timeout');

        syncStatusModel.status = SyncStatii.OFFLINE;

        syncFailed();
    }

    private function syncFailed():Void
    {
        //TODO: move to command
        clearPendingTransactions();
        getOwner().dispatchPublicMessage(SyncModuleStatusMessage.SYNC_FAILED);
        _handleFail();
    }
}
