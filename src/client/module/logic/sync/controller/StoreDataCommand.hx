package module.logic.sync.controller;

import hex.control.command.BasicCommand;
import interfaces.util.ILocalStorage;
import module.logic.sync.const.StorageLabel;
import module.logic.sync.model.interfaces.IAuthenticationModel;
import module.logic.sync.model.interfaces.ISyncStatusModel;
import module.logic.sync.model.interfaces.ITransactionModel;

/**
 * ...
 * @author d3relict
 */
class StoreDataCommand extends BasicCommand
{
    @Inject
    public var transactionModel:ITransactionModel;

    @Inject
    public var authenticationModel:IAuthenticationModel;

    @Inject
    public var syncStatusModel:ISyncStatusModel;

    @Inject
    public var storage:ILocalStorage;

    public function new()
    {
        super();
    }

    public function execute():Void
    {
        getLogger().debug('store data');

        storage.set(StorageLabel.TRANSACTIONS,  transactionModel.queue);

        storage.set(StorageLabel.USER_ID,       authenticationModel.userId);
        storage.set(StorageLabel.SYNC_ID,       syncStatusModel.syncId);

        storage.set(StorageLabel.LAST_SUCCESS,  syncStatusModel.lastSuccess);
        storage.set(StorageLabel.LAST_ATTEMPT,  syncStatusModel.lastAttempt);
    }
}
