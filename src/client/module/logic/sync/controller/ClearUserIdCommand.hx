package module.logic.sync.controller;
import hex.control.command.BasicCommand;
import module.logic.sync.message.SyncModuleStatusMessage;
import module.logic.sync.model.interfaces.IAuthenticationModel;

/**
 * ...
 * @author d3relict
 */
class ClearUserIdCommand extends BasicCommand
{
    @Inject
    public var authenticationModel:IAuthenticationModel;

    public function new()
    {
        super();
    }

    public function execute():Void
    {
        authenticationModel.userId = null;
        getOwner().dispatchPublicMessage(SyncModuleStatusMessage.USER_ID_CLEARED);
    }
}
