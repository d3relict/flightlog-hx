package module.logic.sync.controller;

import hex.control.command.BasicCommand;
import interfaces.util.ILocalStorage;
import module.logic.sync.model.interfaces.ITransactionModel;
import module.logic.sync.request.TransactionRequest;
import types.primitive.TransactionProperties;

/**
 * ...
 * @author d3relict
 */
class AddToQueueCommand extends BasicCommand
{
    @Inject
    public var transactionModel:ITransactionModel;

    public function new()
    {
        super();
    }

    public function execute(request:TransactionRequest):Void
    {
        getLogger().debug('add to queue');
        transactionModel.queue.push(request.transaction);
    }
}
