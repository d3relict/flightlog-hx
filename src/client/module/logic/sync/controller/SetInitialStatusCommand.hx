package module.logic.sync.controller;

import const.SyncStatii;
import hex.control.command.BasicCommand;
import module.logic.sync.model.interfaces.IAuthenticationModel;
import module.logic.sync.model.interfaces.ISyncStatusModel;
import module.logic.sync.model.interfaces.ITransactionModel;

/**
 * ...
 * @author d3relict
 */
class SetInitialStatusCommand extends BasicCommand
{
    @Inject
    public var transactionModel:ITransactionModel;

    @Inject
    public var authenticationModel:IAuthenticationModel;

    @Inject
    public var syncStatusModel:ISyncStatusModel;

    public function new()
    {
        super();
    }

    public function execute():Void
    {
        getLogger().debug('set initial status');

        if (authenticationModel.userId == null)
        {
            syncStatusModel.status = SyncStatii.UNIDENTIFIED;
        }
        else if (transactionModel.queue.length > 0)
        {
            syncStatusModel.status = SyncStatii.QUEUED;
        }
        else
        {
            syncStatusModel.status = SyncStatii.WAITING;
        }
    }
}
