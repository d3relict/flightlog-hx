package module.logic.sync.guard;
import hex.control.guard.IGuard;
import hex.di.IInjectorContainer;
import module.logic.sync.model.interfaces.IAuthenticationModel;

/**
 * ...
 * @author d3relict
 */
class UserIdSetGuard implements IGuard implements IInjectorContainer
{
    @Inject
    public var authenticationModel:IAuthenticationModel;

    public function new()
    {
    }

    public function approve():Bool
    {
        return authenticationModel.userId != null && authenticationModel.userId != "";
    }
}
