package module.logic.sync;
import haxe.Http;
import haxe.Serializer;
import haxe.Timer;
import haxe.Unserializer;
import hex.config.stateful.IStatefulConfig;
import hex.config.stateless.StatelessCommandConfig;
import hex.config.stateless.StatelessModelConfig;
import hex.config.stateless.StatelessModuleConfig;
import hex.control.Request;
import hex.control.payload.ExecutionPayload;
import hex.module.IModule;
import hex.module.Module;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.RuntimeDependencies;
import interfaces.logic.ISynchronizer;
import interfaces.util.ILocalStorage;
import module.logic.sync.controller.*;
import module.logic.sync.macro.*;
import module.logic.sync.model.interfaces.*;
import module.logic.sync.model.*;
import module.logic.sync.message.SyncModulePrivateMessage;
import module.logic.sync.message.SyncModuleTransactionMessage;
import module.logic.sync.request.TransactionRequest;
import types.complex.transaction.AircraftTransaction;
import types.complex.transaction.BatteryTransaction;
import types.complex.transaction.EventTransaction;
import types.primitive.TransactionProperties;
import util.StringUtil;

/**
 * ...
 * @author d3relict
 */

class SyncModule extends Module
    implements IModule
    implements ISynchronizer
{
    public var userId(get, set):String;

    public var status(get, null):UInt;
    public var lastAttempt(get, null):Date;
    public var lastSuccess(get, null):Date;

    public function new(localStorage:ILocalStorage, serviceConfig:IStatefulConfig)
    {
        super();

        _injector.mapToValue(ILocalStorage, localStorage);

        _addStatefulConfigs([serviceConfig]);
        _addStatelessConfigClasses([SyncCommandConfig, SyncModelConfig]);

        //TODO: better solution...
        Timer.delay(function(){
            _dispatchPrivateMessage(SyncModulePrivateMessage.INITIALIZE);
        }, 10);

        //TODO: a lot better solution!
        var t = new Timer(15000);
        t.run = function() {
            _dispatchPrivateMessage(SyncModulePrivateMessage.START_SYNC);
        }
    }

    override function _getRuntimeDependencies():IRuntimeDependencies
    {
        return new RuntimeDependencies();
    }

    public function sync():Void
    {
        getLogger().debug('manual sync');
        _dispatchPrivateMessage(SyncModulePrivateMessage.START_SYNC);
    }

    public function queue(transaction:TransactionProperties):Void
    {
        getLogger().debug('store transaction');
        _dispatchPrivateMessage(
            SyncModulePrivateMessage.ADD_TO_QUEUE,
            new TransactionRequest(transaction)
        );
    }

    function get_userId():String
    {
        return _get(IAuthenticationModel).userId;
    }

    function set_userId(value:String):String
    {
        if (value != null)
            _dispatchPrivateMessage(SyncModulePrivateMessage.SET_USER_ID, new Request([new ExecutionPayload(value).withClassName('String')]));
        else
            _dispatchPrivateMessage(SyncModulePrivateMessage.CLEAR_USER_ID);

        return value;
    }

    function get_status():UInt
    {
        return _get(ISyncStatusModel).status;
    }

    function get_lastAttempt():Date
    {
        return _get(ISyncStatusModel).lastAttempt;
    }

    function get_lastSuccess():Date
    {
        return _get(ISyncStatusModel).lastSuccess;
    }
}

private class SyncCommandConfig extends StatelessCommandConfig
{
	override public function configure():Void
	{
		this.map(SyncModulePrivateMessage.INITIALIZE,       InitializeMacro);
		this.map(SyncModulePrivateMessage.SET_USER_ID,      SetUserIdMacro);
		this.map(SyncModulePrivateMessage.CLEAR_USER_ID,    ClearUserIdMacro);

		this.map(SyncModulePrivateMessage.FETCH,            FetchDataCommand);
		this.map(SyncModulePrivateMessage.STORE,            StoreDataCommand);
        this.map(SyncModulePrivateMessage.ADD_TO_QUEUE,     TransactionMacro);
        this.map(SyncModulePrivateMessage.START_SYNC,       SynchronizeMacro);
	}
}

private class SyncModelConfig extends StatelessModelConfig
{
	override public function configure() : Void
	{
		this.map(ISyncStatusModel,      SyncStatusModel);
		this.map(IAuthenticationModel,  AuthenticationModel);
		this.map(ITransactionModel,     TransactionModel);
	}
}
