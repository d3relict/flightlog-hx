package module.logic.settings;
import hex.config.stateless.StatelessModuleConfig;
import hex.module.IModule;
import hex.module.Module;
import hex.module.dependency.IRuntimeDependencies;
import hex.module.dependency.RuntimeDependencies;
import interfaces.logic.ISettingsProvider;

/**
 * ...
 * @author d3relict
 */
class SettingsModule extends Module implements ISettingsProvider implements IModule
{
    public var minChargeToEnableFlight(default, never):Float = 75;
    public var maxLevelToEnableCharge(default, never):Float = 90;
    public var minLevelToEnableDischarge(default, never):Float = 41;
    public var defaultDischargeLevel(default, never):Float = 40;
    public var defaultPostFlightLevel(default, never):Float = 20;
    public var overChargeMultiplier(default, never):Float = 1.2;
    public var defaultMaxFlightDuration(default, never):Float = 600;

    public function new()
    {
        super();
    }

    override function _getRuntimeDependencies():IRuntimeDependencies
    {
        return new RuntimeDependencies();
    }
}

private class SettingsModuleConfig extends StatelessModuleConfig
{
    override public function configure():Void
    {
    }
}
