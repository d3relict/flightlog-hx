package util;
import haxe.Json;
import haxe.Serializer;
import haxe.Unserializer;
import interfaces.util.ILocalStorage;
import js.Browser;
import js.html.Storage;

/**
 * ...
 * @author d3relict
 */
class LocalStorage implements ILocalStorage
{
    static private var storage:Storage = Browser.window.localStorage;
    private var name:String;
    
    public function new(name:String) 
    {
        this.name = 'flightlog.' + name;
    }
    
    public function set(key:String, value:Dynamic):Void
    {
        storage.setItem(name + '.' + key, Serializer.run(value));
    }
    
    public function get(key:String):Dynamic
    {
        var stored:String = storage.getItem(name + '.' + key);
        return stored != null ? Unserializer.run(stored) : null;
    }
    
    public function remove(key:String):Void
    {
        storage.removeItem(name + '.' + key);
    }
}
