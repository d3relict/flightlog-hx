package util;
import types.complex.aircraft.Aircraft;
import types.complex.battery.Battery;

/**
 * ...
 * @author d3relict
 */
class StringUtil
{
    static public function leadingZero(number:UInt, length:UInt):String
    {
        var zeros:String = '0';
        while (zeros.length < length)
        {
            zeros += zeros;
        }
        zeros += number;
        return zeros.substr(-length, length);
    }

    static public function formatDuration(duration:UInt):String
    {
        var sec:UInt = duration % 60;
        var min:UInt = Math.floor(duration / 60) % 60;
        var hrs:UInt = Math.floor(duration / 3600);
        return leadingZero(hrs, 2) + ':' + leadingZero(min, 2) + ':' + leadingZero(sec, 2);
    }

    static public function aircraftLabel(properties:Aircraft):String
    {
        if (properties == null)
        {
            return 'unknown';
        }

        var label:String = '';

        if (properties.name != null)
        {
            label += properties.name;
            label += ' (';
        }

        label += properties.manufacturer;
        label += ' ';
        label += properties.type;

        if (properties.name != null)
        {
            label += ')';
        }

        return label;
    }

    static public function batteryLabel(properties:Battery, charge:Bool = true, nameOnly:Bool = false):String
    {
        if (properties == null)
        {
            return 'unknown';
        }

        var label:String = '#';
        label += properties.name;

        //TODO: templates!
        if (!nameOnly)
        {
            label += ' ';
            label += properties.manufacturer;
            label += ' ';
            label += properties.type;
            label += ' ';
            label += properties.capacity + 'mAh';
        }

        if (charge)
        {
            label += ' (';
            label += (Math.floor(properties.charge / properties.capacity * 100)) + '%';
            label += ', ';
            label += properties.charge + 'mAh';
            label += ')';
        }

        return label;
    }

    static public function generateId(length:UInt = 16):String
    {
        var id:String = '';

        while (id.length < length)
            id += StringTools.hex(Std.random(cast Math.pow(2, 32)), 8).toLowerCase();

        return id.substr(0, length);
    }

    static public function roundToString(value:Float, precision:UInt = 2):String
    {
        var stringified = Std.string(Math.round(value / Math.pow(10, -precision)));
        return stringified.substr(0, stringified.length - precision) + (precision > 0 ? '.' + stringified.substr( -precision, precision) : '');
    }
}
