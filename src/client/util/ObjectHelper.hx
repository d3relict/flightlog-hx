package util;

/**
 * ...
 * @author d3relict
 */
class ObjectHelper
{

    static public function setFieldDefaults(target:Dynamic, defaults:Dynamic):Void
    {
        for (key in Reflect.fields(defaults))
        {
            if (Reflect.field(target, key) == null)
            {
                Reflect.setField(target, key, Reflect.field(defaults, key));
            }
        }
    }

}
