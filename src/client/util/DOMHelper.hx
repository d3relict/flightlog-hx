package util;
import js.Browser;
import js.html.OptionElement;
import js.html.SelectElement;
import js.jquery.JQuery;

/**
 * ...
 * @author d3relict
 */

typedef SelectItem = {
    var value:String;
    var text:String;
    var selected:Bool;
    var disabled:Bool;
}

class DOMHelper
{
    public static function fillSelectElement(selectElement:SelectElement, properties:Array<SelectItem>):Void {
        var option:OptionElement;
        selectElement.innerHTML = '';

        for (property in properties) {
            option = Browser.document.createOptionElement();
            option.text = property.text;
            option.value = property.value;
            option.selected = property.selected;
            option.disabled = property.disabled;
            selectElement.appendChild(option);
        }

        untyped new JQuery(selectElement).material_select();
    }
}
