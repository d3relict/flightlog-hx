package service.sync;

import haxe.Serializer;
import hex.service.stateless.http.HTTPServiceParameters;
import service.sync.request.Request;

/**
 * ...
 * @author d3relict
 */
class SyncServiceParameters extends HTTPServiceParameters
{
    public var request:String;

    public function new(request:Request)
    {
        super();
        this.request = Serializer.run(request);
    }
}
