package service.sync;
import hex.log.Logger;
import hex.service.stateless.http.HTTPRequestMethod;
import hex.service.stateless.http.HTTPService;
import hex.service.stateless.http.HTTPServiceConfiguration;
import interfaces.service.ISyncService;
import service.sync.response.Response;

/**
 * ...
 * @author d3relict
 */
class SyncService extends HTTPService implements ISyncService
{
    public var result(get, null):Response;

    public function new()
    {
        super();
    }

    @PostConstruct
    override public function createConfiguration() : Void
    {
        setConfiguration(new HTTPServiceConfiguration('/service.php', HTTPRequestMethod.POST));
        setParser(new SyncServiceParser());
    }

    function get_result():Response
    {
        return cast _result;
    }
}
