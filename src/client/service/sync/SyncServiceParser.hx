package service.sync;
import haxe.Unserializer;
import hex.data.IParser;

/**
 * ...
 * @author d3relict
 */
class SyncServiceParser implements IParser<Dynamic>
{
    public function new()
    {
    }

    public function parse(serialized:Dynamic, target:Dynamic = null):Dynamic
    {
        return Unserializer.run(serialized);
    }
}
