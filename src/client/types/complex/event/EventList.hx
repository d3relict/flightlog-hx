package types.complex.event;
import types.complex.event.Event;

/**
 * @author d3relict
 */
typedef EventList = Array<Event>;
