package types.complex.event;
import types.primitive.*;
import types.complex.event.*;
/**
 * @author d3relict

 */
typedef Event =
{
    > Identity,
    > EventProperties,

    var flight:Flight;
    var charges:ChargeList;
}
