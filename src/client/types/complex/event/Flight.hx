package types.complex.event;
import types.primitive.*;

/**
 * @author d3relict
 */
typedef Flight =
{
    > Identity,
    > FlightProperties,
}
