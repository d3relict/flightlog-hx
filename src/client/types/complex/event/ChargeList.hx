package types.complex.event;

/**
 * @author d3relict
 */
typedef ChargeList = Array<Charge>;
