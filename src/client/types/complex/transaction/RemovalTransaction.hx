package types.complex.transaction;
import types.primitive.*;

/**
 * @author d3relict
 */
typedef RemovalTransaction =
{
    > TransactionProperties,
    > Identity,
}
