package types.complex.transaction;
import types.primitive.TransactionProperties;

/**
 * @author d3relict
 */
typedef TransactionList = Array<TransactionProperties>;
