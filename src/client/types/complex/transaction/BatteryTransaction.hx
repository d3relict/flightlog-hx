package types.complex.transaction;
import types.primitive.*;
import types.complex.battery.*;
/**
 * @author d3relict
 */
typedef BatteryTransaction =
{
    > TransactionProperties,
    > Battery,
}
