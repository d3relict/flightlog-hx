package types.complex.transaction;
import types.primitive.*;
import types.complex.aircraft.*;
/**
 * @author d3relict
 */
typedef AircraftTransaction =
{
    > TransactionProperties,
    > Aircraft,
	> Association,
}
