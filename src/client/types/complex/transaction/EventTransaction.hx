package types.complex.transaction;
import types.primitive.*;
import types.complex.event.*;
/**
 * @author d3relict
 */
typedef EventTransaction =
{
    > TransactionProperties,
    > Event,
}
