package types.complex.battery;
import types.complex.*;
import types.complex.battery.Battery;

/**
 * @author d3relict
 */
typedef BatteryList = Array<Battery>;
