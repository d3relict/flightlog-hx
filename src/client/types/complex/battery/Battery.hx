package types.complex.battery;
import types.primitive.*;
/**
 * @author d3relict
 */
typedef Battery =
{
    > Identity,
    > BatteryProperties,
    > Classification,
    > Association,
}
