package types.complex.aircraft;
import types.primitive.*;
/**
 * @author d3relict
 */
typedef Aircraft =
{
    > Identity,
    > Classification,
    > AircraftProperties,
}
