package types.complex.aircraft;
import types.complex.aircraft.Aircraft;

/**
 * @author d3relict
 */
typedef AircraftList = Array<Aircraft>;
