package types.primitive;

/**
 * @author d3relict
 */
typedef Identity =
{
    var id:String;
}
