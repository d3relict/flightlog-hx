package types.primitive;

/**
 * @author d3relict
 */
typedef ChargeProperties =
{
    var charge:Int;
    var level:Float;
}
