package types.primitive;

/**
 * @author d3relict
 */
typedef TransactionProperties =
{
    var txType:UInt;

    @:optional
    var txVer:UInt;
}
