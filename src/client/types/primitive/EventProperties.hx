package types.primitive;
import enums.*;

/**
 * @author d3relict
 */
typedef EventProperties =
{
    var timestamp:Float;
    var eventType:UInt;
}
