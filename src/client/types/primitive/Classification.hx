package types.primitive;

/**
 * @author d3relict
 */
typedef Classification =
{
    @:optional var manufacturer:String;
    @:optional var type:String;
    @:optional var name:String;
}
