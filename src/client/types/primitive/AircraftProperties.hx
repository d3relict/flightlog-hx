package types.primitive;

/**
 * @author d3relict
 */
typedef AircraftProperties =
{
    var cellCount:UInt;

    @:optional
    var usualDuration:UInt;
    @:optional
    var totalDuration:UInt;
    @:optional
    var totalCount:UInt;
}
