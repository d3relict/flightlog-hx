package types.primitive;

/**
 * @author d3relict
 */
typedef BatteryProperties =
{
    var cellCount:UInt;
    var capacity:UInt;
    var charge:UInt;
}
