package;

import hex.compiler.parser.xml.XmlCompiler;
import hex.log.layout.JavaScriptConsoleLayout;
import hex.log.layout.LogProxyLayout;

/**
 * ...
 * @author d3relict
 */
class Main
{
    static var self:Main;

    static function main()
    {
        #if debug
        var proxy = new LogProxyLayout();
        proxy.addListener( new JavaScriptConsoleLayout() );
        #end

        self = new Main();
    }

    public function new()
    {
        XmlCompiler.compile('configuration/context.xml');
    }

}
