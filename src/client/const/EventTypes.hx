package const;

/**
 * ...
 * @author d3relict
 */
class EventTypes
{
    public static var CHARGE    (default, never):UInt = 0;
    public static var DISCHARGE (default, never):UInt = 1;
    public static var FLIGHT    (default, never):UInt = 2;
}
