package const;

/**
 * ...
 * @author d3relict
 */
class SyncStatii
{
    public static var UNIDENTIFIED  (default, never):UInt = 0;

    public static var WAITING       (default, never):UInt = 1;
    public static var QUEUED        (default, never):UInt = 2;
    public static var PROGRESS      (default, never):UInt = 3;

    public static var REJECTED      (default, never):UInt = 4;
    public static var OFFLINE       (default, never):UInt = 5;
    public static var FAILED        (default, never):UInt = 6;
}
