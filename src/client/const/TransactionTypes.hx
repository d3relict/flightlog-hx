package const;

/**
 * ...
 * @author d3relict
 */
class TransactionTypes
{
    public static var ADD_AIRCRAFT      (default, never):UInt = 0;
    public static var ADD_BATTERY       (default, never):UInt = 1;
    public static var ADD_EVENT         (default, never):UInt = 2;

    public static var REMOVE_AIRCRAFT   (default, never):UInt = 3;
    public static var REMOVE_BATTERY    (default, never):UInt = 4;
    public static var REMOVE_EVENT      (default, never):UInt = 5;
}
