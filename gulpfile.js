/**
 * gulpfile
 * @author d3relict
 */

var gulp     = require('gulp');
var clean    = require('gulp-clean');
var run      = require('gulp-run-command').default;
var gulpif   = require('gulp-if');
var uglify   = require('gulp-uglify');
var rename   = require('gulp-rename');
var pump     = require('pump');
var minimist = require('minimist');

var options = minimist(process.argv.slice(2), {
  string: 'env',
  default: { env: process.env.NODE_ENV || 'production' }
});

gulp.task('clean-bin', function() {
    return gulp
    .src('./bin/*', {read: false})
    .pipe(clean({force: true}));
});

gulp.task('compile client', ['clean-bin'], run('haxe ' + (options.env == 'debug' ? ' -debug ' : ' ') + 'build/client.hxml'))
gulp.task('compile server', ['compile client'], run('haxe build/server.hxml'))

gulp.task('compress', ['compile server'], function (cb) {
  pump([
        gulp.src('bin/temp/*.js'),
        gulpif(options.env === 'release', uglify()),
        gulp.dest('bin/js')
    ],
    cb
  );
});

gulp.task('default', ['compress'], function() {
    //copy material-design-icons
    gulp
    .src('./node_modules/material-design-icons/iconfont/MaterialIcons-*.*f*')
    .pipe(gulp.dest('./bin/fonts'));

    //copy jquery
    gulp
    .src('./node_modules/jquery/dist/jquery.min.js')
    .pipe(gulp.dest('./bin/js'));

    //copy materialize-css stuff
    gulp
    .src('./node_modules/materialize-css/dist/css/*.min.css')
    .pipe(gulp.dest('./bin/css/'));

    gulp
    .src('./node_modules/materialize-css/dist/js/*.min.js')
    .pipe(gulp.dest('./bin/js'));

    gulp
    .src('./node_modules/materialize-css/fonts/**/*')
    .pipe(gulp.dest('./bin/fonts/'));

    //copy nouislider stuff
    gulp
    .src('./node_modules/materialize-css/extras/nouislider/*.css')
    .pipe(gulp.dest('./bin/css/'));

    gulp
    .src('./node_modules/materialize-css/extras/nouislider/*.min.js')
    .pipe(gulp.dest('./bin/js'));

    //copy materialize-clockpicker stuff
    gulp
    .src('./node_modules/materialize-clockpicker/dist/css/*.css')
    .pipe(gulp.dest('./bin/css/'));

    gulp
    .src('./node_modules/materialize-clockpicker/dist/js/*.js')
    .pipe(gulp.dest('./bin/js'));

    //copy html
    gulp
    .src('./resources/*.html')
    .pipe(gulp.dest('./bin/'));

    //copy css
    gulp
    .src('./resources/css/*.css')
    .pipe(gulp.dest('./bin/css/'));

    //empty temp
    gulp
    .src('./bin/temp', {read: false})
    .pipe(clean({force: true}));

    //rename php
    gulp.src("./bin/index.php")
    .pipe(rename("service.php"))
    .pipe(gulp.dest("./bin"));
    gulp
    .src('./bin/index.php', {read: false})
    .pipe(clean({force: true}));
});
